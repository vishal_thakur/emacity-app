<Q                         DIRECTIONAL     �  #ifdef VERTEX
#version 300 es

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec4 _ProjectionParams;
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixV[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
in highp vec4 in_POSITION0;
in highp vec2 in_TEXCOORD0;
in highp vec4 in_COLOR0;
out highp vec2 vs_TEXCOORD0;
out highp vec4 vs_COLOR0;
out highp vec4 vs_TEXCOORD1;
vec4 u_xlat0;
vec4 u_xlat1;
float u_xlat2;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
    gl_Position = u_xlat1;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
    vs_COLOR0 = in_COLOR0;
    u_xlat2 = u_xlat0.y * hlslcc_mtx4x4unity_MatrixV[1].z;
    u_xlat0.x = hlslcc_mtx4x4unity_MatrixV[0].z * u_xlat0.x + u_xlat2;
    u_xlat0.x = hlslcc_mtx4x4unity_MatrixV[2].z * u_xlat0.z + u_xlat0.x;
    u_xlat0.x = hlslcc_mtx4x4unity_MatrixV[3].z * u_xlat0.w + u_xlat0.x;
    vs_TEXCOORD1.z = (-u_xlat0.x);
    u_xlat0.x = u_xlat1.y * _ProjectionParams.x;
    u_xlat0.w = u_xlat0.x * 0.5;
    u_xlat0.xz = u_xlat1.xw * vec2(0.5, 0.5);
    vs_TEXCOORD1.w = u_xlat1.w;
    vs_TEXCOORD1.xy = u_xlat0.zz + u_xlat0.xw;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp float;
precision highp int;
#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec4 _Time;
uniform 	vec4 _ProjectionParams;
uniform 	vec4 _ZBufferParams;
uniform 	vec4 _MainTex_ST;
uniform 	vec4 _TintColor;
uniform 	float _GradientUSpeed;
uniform 	float _GradientVSpeed;
uniform 	vec4 _Gradient_ST;
uniform 	float _NoiseAmount;
uniform 	vec4 _Distortion_ST;
uniform 	mediump float _DistortMainTexture;
uniform 	float _DistortionUSpeed;
uniform 	float _DistortionVSpeed;
uniform 	vec4 _MainTexMask_ST;
uniform 	float _GradientPower;
uniform 	float _ColorMultiplier;
uniform 	float _EdgeSoftness;
uniform 	float _MainTextUSpeed;
uniform 	float _MainTextVSpeed;
uniform 	float _DoubleSided;
UNITY_LOCATION(0) uniform highp sampler2D _CameraDepthTexture;
UNITY_LOCATION(1) uniform mediump sampler2D _Distortion;
UNITY_LOCATION(2) uniform mediump sampler2D _MainTex;
UNITY_LOCATION(3) uniform mediump sampler2D _Gradient;
UNITY_LOCATION(4) uniform mediump sampler2D _MainTexMask;
in highp vec2 vs_TEXCOORD0;
in highp vec4 vs_COLOR0;
in highp vec4 vs_TEXCOORD1;
layout(location = 0) out highp vec4 SV_Target0;
vec3 u_xlat0;
mediump vec4 u_xlat16_0;
vec3 u_xlat1;
mediump vec3 u_xlat16_1;
vec3 u_xlat2;
mediump vec3 u_xlat16_2;
vec2 u_xlat6;
mediump float u_xlat16_10;
void main()
{
    u_xlat0.xy = _Time.yy * vec2(_DistortionUSpeed, _DistortionVSpeed) + vs_TEXCOORD0.xy;
    u_xlat0.xy = u_xlat0.xy * _Distortion_ST.xy + _Distortion_ST.zw;
    u_xlat16_0.x = texture(_Distortion, u_xlat0.xy).x;
    u_xlat0.xy = u_xlat16_0.xx + (-vs_TEXCOORD0.xy);
    u_xlat6.xy = vec2(_NoiseAmount) * u_xlat0.xy + vs_TEXCOORD0.xy;
    u_xlat0.xy = u_xlat0.xy * vec2(_NoiseAmount);
    u_xlat0.xy = vec2(_DistortMainTexture) * u_xlat0.xy + vs_TEXCOORD0.xy;
    u_xlat6.xy = _Time.yy * vec2(_GradientUSpeed, _GradientVSpeed) + u_xlat6.xy;
    u_xlat6.xy = u_xlat6.xy * _Gradient_ST.xy + _Gradient_ST.zw;
    u_xlat16_1.xyz = texture(_Gradient, u_xlat6.xy).xyz;
    u_xlat16_2.xyz = log2(u_xlat16_1.xyz);
    u_xlat2.xyz = u_xlat16_2.xyz * vec3(_GradientPower);
    u_xlat2.xyz = exp2(u_xlat2.xyz);
    u_xlat1.xyz = u_xlat16_1.xyz * u_xlat2.xyz;
    u_xlat2.x = _Time.y * _MainTextUSpeed;
    u_xlat2.y = _Time.y * _MainTextVSpeed;
    u_xlat0.xy = u_xlat0.xy + u_xlat2.xy;
    u_xlat0.xy = u_xlat0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat16_0 = texture(_MainTex, u_xlat0.xy);
    u_xlat1.xyz = u_xlat1.xyz * u_xlat16_0.www;
    u_xlat2.xy = vs_TEXCOORD0.xy * _MainTexMask_ST.xy + _MainTexMask_ST.zw;
    u_xlat16_10 = texture(_MainTexMask, u_xlat2.xy).w;
    u_xlat1.xyz = vec3(u_xlat16_10) * u_xlat1.xyz;
    u_xlat2.x = uintBitsToFloat(uint((gl_FrontFacing ? 0xffffffffu : uint(0)) & 1065353216u));
    u_xlat2.x = max(u_xlat2.x, _DoubleSided);
    u_xlat2.x = min(u_xlat2.x, 1.0);
    u_xlat1.xyz = u_xlat1.xyz * u_xlat2.xxx;
    u_xlat0.xyz = u_xlat16_0.xyz * vs_COLOR0.xyz;
    u_xlat2.xyz = _TintColor.xyz * vec3(vec3(_ColorMultiplier, _ColorMultiplier, _ColorMultiplier));
    u_xlat0.xyz = u_xlat0.xyz * u_xlat2.xyz;
    u_xlat0.xyz = u_xlat0.xyz + u_xlat0.xyz;
    u_xlat0.xyz = u_xlat1.xyz * u_xlat0.xyz;
    u_xlat1.xy = vs_TEXCOORD1.xy / vs_TEXCOORD1.ww;
    u_xlat1.x = texture(_CameraDepthTexture, u_xlat1.xy).x;
    u_xlat1.x = _ZBufferParams.z * u_xlat1.x + _ZBufferParams.w;
    u_xlat1.x = float(1.0) / u_xlat1.x;
    u_xlat1.x = u_xlat1.x + (-_ProjectionParams.y);
    u_xlat1.y = vs_TEXCOORD1.z + (-_ProjectionParams.y);
    u_xlat1.xy = max(u_xlat1.xy, vec2(0.0, 0.0));
    u_xlat1.x = (-u_xlat1.y) + u_xlat1.x;
    u_xlat1.x = u_xlat1.x / _EdgeSoftness;
#ifdef UNITY_ADRENO_ES3
    u_xlat1.x = min(max(u_xlat1.x, 0.0), 1.0);
#else
    u_xlat1.x = clamp(u_xlat1.x, 0.0, 1.0);
#endif
    SV_Target0.xyz = u_xlat0.xyz * u_xlat1.xxx + u_xlat0.xyz;
    u_xlat0.x = vs_COLOR0.w * _TintColor.w;
    u_xlat0.x = u_xlat16_0.w * u_xlat0.x;
    u_xlat0.x = u_xlat16_10 * u_xlat0.x;
    SV_Target0.w = u_xlat1.x * u_xlat0.x;
    return;
}

#endif
                             $Globals�         _Time                            _ProjectionParams                           _ZBufferParams                           _MainTex_ST                   0   
   _TintColor                    @      _GradientUSpeed                   P      _GradientVSpeed                   T      _Gradient_ST                  `      _NoiseAmount                  p      _Distortion_ST                    �      _DistortMainTexture                   �      _DistortionUSpeed                     �      _DistortionVSpeed                     �      _MainTexMask_ST                   �      _GradientPower                    �      _ColorMultiplier                  �      _EdgeSoftness                     �      _MainTextUSpeed                   �      _MainTextVSpeed                   �      _DoubleSided                  �          $Globals�         _ProjectionParams                            unity_ObjectToWorld                        unity_MatrixV                    P      unity_MatrixVP                   �             _CameraDepthTexture                   _Distortion                 _MainTex             	   _Gradient                   _MainTexMask             