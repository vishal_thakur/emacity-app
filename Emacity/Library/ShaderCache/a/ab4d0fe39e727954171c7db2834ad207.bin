<Q                         LIGHTMAP_ON     %0  #ifdef VERTEX
#version 300 es

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
UNITY_BINDING(0) uniform UnityPerDraw {
#endif
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_WorldToObject[4];
	UNITY_UNIFORM vec4 unity_LODFade;
	UNITY_UNIFORM mediump vec4 unity_WorldTransformParams;
	UNITY_UNIFORM mediump vec4 unity_LightData;
	UNITY_UNIFORM mediump vec4 unity_LightIndices[2];
	UNITY_UNIFORM mediump vec4 unity_ProbesOcclusion;
	UNITY_UNIFORM mediump vec4 unity_SpecCube0_HDR;
	UNITY_UNIFORM vec4 unity_LightmapST;
	UNITY_UNIFORM vec4 unity_DynamicLightmapST;
	UNITY_UNIFORM mediump vec4 unity_SHAr;
	UNITY_UNIFORM mediump vec4 unity_SHAg;
	UNITY_UNIFORM mediump vec4 unity_SHAb;
	UNITY_UNIFORM mediump vec4 unity_SHBr;
	UNITY_UNIFORM mediump vec4 unity_SHBg;
	UNITY_UNIFORM mediump vec4 unity_SHBb;
	UNITY_UNIFORM mediump vec4 unity_SHC;
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
};
#endif
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TANGENT0;
in highp vec4 in_TEXCOORD0;
in highp vec4 in_TEXCOORD1;
out highp vec2 vs_TEXCOORD0;
out mediump vec4 vs_TEXCOORD1;
out highp vec4 vs_TEXCOORD2;
out highp vec3 vs_TEXCOORD3;
out highp vec3 vs_TEXCOORD4;
out highp vec3 vs_TEXCOORD5;
out highp vec3 vs_TEXCOORD6;
out highp vec3 vs_TEXCOORD7;
out mediump vec4 vs_TEXCOORD8;
out mediump vec4 vs_TEXCOORD9;
vec4 u_xlat0;
vec4 u_xlat1;
vec3 u_xlat2;
float u_xlat9;
void main()
{
    u_xlat0.xyz = in_POSITION0.yyy * hlslcc_mtx4x4unity_ObjectToWorld[1].xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_ObjectToWorld[0].xyz * in_POSITION0.xxx + u_xlat0.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_ObjectToWorld[2].xyz * in_POSITION0.zzz + u_xlat0.xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0.xyz = u_xlat0.xyz + hlslcc_mtx4x4unity_ObjectToWorld[3].xyz;
    vs_TEXCOORD7.xyz = (-u_xlat0.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat0.xyz = u_xlat1.yyy * hlslcc_mtx4x4unity_WorldToObject[1].xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToObject[0].xyz * u_xlat1.xxx + u_xlat0.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToObject[2].xyz * u_xlat1.zzz + u_xlat0.xyz;
    vs_TEXCOORD3.xyz = u_xlat1.xyz;
    u_xlat0.xyz = u_xlat0.xyz + hlslcc_mtx4x4unity_WorldToObject[3].xyz;
    u_xlat1.xyz = u_xlat0.yyy * hlslcc_mtx4x4unity_ObjectToWorld[1].xyz;
    u_xlat0.xyw = hlslcc_mtx4x4unity_ObjectToWorld[0].xyz * u_xlat0.xxx + u_xlat1.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_ObjectToWorld[2].xyz * u_xlat0.zzz + u_xlat0.xyw;
    u_xlat0.xyz = u_xlat0.xyz + hlslcc_mtx4x4unity_ObjectToWorld[3].xyz;
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    gl_Position = u_xlat0 + hlslcc_mtx4x4unity_MatrixVP[3];
    vs_TEXCOORD0.xy = in_TEXCOORD1.xy * unity_LightmapST.xy + unity_LightmapST.zw;
    vs_TEXCOORD1 = vec4(0.0, 0.0, 0.0, 0.0);
    vs_TEXCOORD2 = vec4(0.0, 0.0, 0.0, 0.0);
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat9 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat9 = inversesqrt(u_xlat9);
    u_xlat0.xyz = vec3(u_xlat9) * u_xlat0.xyz;
    vs_TEXCOORD4.xyz = u_xlat0.xyz;
    u_xlat1.xyz = in_TANGENT0.yyy * hlslcc_mtx4x4unity_ObjectToWorld[1].xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_ObjectToWorld[0].xyz * in_TANGENT0.xxx + u_xlat1.xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_ObjectToWorld[2].xyz * in_TANGENT0.zzz + u_xlat1.xyz;
    u_xlat9 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat9 = inversesqrt(u_xlat9);
    u_xlat1.xyz = vec3(u_xlat9) * u_xlat1.xyz;
    vs_TEXCOORD5.xyz = u_xlat1.xyz;
    u_xlat2.xyz = u_xlat0.zxy * u_xlat1.yzx;
    u_xlat0.xyz = u_xlat0.yzx * u_xlat1.zxy + (-u_xlat2.xyz);
    vs_TEXCOORD6.xyz = u_xlat0.xyz * in_TANGENT0.www;
    vs_TEXCOORD8 = in_TEXCOORD0;
    vs_TEXCOORD9 = in_TEXCOORD1;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es
#ifdef GL_EXT_shader_texture_lod
#extension GL_EXT_shader_texture_lod : enable
#endif

precision highp float;
precision highp int;
#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec4 _MainLightPosition;
uniform 	mediump vec4 _MainLightColor;
uniform 	vec4 _TimeParameters;
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
UNITY_BINDING(0) uniform UnityPerDraw {
#endif
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_WorldToObject[4];
	UNITY_UNIFORM vec4 unity_LODFade;
	UNITY_UNIFORM mediump vec4 unity_WorldTransformParams;
	UNITY_UNIFORM mediump vec4 unity_LightData;
	UNITY_UNIFORM mediump vec4 unity_LightIndices[2];
	UNITY_UNIFORM mediump vec4 unity_ProbesOcclusion;
	UNITY_UNIFORM mediump vec4 unity_SpecCube0_HDR;
	UNITY_UNIFORM vec4 unity_LightmapST;
	UNITY_UNIFORM vec4 unity_DynamicLightmapST;
	UNITY_UNIFORM mediump vec4 unity_SHAr;
	UNITY_UNIFORM mediump vec4 unity_SHAg;
	UNITY_UNIFORM mediump vec4 unity_SHAb;
	UNITY_UNIFORM mediump vec4 unity_SHBr;
	UNITY_UNIFORM mediump vec4 unity_SHBg;
	UNITY_UNIFORM mediump vec4 unity_SHBb;
	UNITY_UNIFORM mediump vec4 unity_SHC;
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
};
#endif
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
UNITY_BINDING(1) uniform UnityPerMaterial {
#endif
	UNITY_UNIFORM vec4 Color_5D50800;
	UNITY_UNIFORM float Vector1_78D269BC;
	UNITY_UNIFORM float Vector1_1A5CD27C;
	UNITY_UNIFORM float Vector1_CF777CBB;
	UNITY_UNIFORM vec4 Color_9AFE60D0;
	UNITY_UNIFORM float Vector1_A61ED75F;
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
};
#endif
UNITY_LOCATION(0) uniform mediump samplerCube unity_SpecCube0;
UNITY_LOCATION(1) uniform mediump sampler2D unity_Lightmap;
in highp vec2 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD4;
in highp vec3 vs_TEXCOORD7;
in mediump vec4 vs_TEXCOORD8;
layout(location = 0) out mediump vec4 SV_Target0;
vec3 u_xlat0;
mediump vec4 u_xlat16_0;
vec2 u_xlat1;
mediump vec3 u_xlat16_1;
vec2 u_xlat2;
bool u_xlatb2;
int u_xlati3;
vec2 u_xlat4;
mediump vec3 u_xlat16_5;
mediump vec3 u_xlat16_6;
mediump vec3 u_xlat16_7;
vec3 u_xlat11;
bool u_xlatb11;
float u_xlat16;
vec2 u_xlat17;
vec2 u_xlat18;
float u_xlat24;
mediump float u_xlat16_24;
int u_xlati24;
mediump float u_xlat16_29;
mediump float u_xlat16_30;
void main()
{
    u_xlat0.xy = vs_TEXCOORD8.xy + vec2(-0.5, -0.5);
    u_xlat24 = dot(u_xlat0.xy, u_xlat0.xy);
    u_xlat0.z = (-u_xlat0.x);
    u_xlat0.xy = u_xlat0.yz * vec2(u_xlat24) + vs_TEXCOORD8.xy;
    u_xlat16 = _TimeParameters.x * Vector1_78D269BC;
    u_xlat0.xy = u_xlat0.xy * vec2(vec2(Vector1_1A5CD27C, Vector1_1A5CD27C));
    u_xlat1.xy = floor(u_xlat0.xy);
    u_xlat0.xy = fract(u_xlat0.xy);
    u_xlat17.x = float(0.0);
    u_xlat17.y = float(8.0);
    for(int u_xlati_loop_1 = int(0xFFFFFFFFu) ; u_xlati_loop_1<=1 ; u_xlati_loop_1++)
    {
        u_xlat2.y = float(u_xlati_loop_1);
        u_xlat18.xy = u_xlat17.xy;
        for(int u_xlati_loop_2 = int(0xFFFFFFFFu) ; u_xlati_loop_2<=1 ; u_xlati_loop_2++)
        {
            u_xlat2.x = float(u_xlati_loop_2);
            u_xlat11.xy = u_xlat1.xy + u_xlat2.xy;
            u_xlat11.z = dot(u_xlat11.xy, vec2(15.2700005, 99.4100037));
            u_xlat11.x = dot(u_xlat11.xy, vec2(47.6300011, 89.9800034));
            u_xlat4.xy = sin(u_xlat11.xz);
            u_xlat11.xy = u_xlat4.xy * vec2(46839.3203, 46839.3203);
            u_xlat11.xy = fract(u_xlat11.xy);
            u_xlat11.xy = vec2(u_xlat16) * u_xlat11.xy;
            u_xlat11.x = sin(u_xlat11.x);
            u_xlat4.x = u_xlat11.x * 0.5 + u_xlat2.x;
            u_xlat2.x = cos(u_xlat11.y);
            u_xlat4.y = u_xlat2.x * 0.5 + u_xlat2.y;
            u_xlat11.xy = (-u_xlat0.xy) + u_xlat4.xy;
            u_xlat11.xy = u_xlat11.xy + vec2(0.5, 0.5);
            u_xlat2.x = dot(u_xlat11.xy, u_xlat11.xy);
            u_xlat2.x = sqrt(u_xlat2.x);
#ifdef UNITY_ADRENO_ES3
            u_xlatb11 = !!(u_xlat2.x<u_xlat18.y);
#else
            u_xlatb11 = u_xlat2.x<u_xlat18.y;
#endif
            u_xlat18.xy = (bool(u_xlatb11)) ? u_xlat2.xx : u_xlat18.xy;
        }
        u_xlat17.xy = u_xlat18.xy;
    }
    u_xlat0.x = log2(u_xlat17.x);
    u_xlat0.x = u_xlat0.x * Vector1_CF777CBB;
    u_xlat0.x = exp2(u_xlat0.x);
    u_xlat0.xyz = u_xlat0.xxx * Color_9AFE60D0.xyz + Color_5D50800.xyz;
    u_xlat16_1.xyz = texture(unity_Lightmap, vs_TEXCOORD0.xy).xyz;
    u_xlat16_5.xyz = u_xlat16_1.xyz + u_xlat16_1.xyz;
    u_xlat16_6.xyz = u_xlat0.xyz * vec3(0.959999979, 0.959999979, 0.959999979);
    u_xlat16_29 = unity_LightData.z * unity_ProbesOcclusion.x;
    u_xlat16_30 = dot((-vs_TEXCOORD7.xyz), vs_TEXCOORD4.xyz);
    u_xlat16_30 = u_xlat16_30 + u_xlat16_30;
    u_xlat16_7.xyz = vs_TEXCOORD4.xyz * (-vec3(u_xlat16_30)) + (-vs_TEXCOORD7.xyz);
    u_xlat16_30 = dot(vs_TEXCOORD4.xyz, vs_TEXCOORD7.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat16_30 = min(max(u_xlat16_30, 0.0), 1.0);
#else
    u_xlat16_30 = clamp(u_xlat16_30, 0.0, 1.0);
#endif
    u_xlat16_30 = (-u_xlat16_30) + 1.0;
    u_xlat16_30 = u_xlat16_30 * u_xlat16_30;
    u_xlat16_30 = u_xlat16_30 * u_xlat16_30;
    u_xlat16_0 = textureLod(unity_SpecCube0, u_xlat16_7.xyz, 4.05000019);
    u_xlat16_7.x = u_xlat16_0.w + -1.0;
    u_xlat16_7.x = unity_SpecCube0_HDR.w * u_xlat16_7.x + 1.0;
    u_xlat16_7.x = max(u_xlat16_7.x, 0.0);
    u_xlat16_7.x = log2(u_xlat16_7.x);
    u_xlat16_7.x = u_xlat16_7.x * unity_SpecCube0_HDR.y;
    u_xlat16_7.x = exp2(u_xlat16_7.x);
    u_xlat16_7.x = u_xlat16_7.x * unity_SpecCube0_HDR.x;
    u_xlat16_7.xyz = u_xlat16_0.xyz * u_xlat16_7.xxx;
    u_xlat16_0.xyz = u_xlat16_7.xyz * vec3(0.941176474, 0.941176474, 0.941176474);
    u_xlat16_30 = u_xlat16_30 * 0.5 + 0.0399999991;
    u_xlat16_0.xyz = u_xlat16_0.xyz * vec3(u_xlat16_30);
    u_xlat16_0.xyz = u_xlat16_5.xyz * u_xlat16_6.xyz + u_xlat16_0.xyz;
    u_xlat16_5.x = dot(vs_TEXCOORD4.xyz, _MainLightPosition.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat16_5.x = min(max(u_xlat16_5.x, 0.0), 1.0);
#else
    u_xlat16_5.x = clamp(u_xlat16_5.x, 0.0, 1.0);
#endif
    u_xlat16_5.x = u_xlat16_5.x * u_xlat16_29;
    u_xlat16_5.xyz = u_xlat16_5.xxx * _MainLightColor.xyz;
    u_xlat16_7.xyz = vs_TEXCOORD7.xyz + _MainLightPosition.xyz;
    u_xlat16_29 = dot(u_xlat16_7.xyz, u_xlat16_7.xyz);
    u_xlat16_29 = max(u_xlat16_29, 6.10351563e-05);
    u_xlat16_29 = inversesqrt(u_xlat16_29);
    u_xlat16_7.xyz = vec3(u_xlat16_29) * u_xlat16_7.xyz;
    u_xlat16_29 = dot(vs_TEXCOORD4.xyz, u_xlat16_7.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat16_29 = min(max(u_xlat16_29, 0.0), 1.0);
#else
    u_xlat16_29 = clamp(u_xlat16_29, 0.0, 1.0);
#endif
    u_xlat16_30 = dot(_MainLightPosition.xyz, u_xlat16_7.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat16_30 = min(max(u_xlat16_30, 0.0), 1.0);
#else
    u_xlat16_30 = clamp(u_xlat16_30, 0.0, 1.0);
#endif
    u_xlat16_29 = u_xlat16_29 * u_xlat16_29;
    u_xlat16_24 = u_xlat16_29 * -0.9375 + 1.00001001;
    u_xlat16_29 = u_xlat16_30 * u_xlat16_30;
    u_xlat16_30 = u_xlat16_24 * u_xlat16_24;
    u_xlat16_24 = max(u_xlat16_29, 0.100000001);
    u_xlat16_24 = u_xlat16_24 * u_xlat16_30;
    u_xlat16_24 = u_xlat16_24 * 3.0;
    u_xlat16_24 = 0.0625 / u_xlat16_24;
    u_xlat16_29 = u_xlat16_24 + -6.10351563e-05;
    u_xlat16_6.xyz = vec3(u_xlat16_29) * vec3(0.0399999991, 0.0399999991, 0.0399999991) + u_xlat16_6.xyz;
    SV_Target0.xyz = u_xlat16_6.xyz * u_xlat16_5.xyz + u_xlat16_0.xyz;
    SV_Target0.w = 0.800000012;
    return;
}

#endif
   7                             $Globals0         _MainLightPosition                           _MainLightColor                         _TimeParameters                              UnityPerDraw�        unity_LODFade                     �      unity_WorldTransformParams                    �      unity_LightData                   �      unity_LightIndices                   �      unity_ProbesOcclusion                     �      unity_SpecCube0_HDR                   �      unity_LightmapST                  �      unity_DynamicLightmapST                      
   unity_SHAr                      
   unity_SHAg                       
   unity_SHAb                    0  
   unity_SHBr                    @  
   unity_SHBg                    P  
   unity_SHBb                    `  	   unity_SHC                     p     unity_ObjectToWorld                         unity_WorldToObject                  @          UnityPerMaterial4         Color_5D50800                            Vector1_78D269BC                        Vector1_1A5CD27C                        Vector1_CF777CBB                        Color_9AFE60D0                           Vector1_A61ED75F                  0          $GlobalsP         _WorldSpaceCameraPos                         unity_MatrixVP                                unity_SpecCube0                   unity_Lightmap                  UnityPerDraw              UnityPerMaterial          