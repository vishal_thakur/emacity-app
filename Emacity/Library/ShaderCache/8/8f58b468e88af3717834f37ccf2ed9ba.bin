<Q                         LIGHTMAP_ON    _ADDITIONAL_LIGHT_SHADOWS      _MAIN_LIGHT_SHADOWS     l   #ifdef VERTEX
#version 100

uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 unity_LightmapST;
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 _BaseMap_ST;
uniform 	vec4 hlslcc_mtx4x4_MainLightWorldToShadow[20];
attribute highp vec4 in_POSITION0;
attribute highp vec3 in_NORMAL0;
attribute highp vec2 in_TEXCOORD0;
attribute highp vec2 in_TEXCOORD1;
varying highp vec2 vs_TEXCOORD0;
varying highp vec2 vs_TEXCOORD1;
varying mediump vec3 vs_TEXCOORD3;
varying mediump vec3 vs_TEXCOORD4;
varying mediump vec4 vs_TEXCOORD6;
varying highp vec4 vs_TEXCOORD7;
vec4 u_xlat0;
vec4 u_xlat1;
float u_xlat6;
void main()
{
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _BaseMap_ST.xy + _BaseMap_ST.zw;
    vs_TEXCOORD1.xy = in_TEXCOORD1.xy * unity_LightmapST.xy + unity_LightmapST.zw;
    u_xlat0.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat0.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat0.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat6 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat6 = inversesqrt(u_xlat6);
    u_xlat0.xyz = vec3(u_xlat6) * u_xlat0.xyz;
    vs_TEXCOORD3.xyz = u_xlat0.xyz;
    u_xlat0.xyz = in_POSITION0.yyy * hlslcc_mtx4x4unity_ObjectToWorld[1].xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_ObjectToWorld[0].xyz * in_POSITION0.xxx + u_xlat0.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_ObjectToWorld[2].xyz * in_POSITION0.zzz + u_xlat0.xyz;
    u_xlat0.xyz = u_xlat0.xyz + hlslcc_mtx4x4unity_ObjectToWorld[3].xyz;
    u_xlat1.xyz = (-u_xlat0.xyz) + _WorldSpaceCameraPos.xyz;
    vs_TEXCOORD4.xyz = u_xlat1.xyz;
    vs_TEXCOORD6 = vec4(0.0, 0.0, 0.0, 0.0);
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4_MainLightWorldToShadow[1];
    u_xlat1 = hlslcc_mtx4x4_MainLightWorldToShadow[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4_MainLightWorldToShadow[2] * u_xlat0.zzzz + u_xlat1;
    vs_TEXCOORD7 = u_xlat1 + hlslcc_mtx4x4_MainLightWorldToShadow[3];
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat0 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    gl_Position = u_xlat0 + hlslcc_mtx4x4unity_MatrixVP[3];
    return;
}

#endif
#ifdef FRAGMENT
#version 100

#ifdef GL_FRAGMENT_PRECISION_HIGH
    precision highp float;
#else
    precision mediump float;
#endif
precision highp int;
uniform 	vec4 _MainLightPosition;
uniform 	mediump vec4 _MainLightColor;
uniform 	mediump vec4 unity_LightData;
uniform 	mediump vec4 unity_ProbesOcclusion;
uniform 	mediump vec4 unity_SpecCube0_HDR;
uniform 	mediump vec4 _BaseColor;
uniform 	mediump float _Smoothness;
uniform 	mediump float _Metallic;
uniform 	mediump vec4 _MainLightShadowData;
uniform lowp sampler2D _BaseMap;
uniform lowp sampler2D unity_Lightmap;
uniform highp sampler2D _MainLightShadowmapTexture;
uniform lowp samplerCube unity_SpecCube0;
varying highp vec2 vs_TEXCOORD0;
varying highp vec2 vs_TEXCOORD1;
varying mediump vec3 vs_TEXCOORD3;
varying mediump vec3 vs_TEXCOORD4;
varying highp vec4 vs_TEXCOORD7;
#define SV_Target0 gl_FragData[0]
float u_xlat0;
mediump vec3 u_xlat16_0;
lowp vec4 u_xlat10_0;
bool u_xlatb0;
mediump vec3 u_xlat16_1;
mediump vec3 u_xlat16_2;
mediump vec3 u_xlat16_3;
mediump vec3 u_xlat16_4;
lowp vec4 u_xlat10_4;
mediump vec3 u_xlat16_5;
mediump vec3 u_xlat16_6;
mediump float u_xlat16_7;
lowp vec3 u_xlat10_7;
mediump vec3 u_xlat16_8;
mediump float u_xlat16_10;
mediump vec3 u_xlat16_11;
mediump float u_xlat16_20;
mediump float u_xlat16_27;
mediump float u_xlat16_28;
mediump float u_xlat16_30;
void main()
{
    u_xlat0 = texture2D(_MainLightShadowmapTexture, vs_TEXCOORD7.xy).x;
    u_xlatb0 = u_xlat0<vs_TEXCOORD7.z;
    u_xlat16_1.x = (u_xlatb0) ? 0.0 : _MainLightShadowData.x;
    u_xlat16_10 = (-_MainLightShadowData.x) + 1.0;
    u_xlat16_1.x = u_xlat16_1.x + u_xlat16_10;
    u_xlatb0 = vs_TEXCOORD7.z>=1.0;
    u_xlat16_1.x = (u_xlatb0) ? 1.0 : u_xlat16_1.x;
    u_xlat16_10 = unity_LightData.z * unity_ProbesOcclusion.x;
    u_xlat16_1.x = u_xlat16_1.x * u_xlat16_10;
    u_xlat16_10 = dot(vs_TEXCOORD3.xyz, _MainLightPosition.xyz);
    u_xlat16_10 = clamp(u_xlat16_10, 0.0, 1.0);
    u_xlat16_1.x = u_xlat16_10 * u_xlat16_1.x;
    u_xlat16_1.xyz = u_xlat16_1.xxx * _MainLightColor.xyz;
    u_xlat16_28 = dot(vs_TEXCOORD4.xyz, vs_TEXCOORD4.xyz);
    u_xlat16_28 = max(u_xlat16_28, 6.10351563e-05);
    u_xlat16_28 = inversesqrt(u_xlat16_28);
    u_xlat16_2.xyz = vec3(u_xlat16_28) * vs_TEXCOORD4.xyz;
    u_xlat16_3.xyz = vs_TEXCOORD4.xyz * vec3(u_xlat16_28) + _MainLightPosition.xyz;
    u_xlat16_28 = dot((-u_xlat16_2.xyz), vs_TEXCOORD3.xyz);
    u_xlat16_28 = u_xlat16_28 + u_xlat16_28;
    u_xlat16_4.xyz = vs_TEXCOORD3.xyz * (-vec3(u_xlat16_28)) + (-u_xlat16_2.xyz);
    u_xlat16_28 = dot(vs_TEXCOORD3.xyz, u_xlat16_2.xyz);
    u_xlat16_28 = clamp(u_xlat16_28, 0.0, 1.0);
    u_xlat16_28 = (-u_xlat16_28) + 1.0;
    u_xlat16_28 = u_xlat16_28 * u_xlat16_28;
    u_xlat16_28 = u_xlat16_28 * u_xlat16_28;
    u_xlat16_2.x = (-_Smoothness) + 1.0;
    u_xlat16_11.x = (-u_xlat16_2.x) * 0.699999988 + 1.70000005;
    u_xlat16_11.x = u_xlat16_11.x * u_xlat16_2.x;
    u_xlat16_2.x = u_xlat16_2.x * u_xlat16_2.x;
    u_xlat16_11.x = u_xlat16_11.x * 6.0;
    u_xlat10_0 = textureCube(unity_SpecCube0, u_xlat16_4.xyz, u_xlat16_11.x);
    u_xlat16_11.x = u_xlat10_0.w + -1.0;
    u_xlat16_2.y = unity_SpecCube0_HDR.w * u_xlat16_11.x + 1.0;
    u_xlat16_2.xy = max(u_xlat16_2.xy, vec2(6.10351563e-05, 0.0));
    u_xlat16_11.x = log2(u_xlat16_2.y);
    u_xlat16_11.x = u_xlat16_11.x * unity_SpecCube0_HDR.y;
    u_xlat16_11.x = exp2(u_xlat16_11.x);
    u_xlat16_11.x = u_xlat16_11.x * unity_SpecCube0_HDR.x;
    u_xlat16_11.xyz = u_xlat10_0.xyz * u_xlat16_11.xxx;
    u_xlat16_30 = u_xlat16_2.x * u_xlat16_2.x + 1.0;
    u_xlat16_30 = float(1.0) / u_xlat16_30;
    u_xlat16_0.xyz = u_xlat16_11.xyz * vec3(u_xlat16_30);
    u_xlat16_11.x = (-_Metallic) * 0.959999979 + 0.959999979;
    u_xlat16_20 = (-u_xlat16_11.x) + _Smoothness;
    u_xlat16_20 = u_xlat16_20 + 1.0;
    u_xlat16_20 = clamp(u_xlat16_20, 0.0, 1.0);
    u_xlat10_4 = texture2D(_BaseMap, vs_TEXCOORD0.xy);
    u_xlat16_5.xyz = u_xlat10_4.xyz * _BaseColor.xyz + vec3(-0.0399999991, -0.0399999991, -0.0399999991);
    u_xlat16_5.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat16_5.xyz + vec3(0.0399999991, 0.0399999991, 0.0399999991);
    u_xlat16_6.xyz = vec3(u_xlat16_20) + (-u_xlat16_5.xyz);
    u_xlat16_6.xyz = vec3(u_xlat16_28) * u_xlat16_6.xyz + u_xlat16_5.xyz;
    u_xlat16_0.xyz = u_xlat16_0.xyz * u_xlat16_6.xyz;
    u_xlat10_7.xyz = texture2D(unity_Lightmap, vs_TEXCOORD1.xy).xyz;
    u_xlat16_6.xyz = u_xlat10_7.xyz + u_xlat10_7.xyz;
    u_xlat16_8.xyz = u_xlat10_4.xyz * _BaseColor.xyz;
    SV_Target0.w = u_xlat10_4.w * _BaseColor.w;
    u_xlat16_11.xyz = u_xlat16_11.xxx * u_xlat16_8.xyz;
    u_xlat16_0.xyz = u_xlat16_6.xyz * u_xlat16_11.xyz + u_xlat16_0.xyz;
    u_xlat16_28 = dot(u_xlat16_3.xyz, u_xlat16_3.xyz);
    u_xlat16_28 = max(u_xlat16_28, 6.10351563e-05);
    u_xlat16_28 = inversesqrt(u_xlat16_28);
    u_xlat16_3.xyz = vec3(u_xlat16_28) * u_xlat16_3.xyz;
    u_xlat16_28 = dot(_MainLightPosition.xyz, u_xlat16_3.xyz);
    u_xlat16_28 = clamp(u_xlat16_28, 0.0, 1.0);
    u_xlat16_3.x = dot(vs_TEXCOORD3.xyz, u_xlat16_3.xyz);
    u_xlat16_3.x = clamp(u_xlat16_3.x, 0.0, 1.0);
    u_xlat16_3.x = u_xlat16_3.x * u_xlat16_3.x;
    u_xlat16_28 = u_xlat16_28 * u_xlat16_28;
    u_xlat16_27 = max(u_xlat16_28, 0.100000001);
    u_xlat16_7 = u_xlat16_2.x * u_xlat16_2.x + -1.0;
    u_xlat16_7 = u_xlat16_3.x * u_xlat16_7 + 1.00001001;
    u_xlat16_28 = u_xlat16_7 * u_xlat16_7;
    u_xlat16_27 = u_xlat16_27 * u_xlat16_28;
    u_xlat16_7 = u_xlat16_2.x * 4.0 + 2.0;
    u_xlat16_28 = u_xlat16_2.x * u_xlat16_2.x;
    u_xlat16_27 = u_xlat16_27 * u_xlat16_7;
    u_xlat16_27 = u_xlat16_28 / u_xlat16_27;
    u_xlat16_28 = u_xlat16_27 + -6.10351563e-05;
    u_xlat16_28 = max(u_xlat16_28, 0.0);
    u_xlat16_28 = min(u_xlat16_28, 100.0);
    u_xlat16_2.xyz = vec3(u_xlat16_28) * u_xlat16_5.xyz + u_xlat16_11.xyz;
    SV_Target0.xyz = u_xlat16_2.xyz * u_xlat16_1.xyz + u_xlat16_0.xyz;
    return;
}

#endif
3                              