<Q                         DIRECTIONAL     �  #ifdef VERTEX
#version 300 es

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
in highp vec4 in_POSITION0;
in highp vec2 in_TEXCOORD0;
in highp vec4 in_COLOR0;
out highp vec2 vs_TEXCOORD0;
out highp vec4 vs_COLOR0;
vec4 u_xlat0;
vec4 u_xlat1;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat0 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat0.wwww + u_xlat1;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
    vs_COLOR0 = in_COLOR0;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp float;
precision highp int;
#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec4 _Time;
uniform 	vec4 _MainTex_ST;
uniform 	vec4 _TintColor;
uniform 	float _GradientUSpeed;
uniform 	float _GradientVSpeed;
uniform 	vec4 _Gradient_ST;
uniform 	float _NoiseAmount;
uniform 	vec4 _Distortion_ST;
uniform 	mediump float _DistortMainTexture;
uniform 	float _DistortionUSpeed;
uniform 	float _DistortionVSpeed;
uniform 	vec4 _MainTexMask_ST;
uniform 	float _GradientPower;
uniform 	float _ColorMultiplier;
uniform 	float _MainTextUSpeed;
uniform 	float _MainTextVSpeed;
uniform 	float _DoubleSided;
UNITY_LOCATION(0) uniform mediump sampler2D _Distortion;
UNITY_LOCATION(1) uniform mediump sampler2D _MainTex;
UNITY_LOCATION(2) uniform mediump sampler2D _Gradient;
UNITY_LOCATION(3) uniform mediump sampler2D _MainTexMask;
in highp vec2 vs_TEXCOORD0;
in highp vec4 vs_COLOR0;
layout(location = 0) out highp vec4 SV_Target0;
vec3 u_xlat0;
mediump vec3 u_xlat16_0;
vec3 u_xlat1;
mediump vec4 u_xlat16_1;
vec3 u_xlat2;
mediump vec3 u_xlat16_2;
vec2 u_xlat6;
mediump float u_xlat16_9;
void main()
{
    u_xlat0.xy = _Time.yy * vec2(_DistortionUSpeed, _DistortionVSpeed) + vs_TEXCOORD0.xy;
    u_xlat0.xy = u_xlat0.xy * _Distortion_ST.xy + _Distortion_ST.zw;
    u_xlat16_0.x = texture(_Distortion, u_xlat0.xy).x;
    u_xlat0.xy = u_xlat16_0.xx + (-vs_TEXCOORD0.xy);
    u_xlat6.xy = vec2(_NoiseAmount) * u_xlat0.xy + vs_TEXCOORD0.xy;
    u_xlat0.xy = u_xlat0.xy * vec2(_NoiseAmount);
    u_xlat0.xy = vec2(_DistortMainTexture) * u_xlat0.xy + vs_TEXCOORD0.xy;
    u_xlat0.xy = _Time.yy * vec2(_MainTextUSpeed, _MainTextVSpeed) + u_xlat0.xy;
    u_xlat0.xy = u_xlat0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    u_xlat16_1 = texture(_MainTex, u_xlat0.xy);
    u_xlat0.xy = _Time.yy * vec2(_GradientUSpeed, _GradientVSpeed) + u_xlat6.xy;
    u_xlat0.xy = u_xlat0.xy * _Gradient_ST.xy + _Gradient_ST.zw;
    u_xlat16_0.xyz = texture(_Gradient, u_xlat0.xy).xyz;
    u_xlat16_2.xyz = log2(u_xlat16_0.xyz);
    u_xlat2.xyz = u_xlat16_2.xyz * vec3(_GradientPower);
    u_xlat2.xyz = exp2(u_xlat2.xyz);
    u_xlat0.xyz = u_xlat16_0.xyz * u_xlat2.xyz;
    u_xlat0.xyz = u_xlat0.xyz * u_xlat16_1.www;
    u_xlat2.xy = vs_TEXCOORD0.xy * _MainTexMask_ST.xy + _MainTexMask_ST.zw;
    u_xlat16_9 = texture(_MainTexMask, u_xlat2.xy).w;
    u_xlat0.xyz = vec3(u_xlat16_9) * u_xlat0.xyz;
    u_xlat2.x = uintBitsToFloat(uint((gl_FrontFacing ? 0xffffffffu : uint(0)) & 1065353216u));
    u_xlat2.x = max(u_xlat2.x, _DoubleSided);
    u_xlat2.x = min(u_xlat2.x, 1.0);
    u_xlat0.xyz = u_xlat0.xyz * u_xlat2.xxx;
    u_xlat1.xyz = u_xlat16_1.xyz * vs_COLOR0.xyz;
    u_xlat2.xyz = _TintColor.xyz * vec3(vec3(_ColorMultiplier, _ColorMultiplier, _ColorMultiplier));
    u_xlat1.xyz = u_xlat1.xyz * u_xlat2.xyz;
    u_xlat1.xyz = u_xlat1.xyz + u_xlat1.xyz;
    u_xlat0.xyz = u_xlat0.xyz * u_xlat1.xyz;
    SV_Target0.xyz = u_xlat0.xyz + u_xlat0.xyz;
    u_xlat0.x = vs_COLOR0.w * _TintColor.w;
    u_xlat0.x = u_xlat16_1.w * u_xlat0.x;
    SV_Target0.w = u_xlat16_9 * u_xlat0.x;
    return;
}

#endif
                             $Globals�         _Time                            _MainTex_ST                      
   _TintColor                           _GradientUSpeed                   0      _GradientVSpeed                   4      _Gradient_ST                  @      _NoiseAmount                  P      _Distortion_ST                    `      _DistortMainTexture                   p      _DistortionUSpeed                     t      _DistortionVSpeed                     x      _MainTexMask_ST                   �      _GradientPower                    �      _ColorMultiplier                  �      _MainTextUSpeed                   �      _MainTextVSpeed                   �      _DoubleSided                  �          $Globals�         unity_ObjectToWorld                         unity_MatrixVP                   @             _Distortion                   _MainTex             	   _Gradient                   _MainTexMask             