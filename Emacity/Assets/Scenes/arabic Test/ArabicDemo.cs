﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ArabicSupport;

public class ArabicDemo : MonoBehaviour
{

    [SerializeField]
    InputField inputRef;

    [SerializeField]
    Text outputTextRef;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        outputTextRef.text = ArabicFixer.Fix(inputRef.text);
    }



}
