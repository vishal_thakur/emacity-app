﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileTarget : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(selfDestroy());
    }

    IEnumerator selfDestroy()
    {
        yield return new WaitForSeconds(1.5f);
        Destroy(gameObject);
    }
    

    private void OnCollisionEnter(Collision collision)
    {
        Destroy(gameObject);
    }
}
