﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActiveChatUnit : MonoBehaviour
{
    public string userName;
    [SerializeField] Text userNameTextRef;



    [SerializeField] GameObject newMessageNotificationObj;


    // Start is called before the first frame update
    void Start()
    {

        Messenger.AddListener<string, object>(Events.OnIncomingChatRequest, OnIncomingChatRequest);
    }

    private void OnDestroy()
    {

        Messenger.RemoveListener<string, object>(Events.OnIncomingChatRequest, OnIncomingChatRequest);
    }


    public void Initialize(string name)
    {
        userName = name;
        userNameTextRef.text = userName;
    }

    public void OnSelected()
    {
        Messenger.Broadcast<string>(Events.OnUserSelectedToChat, userName);
        newMessageNotificationObj.SetActive(false);
        //User has opened That Chat hence Hide the new message Icon
        //newMessageIconRef.SetActive(false);
    }

    void OnIncomingChatRequest(string str, object obj){
        if (userName != str || Globals.InChattingMode)
            return;
        

        //Show new Message Notification icon
        newMessageNotificationObj.SetActive(true);
    }

}
