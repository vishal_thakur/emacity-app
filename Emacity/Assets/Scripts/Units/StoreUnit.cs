﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class StoreUnit : MonoBehaviour
{
    [SerializeField]
    Image thumbnailImage;

    [SerializeField]
    Text storeNameTextRef;

    //[SerializeField]
    //string id;


    [Space(15) ,SerializeField]
    string name;

    [SerializeField]
    string imageURL;


    public void Initialize(GetStoresResponseRootObject obj)
    {
        //Save parameters
        //id = obj.Message[0].Id;
        storeNameTextRef.text = name = obj.Message[0].Name;
        imageURL = obj.Message[0].StoreImage;
        
        //Download and Load Image
        StartCoroutine(LoadImage());
    }


    IEnumerator LoadImage()
    {
        // Start a download of the given URL
        UnityWebRequest www = UnityWebRequestTexture.GetTexture(imageURL);

        // wait until the download is done
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.LogError(www.error);
        }
        else
        {
            Texture myTexture = DownloadHandlerTexture.GetContent(www);

            Texture2D texture2D = new Texture2D(myTexture.width, myTexture.height);
            texture2D.LoadImage(www.downloadHandler.data);

            // assign the downloaded image to sprite
            //www.LoadImageIntoTexture(texture);
            Rect rec = new Rect(0, 0, texture2D.width, texture2D.height);
            Sprite spriteToUse = Sprite.Create(texture2D, rec, new Vector2(0.5f, 0.5f), 100);
            thumbnailImage.sprite = spriteToUse;
        }

        www.Dispose();
        www = null;

        //Show Loading Untill The Stores Info is downloaded
        Messenger.Broadcast<PopupManager.Type>(Events.OnPopupEnd, PopupManager.Type.loading);

        StopAllCoroutines();

    }


    //------------------Callbacks-------------------

    public void OnSelected()
    {
        Messenger.Broadcast(Events.OnStoreSelected);
    }


}
