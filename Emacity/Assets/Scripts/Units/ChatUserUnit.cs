﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChatUserUnit : MonoBehaviour
{
    [SerializeField]
    public Text nameTextRef;
    
    [SerializeField]
    Image profileImageRef;

    [SerializeField]
    Button buttonRef;

    [SerializeField]
    bool isBlocked = false;

    [SerializeField]
    public GameObject newMessageIconRef;

    //private void Start()
    //{

    //    Messenger.AddListener<string, object>(Events.OnIncomingChatRequest, OnIncomingChatRequest);
    //}

    //private void OnDestroy()
    //{

    //    Messenger.RemoveListener<string, object>(Events.OnIncomingChatRequest, OnIncomingChatRequest);
    //}

    public void OnSelected()
    {
        Globals.newMessageUserName = "none";
        Messenger.Broadcast<string>(Events.OnUserSelectedToChat , nameTextRef.text);
        //User has opened That Chat hence Hide the new message Icon
        newMessageIconRef.SetActive(false);
        Debug.LogError("False");
    }

    public void Initialize(string name)
    {

        //Disable Button if the user is blocked
        if (isBlocked)
            buttonRef.interactable = false;
        //Load user's Image
        //Load user's Name

        nameTextRef.text = name;
    }


    public bool OnIncomingChatRequest(string userName)
    {
        //If this is the user that sent a new message
        if (nameTextRef.text == userName)
        {
            // Show new Message Notification icon
            newMessageIconRef.SetActive(true);
            Debug.LogError("true");
            return true;
        }
        else
            return false;

    }
}
