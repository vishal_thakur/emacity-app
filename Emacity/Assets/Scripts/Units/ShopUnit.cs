﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.EventSystems;
using UnityEngine.UI;
//using HighlightingSystem;


public class ShopUnit : MonoBehaviour
{
    //Active = Shop has a products webview
    //Inactive = Shop can be rented on the respective webview
    [SerializeField]
    bool isActive;

    [SerializeField]
    GetVendorsInfoResponseMessage myInfo;

    [SerializeField]
    MeshRenderer adPanelMeshFront;
    [SerializeField]
    MeshRenderer adPanelMeshLeft;
    [SerializeField]
    MeshRenderer adPanelMeshRight;

    [SerializeField]
    float shopNameRotationSpeed = 1.5f;

    [SerializeField]
    MoveDirection rotationDirection;
    Vector3 direction;


    [SerializeField]
    TMPro.TextMeshPro shopNameTMPRef;

    [SerializeField]
    Transform mainCameraRef;


    [SerializeField]
    bool reverseFace;


    [SerializeField]
    Button shopInteractionButton;


    [SerializeField]
    SpectrumController highlighterRef;
    
    [SerializeField]
    bool isInitialized = false;



    //Cached Initializing
    public void Initialize(ShopUnitData shopUnitData)
    {

        //Grab Shop Info as per The API CAllback info
        myInfo = shopUnitData.MyInfo;

        //Grab the main Camera Ref
        mainCameraRef = Camera.main.transform;


        shopInteractionButton.transform.parent.GetComponent<Canvas>().worldCamera = Camera.main;


        //shopInteractionButton.onClick.AddListener(delegate { OnInteracted(); });

        //Disable Shop Interaction Button initally
        shopInteractionButton.gameObject.SetActive(false);

        //Get and set ShopName Text Rotation parameter
        switch (rotationDirection)
        {
            case MoveDirection.Down:
                direction = Vector3.down;
                break;
            case MoveDirection.Up:
                direction = Vector3.up;
                break;
            case MoveDirection.Left:
                direction = Vector3.left;
                break;
            case MoveDirection.Right:
                direction = Vector3.right;
                break;
        }

        //Set Shop's  Active/Inactive flag
        isActive = IsShopActive(myInfo.VendorStatus);
        
        //Set Shop Interaction button's Text as per it's active status
        shopInteractionButton.GetComponentInChildren<Text>().text = isActive ? "Enter Shop" : "Rent Shop";
        
        //Update Shop name
        shopNameTMPRef.text = myInfo.VendorName;

        //Apply Textures
        adPanelMeshFront.material.SetTexture("_MainTex", shopUnitData.FrontPictureTexture);
        adPanelMeshLeft.material.SetTexture("_MainTex", shopUnitData.LeftPictureTexture);
        adPanelMeshRight.material.SetTexture("_MainTex", shopUnitData.RightPictureTexture);

        isInitialized = true;

        //Signal this Unit's Loading Successful
        Messenger.Broadcast<GetVendorsInfoResponseMessage, Texture, Texture, Texture>(Events.OnVendorUnitLoadedSuccessfully , null , null  , null ,null);


    }


    //API Initializing
    public void Initialize(GetVendorsInfoResponseMessage getVendorsInfoResponseMessage)
    {
        //Disable Shop Interaction Button initally
        shopInteractionButton.gameObject.SetActive(false);

        //Grab Shop Info as per The API CAllback info
        myInfo = getVendorsInfoResponseMessage;

        //Grab the main Camera Ref
        mainCameraRef = Camera.main.transform;

        shopInteractionButton.transform.parent.GetComponent<Canvas>().worldCamera = Camera.main;
        

        //shopInteractionButton.onClick.AddListener(delegate { OnInteracted(); });

        //Get and set ShopName Text Rotation parameter
        switch (rotationDirection)
        {
            case MoveDirection.Down:
                direction = Vector3.down;
                break;
            case MoveDirection.Up:
                direction = Vector3.up;
                break;
            case MoveDirection.Left:
                direction = Vector3.left;
                break;
            case MoveDirection.Right:
                direction = Vector3.right;
                break;
        }
        
        //Set Shop's  Active/Inactive flag
        isActive = IsShopActive(myInfo.VendorStatus);


        //Set Shop Interaction button's Text as per it's active status
        shopInteractionButton.GetComponentInChildren<Text>().text = isActive ? "Enter Shop" : "Rent Shop";


        //Update Shop name
        shopNameTMPRef.text = myInfo.VendorName;


        //Download and apply front vendor picture
        //if (!string.IsNullOrEmpty(myInfo.VendorFrontPicture) || !string.IsNullOrEmpty(myInfo.VendorLeftPicture) || !string.IsNullOrEmpty(myInfo.VendorRightPicture)
        StartCoroutine(DownloadAndApplyADTextures(myInfo.VendorFrontPicture, myInfo.VendorLeftPicture, myInfo.VendorRightPicture));
        //else
        //{
        //    //Signal this Unit's Loading Successful
        //    Messenger.Broadcast(Events.OnVendorUnitLoadedSuccessfully);
        //}

        ////Download and apply left vendor picture
        //if (!string.IsNullOrEmpty(myInfo.VendorLeftPicture))
        //    StartCoroutine(DownloadAndApplyADTexture(adPanelMeshLeft, myInfo.VendorLeftPicture, Globals.ShopAdDirection.left));
        //else
        //{
        //    //Signal this Unit's Loading Successful
        //    Messenger.Broadcast(Events.OnVendorUnitLoadedSuccessfully);
        //}

        ////Download and apply right vendor picture
        //if (!string.IsNullOrEmpty(myInfo.VendorRightPicture))
        //    StartCoroutine(DownloadAndApplyADTexture(adPanelMeshRight, myInfo.VendorRightPicture, Globals.ShopAdDirection.right));
        //else
        //{
        //    //Signal this Unit's Loading Successful
        //    Messenger.Broadcast(Events.OnVendorUnitLoadedSuccessfully);
        //}


    }



    void LateUpdate()
    {
        if (mainCameraRef == null)
            return;

        //rotates the Shop name and Shop interaction button to always face the camera while Camera Rotation
        Vector3 shopNametargetPos = shopNameTMPRef.transform.position + mainCameraRef.transform.rotation * (reverseFace ? Vector3.forward : Vector3.back);
        Vector3 shopInteractionButtontargetPos = shopInteractionButton.transform.position + mainCameraRef.transform.rotation * (reverseFace ? Vector3.forward : Vector3.back);
        Vector3 targetOrientation = mainCameraRef.transform.rotation * direction;
        //shopNameTMPRef.transform.LookAt(shopNametargetPos, targetOrientation);
        shopInteractionButton.transform.parent.LookAt(shopInteractionButtontargetPos, targetOrientation);
    }





    IEnumerator DownloadAndApplyADTextures(string urlFrontAdTexture, string urlLeftAdTexture, string urlRightAdTexture)
    {
        //Download Vendor Front AD
        UnityWebRequest wwwFrontAd = UnityWebRequestTexture.GetTexture(urlFrontAdTexture);
        yield return wwwFrontAd.SendWebRequest();
        Texture textureFrontAd = null;

        //--------------------Front Ad Panel
        if (wwwFrontAd.isNetworkError || wwwFrontAd.isHttpError)
        {
            Debug.Log(wwwFrontAd.error);
        }
        else
        {
            //make texture
            textureFrontAd = ((DownloadHandlerTexture)wwwFrontAd.downloadHandler).texture;

            //Apply It to The specified Ad Mesh
            adPanelMeshFront.material.SetTexture("_MainTex", textureFrontAd);
        }





        UnityWebRequest wwwLeftAd = UnityWebRequestTexture.GetTexture(urlLeftAdTexture);
        yield return wwwLeftAd.SendWebRequest();
        Texture textureLeftAd = null;
        //-------------------Left Ad Panel
        if (wwwLeftAd.isNetworkError || wwwLeftAd.isHttpError)
        {
            Debug.Log(wwwLeftAd.error);
        }
        else
        {
            //make texture
            textureLeftAd = ((DownloadHandlerTexture)wwwLeftAd.downloadHandler).texture;

            //Apply It to The specified Ad Mesh
            adPanelMeshLeft.material.SetTexture("_MainTex", textureLeftAd);
        }




        UnityWebRequest wwwRightAd = UnityWebRequestTexture.GetTexture(urlRightAdTexture);
        yield return wwwRightAd.SendWebRequest();
        Texture textureRightAd = null;
        //-------------------Right Ad Panel
        if (wwwRightAd.isNetworkError || wwwRightAd.isHttpError)
        {
            Debug.Log(wwwRightAd.error);
        }
        else
        {
            //make texture
            textureRightAd = ((DownloadHandlerTexture)wwwRightAd.downloadHandler).texture;

            //Apply It to The specified Ad Mesh
            adPanelMeshRight.material.SetTexture("_MainTex", textureRightAd);
        }

        isInitialized = true;

        //Signal this Unit's Loading Successful
        Messenger.Broadcast<GetVendorsInfoResponseMessage , Texture, Texture, Texture>(Events.OnVendorUnitLoadedSuccessfully , myInfo , textureFrontAd , textureLeftAd , textureRightAd);

        
        

        StopCoroutine(DownloadAndApplyADTextures(urlFrontAdTexture , urlLeftAdTexture, urlRightAdTexture));
    }
    


    public void OnCustomerApproachedEntrance()
    {
        //Show Enter Shop or Rent Shop Button
        shopInteractionButton.gameObject.SetActive(true);

        //if(myInfo.VendorStatus == null)
        //Set Color and Enable hilighter Object
        int random = Random.Range(0, 10);
        //if (highlighterRef != null)

        highlighterRef.gameObject.SetActive(true);

        //if (random > 7)
        //Inactive Shop
        if (myInfo.VendorStatus == null)
            highlighterRef.UpdateColor(Globals.inactiveHighlightColor);
        else//Inactive Shop
            highlighterRef.UpdateColor(Globals.activeHighlightColor);

    }


    public void OnCustomerLeftEntrance()
    {
        //Show Enter Shop or Rent Shop Button
        shopInteractionButton.gameObject.SetActive(false);

        //Set Color and Enable hilighter Object
        highlighterRef.gameObject.SetActive(false);
    }





    public void OnInteracted()
    {
        //Show WebView
        Messenger.Broadcast<string , string>(Events.OnShopInteracted, myInfo.VendorURL , myInfo.VendorId);

    }

    void OnReduceUserCoinsResponeReceived(string response)
    {
        Debug.LogError(response);
    }

    private void OnCollisionEnter(Collision collision)
    {
        //IF hit by a Spell, then reduce coins for the user , Also It does not work for Guest Users
        if (collision.gameObject.layer == 13 && !Globals.IsGuestLogin)
        {
            //Create a new SocialLogin Root Obj
            ReduceCoinsRequestRootObject reduceCoinsRequestRootObject = new ReduceCoinsRequestRootObject();

            //Set userID and Coins to Reduce
            reduceCoinsRequestRootObject.UserId = Globals.myUserID;
            reduceCoinsRequestRootObject.Coins = Globals.OnShopHitBySpellCoinsPenalty;

            string rawData = JsonUtility.ToJson(reduceCoinsRequestRootObject);
            
            //Subscribe To GetUserProfile Response Event
            Messenger.AddListener<string>(Events.OnRESTAPICallback, OnReduceUserCoinsResponeReceived);

            //Call the API
            RestAPICallManager.CallRESTAPI(BestHTTP.HTTPMethods.Post, Globals.ReduceUserCoinsURL , rawData);

            //Debug.LogError("Calling Rest API");
        }
    }

    #region Utility
    //Check wether the Shop is Active or inactive
    public static bool IsShopActive(string vendorStatus)
    {
        if (vendorStatus == "0")
            return false;
        else
            return true;
    }
    #endregion
}
