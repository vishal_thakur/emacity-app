﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.EventSystems;

public class AdvertisementUnit : MonoBehaviour
{

    [SerializeField]
    public List<GetAdvertisementsResponeMessage> allAdvertisements;

    public List<Texture> adTextures;


    [SerializeField]
    MeshRenderer adPanelMesh;

    [SerializeField]
    YoutubePlayer youtubePlayerRef;

    [SerializeField]
    GameObject advertisementInteractionButton;

    [SerializeField]
    SpectrumController highlighterRef;

    [SerializeField]
    Transform mainCameraRef;

    [SerializeField]
    MoveDirection rotationDirection;
    Vector3 direction;

    [SerializeField]
    bool reverseFace;

    [SerializeField]
    int advertisementTextureDownloadProgress = 0;

    [SerializeField]
    float youtubeVideoDuration = 0;

    [SerializeField]
    bool isInitialized = false;


    public void Initialize()
    {
        advertisementInteractionButton.transform.parent.GetComponent<Canvas>().worldCamera = Camera.main;

        //Grab the main Camera Ref
        mainCameraRef = Camera.main.transform;

        //Disable Advertisement Interaction Button initally
        advertisementInteractionButton.gameObject.SetActive(false);

        //Get and set ShopName Text Rotation parameter
        switch (rotationDirection)
        {
            case MoveDirection.Down:
                direction = Vector3.down;
                break;
            case MoveDirection.Up:
                direction = Vector3.up;
                break;
            case MoveDirection.Left:
                direction = Vector3.left;
                break;
            case MoveDirection.Right:
                direction = Vector3.right;
                break;
        }

        LoadAdvertisementTextures();
    }

    void LoadAdvertisementTextures()
    {
        try
        {
            if(allAdvertisements.Count > 0)
            StartCoroutine(DownloadAdvertisementTexture(allAdvertisements[advertisementTextureDownloadProgress].Image));
            else
                //Signal this Unit's Loading Successful
                Messenger.Broadcast<List<GetAdvertisementsResponeMessage>, List<Texture>>(Events.OnAdvertisementUnitLoadedSuccessfully, allAdvertisements, new List<Texture>());
        }
        catch(System.Exception e)
        {
            Debug.LogError(e.ToString());
        }
    }



    void LateUpdate()
    {
        if (mainCameraRef == null)
            return;


        ////Get and set ShopName Text Rotation parameter
        //switch (rotationDirection)
        //{
        //    case MoveDirection.Down:
        //        direction = Vector3.down;
        //        break;
        //    case MoveDirection.Up:
        //        direction = Vector3.up;
        //        break;
        //    case MoveDirection.Left:
        //        direction = Vector3.left;
        //        break;
        //    case MoveDirection.Right:
        //        direction = Vector3.right;
        //        break;
        //}

        //rotates the Shop name and Shop interaction button to always face the camera while Camera Rotation
        //Vector3 shopNametargetPos = shopNameTMPRef.transform.position + mainCameraRef.transform.rotation * (reverseFace ? Vector3.forward : Vector3.back);
        Vector3 shopInteractionButtontargetPos = advertisementInteractionButton.transform.position + mainCameraRef.transform.rotation * (reverseFace ? Vector3.forward : Vector3.back);
        Vector3 targetOrientation = mainCameraRef.transform.rotation * direction;
        //shopNameTMPRef.transform.LookAt(shopNametargetPos, targetOrientation);
        advertisementInteractionButton.transform.parent.LookAt(shopInteractionButtontargetPos, targetOrientation);
    }

    #region Callbacks

    void OnAdTextureLoadComplete()
    {
        //Advertisement Texture Unavailable
        if(adTextures.Count == 0)
        {
            //Start Slideshow
            StartCoroutine(AdvertisementSlideShow());

            isInitialized = true;
            //Signal this Unit's Loading Successful
            Messenger.Broadcast<List<GetAdvertisementsResponeMessage>, List<Texture>>(Events.OnAdvertisementUnitLoadedSuccessfully, allAdvertisements, new List<Texture>());
            return;
        }

        //if Ready then Start SlideShow 
        //else wait for Remaining Advertisement textures to be downloaded
        if (adTextures.Count >= allAdvertisements.Count){
            if (adTextures.Count == 1)
            {
                if (adTextures[0] != null)
                    adPanelMesh.material.SetTexture("_MainTex", adTextures[0]);

            }
            else
            {
                //Start Slideshow
                StartCoroutine(AdvertisementSlideShow());
            }

            isInitialized = true;
            //Signal this Unit's Loading Successful
            Messenger.Broadcast<List<GetAdvertisementsResponeMessage>, List<Texture>>(Events.OnAdvertisementUnitLoadedSuccessfully, allAdvertisements, new List<Texture>());
        }
        else
        {
            advertisementTextureDownloadProgress++;
            LoadAdvertisementTextures();
        }
    }
    
    public void OnCustomerApproachedAdvertisement()
    {
        //Show Interaction Button
        advertisementInteractionButton.gameObject.SetActive(true);
        
        //Enable Highlighter
        highlighterRef.gameObject.SetActive(true);
        
        highlighterRef.UpdateColor(Globals.activeHighlightColor);

    }


    public void OnCustomerLeftAdvertisement()
    {
        //Hide Interaction button
        advertisementInteractionButton.gameObject.SetActive(false);

        //Disable Highlighter
        highlighterRef.gameObject.SetActive(false);
    }
    
    public void OnInteracted()
    {
        //Show WebView
        Messenger.Broadcast(Events.OnAdvertisementInteracted);

    }
    #endregion







    #region Co-Routines
    IEnumerator DownloadAdvertisementTexture(string url)
    {
        Texture myTexture = null;
        if (!string.IsNullOrEmpty(url))
        {
            //Download Vendor Image
            UnityWebRequest www = UnityWebRequestTexture.GetTexture(url);

            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.LogError(www.error + "------------" + url);
            }
            else
            {
                myTexture = ((DownloadHandlerTexture)www.downloadHandler).texture;

                //Add new Texture to the AdTextures list
                adTextures.Add(myTexture);
            }
        }
        //else
        //{
        //    //Add new Texture to the AdTextures list
        //    adTextures.Add(myTexture);

        //}
        OnAdTextureLoadComplete();

        StopCoroutine(DownloadAdvertisementTexture(url));
    }

    IEnumerator AdvertisementSlideShow()
    {
        int adIndex = 0;
        int adTextureIndex = 0;

        //Image Slide Show
        while (true)
        {
            yield return null;

            //decide Whether The next Advertisement Type is Image or Video
            //Advertisement Type is Image
            if (allAdvertisements[adIndex].type == Globals.AdvertisementType.image)
            {
                //Disable Youtube Mesh as we want to show a Texture Now
                youtubePlayerRef.videoPlayer.targetMaterialRenderer.gameObject.SetActive(false);

                //Apply It to The Ad Mesh
                adPanelMesh.material.SetTexture("_MainTex", adTextures[adTextureIndex]);

                //wait for Specified Seconds
                yield return new WaitForSeconds(Globals.AdvertisementPanelSlideshowDelay);

                adTextureIndex++;

                //Index Overflow Prevention
                if (adTextureIndex > adTextures.Count - 1)
                {
                    adTextureIndex = 0;
                }
            }
            else if(allAdvertisements[adIndex].type == Globals.AdvertisementType.video)//Advertisement Type is Video
            {   
                //check if URL is valid
                if (!string.IsNullOrEmpty(allAdvertisements[adIndex].DownloadURL))
                {
                    //Enable Youtube Mesh as we want to show a Video Now
                    youtubePlayerRef.videoPlayer.targetMaterialRenderer.gameObject.SetActive(true);

                    //If New URL = Old URL or It is the same video as the last video
                    if (youtubePlayerRef.youtubeUrl == allAdvertisements[adIndex].DownloadURL)
                    {
                        //Wait for the Video Duration
                        yield return new WaitForSeconds(youtubeVideoDuration);
                    }
                    else//This is a new Video
                    {
                        //So Update Youtube URL
                        youtubePlayerRef.youtubeUrl = allAdvertisements[adIndex].DownloadURL;
                     
                        //Reset The Youtube Duration
                        youtubeVideoDuration = 0;

                        //For the First Time This just Enables The Youtube player, which will play when it's Game Object is set to active
                        if (!youtubePlayerRef.gameObject.activeSelf)
                            youtubePlayerRef.gameObject.SetActive(true);
                        
                        //Wait for a Set Duration for Video To load
                        yield return new WaitForSeconds(5);

                        //By this Time The Video Duration must have been Loaded, so Grab the Video Duration
                        youtubeVideoDuration = (float)youtubePlayerRef.videoPlayer.length;

                        //Wait for the Video Duration
                        if (youtubeVideoDuration > 5)
                            yield return new WaitForSeconds(youtubeVideoDuration - 5);
                        else
                            yield return null;
                    }
                }
                else
                {
                    Debug.LogError("No Youtube Url To PLay!!");
                }
            }


            //Increment Advertisement Index
            adIndex++;

            //Index Overflow Prevention
            if (adIndex > allAdvertisements.Count - 1)
            {
                adIndex = 0;
            }
        }
    }
    #endregion
}
