﻿using System.Collections;
using System.Collections.Generic;
//using UnityEditor.Animations;
using UnityEngine;

public class AvatarUnit : MonoBehaviour
{

    //[SerializeField]
    //public RuntimeAnimatorController RuntimeAnimatorControllerRef;
    //[SerializeField]
    //public Avatar AvatarRef;

    public Globals.Avatar myAvatar;


    [Space(10) , SerializeField] SkinnedMeshRenderer[] hair;
    [SerializeField] SkinnedMeshRenderer[] top;
    [SerializeField] SkinnedMeshRenderer[] bottom;
    [SerializeField] SkinnedMeshRenderer[] shoe;

    
    public float spellAnimationTimeWait;
    


    void OnEnable()
    {
        Messenger.AddListener<string>(Events.OnAvatarCustomizationOptionSelected, OnAvatarCustomizationOptionSelected);
        //Globals.SpellDuration = spellAnimationTimeWait;

    }

    void OnDisable()
    {
        Messenger.RemoveListener<string>(Events.OnAvatarCustomizationOptionSelected, OnAvatarCustomizationOptionSelected);

    }


    public void OnAvatarCustomizationOptionSelected(string customizationTypeStringValue) {


        switch (Globals.ActiveCustomizationOption) {
            //Hair Customization Options
            case Globals.CustomizationOptionType.hair:
                if (myAvatar == Globals.Avatar.kevin_male)
                {
                    foreach (SkinnedMeshRenderer mesh in hair)
                        if (ColorUtility.TryParseHtmlString(customizationTypeStringValue, out Color color))
                            mesh.materials[4].SetColor("_Color", color);
                }
                else
                {
                    foreach (SkinnedMeshRenderer mesh in hair)
                        if (ColorUtility.TryParseHtmlString(customizationTypeStringValue, out Color color))
                            mesh.material.SetColor("_Color", color);
                }
                break;

            //Top Customization Options
            case Globals.CustomizationOptionType.top:
                foreach (SkinnedMeshRenderer mesh in top)
                    if (ColorUtility.TryParseHtmlString(customizationTypeStringValue, out Color color))
                        mesh.material.SetColor("_Color", color);
                break;

            //Bottom Customization Options
            case Globals.CustomizationOptionType.bottom:
                foreach (SkinnedMeshRenderer mesh in bottom)
                    if (ColorUtility.TryParseHtmlString(customizationTypeStringValue, out Color color))
                        mesh.material.SetColor("_Color", color);
                break;


            //Shoe Customization Options
            case Globals.CustomizationOptionType.shoe:
                if (myAvatar == Globals.Avatar.charming_male || myAvatar == Globals.Avatar.kevin_male)
                {
                    foreach (SkinnedMeshRenderer mesh in shoe)
                        if (ColorUtility.TryParseHtmlString(customizationTypeStringValue, out Color color))
                            mesh.materials[1].SetColor("_Color" ,color);
                    
                }
                else
                {
                    foreach (SkinnedMeshRenderer mesh in shoe)
                        if (ColorUtility.TryParseHtmlString(customizationTypeStringValue, out Color color))
                            mesh.material.SetColor("_Color", color);
                }
                break;
        }
    }
}
