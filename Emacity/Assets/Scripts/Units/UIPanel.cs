﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIPanel : MonoBehaviour
{
    public Globals.UIPanel UIPanelType;



    private void OnEnable()
    {
        if (UIPanelType == Globals.UIPanel.avatarSelection)
            Messenger.Broadcast(Events.OnAvatarCustomizationFinished);
    }

    private void OnDisable()
    {
        //if (UIPanelType == Globals.UIPanel.avatarSelection)
        //    Messenger.Broadcast(Events.OnAvatarCustomizationFinished);
    }
}
