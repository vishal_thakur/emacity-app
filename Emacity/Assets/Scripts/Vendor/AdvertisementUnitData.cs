﻿//using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdvertisementUnitData
{

    public AdvertisementUnitData(List<GetAdvertisementsResponeMessage> myInfo, List<Texture> adTextures)
    {
        MyInfo = myInfo;
        AdTextures = adTextures;
    }

    public List<GetAdvertisementsResponeMessage> MyInfo;

    public List<Texture> AdTextures;
}
