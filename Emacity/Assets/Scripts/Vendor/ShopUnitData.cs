﻿//using System.Collections;
//using System.Collections.Generic;
using UnityEngine;

public class ShopUnitData
{

    public ShopUnitData(GetVendorsInfoResponseMessage myInfo, Texture frontPictureTexture , Texture leftPictureTexture, Texture rightPictureTexture)
    {
        MyInfo = myInfo;
        FrontPictureTexture = frontPictureTexture;
        LeftPictureTexture = leftPictureTexture;
        RightPictureTexture = rightPictureTexture;
    }

    public GetVendorsInfoResponseMessage MyInfo;

    public Texture LeftPictureTexture;
    public Texture RightPictureTexture;
    public Texture FrontPictureTexture;
}
