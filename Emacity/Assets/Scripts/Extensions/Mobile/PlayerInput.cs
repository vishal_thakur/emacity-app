﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class PlayerInput 
{
	static Vector3 INITIAL_POS = new Vector3(-100000,-100000, -100000);
	static Vector3 _lastMousePosition = INITIAL_POS;
	static Vector2 _deltaPosition = Vector2.zero;


	/// <summary>
	/// Number of touches.
	/// </summary>
	/// <value>The touch count.</value>
	public static int touchCount
	{
		get
		{ 
			#if UNITY_EDITOR
			if( Input.GetMouseButton(0) )
				return 1;			
			if( Input.GetMouseButton(1) )
				return 2;
			return 0;
			#else
			return Input.touchCount;
			#endif
		}
	}

	public static Vector2 DeltaPoisiton( int touch )
	{
		#if UNITY_EDITOR
		if( Input.GetMouseButton(0) && touch == 1 || Input.GetMouseButton(1) && touch == 2)
		{
			if( Phase == TouchPhase.Moved )
				return _deltaPosition;
			else
				return Vector2.zero;
		}
		else 
			return Vector2.zero;
		#else
		return Input.GetTouch(touch).deltaPosition;
		#endif
	}


	public static bool Active
	{
		get
		{
			#if UNITY_EDITOR
			return Input.GetMouseButton(0) || Input.GetMouseButton(1);
			#else
			return Input.touchCount > 0;
			#endif
		}
	}

	public static Vector2 Position
	{
		get
		{
			#if UNITY_EDITOR
			return new Vector2(Input.mousePosition.x, Input.mousePosition.y);
			#else
			return Input.touches[0].position;
			#endif
		}
	}

	public static TouchPhase Phase
	{
		get
		{
			#if UNITY_EDITOR
			if(Input.GetKeyDown(KeyCode.Mouse0) && !EventSystem.current.IsPointerOverGameObject())
			{
				if(_lastMousePosition == INITIAL_POS)
				{
					_lastMousePosition = Input.mousePosition;
					return TouchPhase.Began;
				}
			}
			else if( Input.GetKey(KeyCode.Mouse0) && !EventSystem.current.IsPointerOverGameObject())
			{
				if(Input.mousePosition != _lastMousePosition)
				{
					_deltaPosition = Input.mousePosition - _lastMousePosition;
					return TouchPhase.Moved;
				}
				else
					return TouchPhase.Stationary;
			}
			else if(Input.GetKeyUp(KeyCode.Mouse0))
			{
				_lastMousePosition = INITIAL_POS;
				return TouchPhase.Ended;
			}
			return TouchPhase.Ended;

			#else
			if(Active && !EventSystem.current.IsPointerOverGameObject(Input.touches[0].fingerId))
				return Input.touches[0].phase;
			else 
				return TouchPhase.Ended;
			#endif
		}
	}
}
