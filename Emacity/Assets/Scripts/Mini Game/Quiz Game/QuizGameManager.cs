﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class QuizGameManager : MonoBehaviour
{
    #region Parameters
    [SerializeField]
    GameObject QuizPopup;

    [SerializeField]
    GameObject QuizResultPopup;

    [SerializeField]
    Text QuizResultTextRef;

    [SerializeField]
    string answer;

    [SerializeField]
    string rewardCoins;

    [SerializeField]
    Text QuestionTextRef;

    [SerializeField]
    Text[] QuizOptions;

    [SerializeField]
    AudioSource QuizGameWonAudioRef;

    [SerializeField]
    //RestAPICallManager RestAPICallManagerRef;
    #endregion


    void Start()
    {

        Messenger.AddListener(Events.OnMainMenuLoadedCompletely, OnSceneLoadedCompletely);
        Messenger.AddListener(Events.OnGameSceneLoadedCompletely, OnSceneLoadedCompletely);

        Messenger.AddListener(Events.OnQuizGameInitialized, OnQuizGameInitialized);
    }


    void OnDestroy()
    {
        Messenger.RemoveListener(Events.OnQuizGameInitialized, OnQuizGameInitialized);
    }


    void OnSceneLoadedCompletely()
    {
        //Debug.LogError("On scene Loaded Completly. QuizGame Status = " + Globals.QuizGameActive);

        if (Globals.QuizGameActive)
        {
            OnQuizGameInitialized();
        }

        ////Scene based UnSubscription to The Mainmenu | GameScene LoadedCompletly
        //if (SceneManager.GetActiveScene().name == Globals.SceneNames.MainMenu.ToString())
        //{
        //    Messenger.RemoveListener(Events.OnMainMenuLoadedCompletely, OnSceneLoadedCompletely);
        //}
        //else if (SceneManager.GetActiveScene().name == Globals.SceneNames.GameScene.ToString())
        //{
        //    Messenger.RemoveListener(Events.OnGameSceneLoadedCompletely, OnSceneLoadedCompletely);
        //}

        Messenger.RemoveListener(Events.OnMainMenuLoadedCompletely, OnSceneLoadedCompletely);
        Messenger.RemoveListener(Events.OnGameSceneLoadedCompletely, OnSceneLoadedCompletely);
    }


    #region Core
    public void SelectOption(Text optionText)
    {

        QuizPopup.SetActive(false);
        QuizResultPopup.SetActive(true);


        //if right answer 
        //Show Quiz results Window with Reward text
        //Reward player
        if (optionText.text == answer)
        {
            QuizGameWonAudioRef.Play();

            QuizResultTextRef.text = "Correct Answer. Congratulations! You have earned " + rewardCoins + " coins.";
        }
        //else
        else
        {
            //Show Quiz results Window with Better Luck Next Time
            QuizResultTextRef.text = "Oops, Wrong Answer, Better luck next time!";

            rewardCoins = null;
        }

        //Mark Quiz Game As Inactive Now
        Globals.QuizGameActive = false;
    }



    //Add reward to player's Total Coins using API
    public void CollectReward()
    {
        //Get Int value by parsing the reward String
        if(int.TryParse(rewardCoins , out int result) && !Globals.IsGuestLogin)
        {
            //Subscribe To AddUserCoins Response Event
            Messenger.AddListener<string>(Events.OnRESTAPICallback, onAddUserCoinsResponseRecieved);

            //Create new AddUserCoins Obj and Feed values to it
            AddUserCoinsRequestRootObject addUserCoinsRequestRootObject = new AddUserCoinsRequestRootObject();

            addUserCoinsRequestRootObject.UserId = Globals.myUserID;

            //Set Coins
            addUserCoinsRequestRootObject.Coins = result;

            //Prepare Raw data
            string rawData = JsonUtility.ToJson(addUserCoinsRequestRootObject);

            //Call the API
            RestAPICallManager.CallRESTAPI(BestHTTP.HTTPMethods.Post, Globals.AddUserCoins, rawData);

            //Show Loading 
            Messenger.Broadcast<string, PopupManager.Type>(Events.OnPopupRequired, "Fetching Reward..", PopupManager.Type.loading);
        }
        else//Could not parse Hence Donot add coins
        {
            Debug.LogError("Unable to parse Reward Coins String!!");
            //Disable Quiz Result Popup
            QuizResultPopup.SetActive(false);
        }
        
    }
    #endregion




    #region Event Callbacks
    void onAddUserCoinsResponseRecieved(string responseText)
    {
        //parse Response Json
        //Debug.LogError(responseText);

        //Try To Parse json Response
        try
        {
            AddUserCoinsResponseRootObject addUserCoinsResponseRootObject = JsonUtility.FromJson<AddUserCoinsResponseRootObject>(responseText);
            
            //Successfully Added Coins To user's Info
            if (addUserCoinsResponseRootObject.Success) { 
                //Remove Loading Screen
                Messenger.Broadcast<PopupManager.Type>(Events.OnPopupEnd, PopupManager.Type.loading);
                
                //Remove Loading Screen
                Messenger.Broadcast<int>(Events.OnQuizGameRewardRecieved, int.Parse(rewardCoins));
            }
            else
            {
                //Remove Loading Screen
                Messenger.Broadcast<PopupManager.Type>(Events.OnPopupEnd, PopupManager.Type.loading);

                Debug.LogError("Unable to add coins = " + addUserCoinsResponseRootObject.Message);
                //Intuit User About not able to add Coins
                Messenger.Broadcast<string, PopupManager.Type>(Events.OnPopupRequired, "Unable to collect reward!", PopupManager.Type.notification);
            }
        }
        catch (System.Exception e)
        {
            //Remove Loading Screen
            Messenger.Broadcast<PopupManager.Type>(Events.OnPopupEnd, PopupManager.Type.loading);

            Debug.LogError("Unable to add coins = " + e);
            //Intuit User About not able to add Coins
            Messenger.Broadcast<string,PopupManager.Type>(Events.OnPopupRequired, "Unable to collect reward!" , PopupManager.Type.notification);
        }

        //Unsubscibe Listener
        Messenger.RemoveListener<string>(Events.OnRESTAPICallback, onAddUserCoinsResponseRecieved);

        //Disable Quiz Result Popup
        QuizResultPopup.SetActive(false);
    }



    public void OnQuizGameInitialized()
    {

        QuizPopup.SetActive(true);

        //Save Question
        QuestionTextRef.text = Globals.quizGameObj.Question;

        //Save Options
        for (int i = 0; i < Globals.quizGameObj.Options.Length; i++)
        {
            QuizOptions[i].text = Globals.quizGameObj.Options[i];
        }

        //Save Answer
        answer = Globals.quizGameObj.Answer;

        //Save Reward Coins
        rewardCoins = Globals.quizGameObj.Coins;
    }


    public void OnCanceled()
    {
        QuizPopup.SetActive(Globals.QuizGameActive = false);

        //Mark Quiz Game As Inactive Now
        //Globals.QuizGameActive = false;
    }
    #endregion
}
