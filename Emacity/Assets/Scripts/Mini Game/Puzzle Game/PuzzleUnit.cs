﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleUnit : MonoBehaviour
{

    //[SerializeField]
    //public GetPuzzleResponseMessage GetPuzzleResponseMessageRef;


    [SerializeField]
    public Vector3 myPosition;

    [SerializeField]
    public int myNumber;

    [SerializeField]
    public string myIndex;

    [SerializeField]
    Animation animationRef;

    [SerializeField]
    float rotationAngle;

    [SerializeField]
    float rotationSpeed;

    public void Start()
    {
        transform.position = myPosition;
    }


    public void Initialize()
    {

        animationRef.GetComponent<MeshRenderer>().enabled = true;
        animationRef.GetComponent<SphereCollider>().enabled = true;
        //myPosition = Globals.PositionFromString(obj.Position);

        //Show Visuals and Enable Collider
    }

    public void OnCollected()
    {
        StartCoroutine(OnCollectedBehaviour());
    }

    IEnumerator OnCollectedBehaviour()
    {
        //Show On Collected Animation
        animationRef.Play();

        yield return new WaitForSeconds(1.5f);

        //Destroy after Animation Complete
        DestroyImmediate(gameObject);
    }



    private void Update()
    {
        animationRef.transform.Rotate(transform.position , rotationAngle * Time.deltaTime * rotationSpeed);
    }
}
