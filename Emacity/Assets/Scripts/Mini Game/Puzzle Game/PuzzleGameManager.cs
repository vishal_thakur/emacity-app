﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;

public class PuzzleGameManager : MonoBehaviour
{


    [Header("Puzzle Popup Params")]
    [SerializeField]
    GameObject puzzlePopupRef;

    [SerializeField]
    Text timeRemainingTextRef;

    [SerializeField]
    Text piecesRemainingTextRef;


    [SerializeField]
    Image PuzzleImageRef;



    [Header("Puzzle result Popup Params"), Space(20)]
    [SerializeField] GameObject PuzzleResultPopupRef;
    [SerializeField] Text PuzzleGameWonTextRef;
    [SerializeField] Text PuzzleGameLostTextRef;





    [Space(20)]
    //[SerializeField]
    //RestAPICallManager RestAPICallManagerRef;


    [SerializeField]
    PuzzleUnit PuzzleUnitPrefabRef;





    [SerializeField]
    GetPuzzleResponseRootObject getPuzzleResponseRootObject;
    

    [SerializeField]
    List<PuzzleUnit> FinalPuzzlePieces = new List<PuzzleUnit>();

    [SerializeField]
    AudioSource PuzzleGameBGAudioRef;
    [SerializeField]
    AudioSource PuzzleGameWonAudioRef;



    //Just for testing Purpose
    //private void Update()
    //{
    //    if (Input.GetKeyUp(KeyCode.Space))
    //    {
    //        Globals.PuzzleGameDeadlineTime += (int)Time.time;
    //        Globals.PuzzleGameActive = true;
    //        OnPuzzleGameInitialized();
    //    }

    //}


    void Start()
    {
        Messenger.AddListener(Events.OnMainMenuLoadedCompletely, OnSceneLoadedCompletely);
        Messenger.AddListener(Events.OnGameSceneLoadedCompletely, OnSceneLoadedCompletely);
        Messenger.AddListener(Events.OnPuzzleGameInitialized, OnPuzzleGameInitialized);
    }

    // Start is called before the first frame update
    void OnSceneLoadedCompletely(){
        if (Globals.PuzzleGameActive)
        {
            OnPuzzleGameInitialized();

            //We donot want to recive this Broadcast While not in any other but the game scene
            if (SceneManager.GetActiveScene().name == Globals.SceneNames.GameScene.ToString())
                Messenger.AddListener(Events.OnAllPuzzlePiecesCollected, OnAllPuzzlePiecesCollected);
        }

        Messenger.RemoveListener(Events.OnMainMenuLoadedCompletely, OnSceneLoadedCompletely);
        Messenger.RemoveListener(Events.OnGameSceneLoadedCompletely, OnSceneLoadedCompletely);
    }

    // Start is called before the first frame update
    void OnDestroy()
    {
        //Messenger.RemoveListener(Events.OnMainMenuLoadedCompletely, OnSceneLoadedCompletely);
        //Messenger.RemoveListener(Events.OnGameSceneLoadedCompletely, OnSceneLoadedCompletely);
        Messenger.RemoveListener(Events.OnPuzzleGameInitialized, OnPuzzleGameInitialized);
    }




    //Event Callbacks
    void OnPuzzleGameInitialized()
    {
        //Play Puzzle Game Sound
        PuzzleGameBGAudioRef.Play();

        StartCoroutine(LoadPuzzleImage());
        
        //We want to Call GetPuzzle API only in the Game scene as there is no use of that information in any other scene
        if (SceneManager.GetActiveScene().name == Globals.SceneNames.GameScene.ToString())
        {
            //Subscribe To GetPuzzleGame Info Response Event
            Messenger.AddListener<string>(Events.OnRESTAPICallback, OnGetPuzzleGameInfoResponseRecieved);

            //Grab The Puzzle Game info from the GetPuzzle API
            RestAPICallManager.CallRESTAPI(BestHTTP.HTTPMethods.Get, Globals.GetPuzzleGameInfoURL);
        }

        //Show the Puzzle update which are The puzzle pieces collected and Time Remaning for the puzzle game to end
        StartCoroutine(PuzzleGameUpdate());


    }


    void PuzzleGameOver() {

        //Stop puzzle Game Sound
        PuzzleGameBGAudioRef.Stop();

        this.StopAllCoroutines();

        //Reset Set Globals param as false
        Globals.PuzzlePiecesToSpawnNumbers = null;
        Globals.PuzzleGameDeadlineTime = 0;
        Globals.PuzzlePiecesRemainingToCollect = 0;
        Globals.PuzzleGameActive = false;


        //Disable The Puzzle Popup 
        puzzlePopupRef.SetActive(false);

        PuzzleResultPopupRef.SetActive(true);
        
        //Clear list
        //AllPuzzlePieces.Clear();
        FinalPuzzlePieces.Clear();


        //Unsubscribe  from PuzzlePiece collected Event Callback
        Messenger.RemoveListener(Events.OnAllPuzzlePiecesCollected, OnAllPuzzlePiecesCollected);
    }


    //Won
    void OnAllPuzzlePiecesCollected()
    {
        PuzzleGameOver();
        //Debug.LogError("Won");
        //Enable Won text and Disable Lost Text to Intuit The player
        PuzzleGameWonTextRef.gameObject.SetActive(true);
        PuzzleGameLostTextRef.gameObject.SetActive(false);
    }


    void OnGetPuzzleGameInfoResponseRecieved(string responseText)
    {
        try{
            getPuzzleResponseRootObject = JsonUtility.FromJson<GetPuzzleResponseRootObject>(responseText);

            if (getPuzzleResponseRootObject.Success)
            {
                //We have puzzle Piece Positions, now choose any random 10 out of them and place puzzle pieces in them
                StartCoroutine(SpawnPuzzleUnits(getPuzzleResponseRootObject));
            }
            else
            {
                Debug.LogError("Issue here = ");
                Messenger.Broadcast<string, PopupManager.Type>(Events.OnPopupRequired, "Unable to fetch Puzzle Game. " , PopupManager.Type.notification);
            }

        }
        catch(System.Exception e) {
            Debug.LogError("Issue here = " + e);
            Messenger.Broadcast<string, PopupManager.Type>(Events.OnPopupRequired, "Unable to fetch Puzzle Game. " , PopupManager.Type.notification);
        }
    }


    //The update method where the Puzzle Pieces remaining And Puzzle Time remaining are updated and hence are reflected in the UI
    IEnumerator PuzzleGameUpdate()
    {
        //Show the Popup with the Puzzle Game Information
        puzzlePopupRef.SetActive(true);
        //Debug.LogError("Start Time" + Time.time);
        //Debug.LogError("Puzzle started and Time remaing " + Globals.PuzzleTimeRemainig.ToString());


        int remainingTime;

        while (Globals.PuzzleGameActive && Time.time <= Globals.PuzzleGameDeadlineTime)
        {

            yield return null;
            remainingTime = Globals.PuzzleGameDeadlineTime - (int)Time.time;

            timeRemainingTextRef.text = "Time Remaining : " + remainingTime;
            piecesRemainingTextRef.text = "Total Pieces : " + Globals.PuzzlePiecesRemainingToCollect.ToString();
        }

        //Delete Final Puzzle pieces as they are not needed
        foreach (PuzzleUnit unit in FinalPuzzlePieces)
        {
            yield return null;
            if(unit != null)
                Destroy(unit.gameObject);
        }

        //Debug.LogError("Lost");
        //Enable Won text and Disable Lost Text to Intuit The player
        PuzzleGameWonTextRef.gameObject.SetActive(false);
        PuzzleGameLostTextRef.gameObject.SetActive(true);

        PuzzleGameOver();

        StopCoroutine(PuzzleGameUpdate());
    }

    IEnumerator LoadPuzzleImage()
    {
        if (string.IsNullOrEmpty(Globals.PuzzleImageURL))
        {
            Debug.LogError("Puzzle game Image URL invalid");
            StopCoroutine(LoadPuzzleImage());
        }
        // Start a download of the given URL
        UnityWebRequest www = UnityWebRequestTexture.GetTexture(Globals.PuzzleImageURL);


        // wait until the download is done
        yield return www.SendWebRequest();


        if (www.isNetworkError || www.isHttpError)
        {
            Debug.LogError(www.error);
        }
        else
        {
            Texture myTexture = DownloadHandlerTexture.GetContent(www);

            Texture2D texture2D = new Texture2D(myTexture.width, myTexture.height);
            texture2D.LoadImage(www.downloadHandler.data);
            
            Rect rec = new Rect(0, 0, texture2D.width, texture2D.height);
            Sprite spriteToUse = Sprite.Create(texture2D, rec, new Vector2(0.5f, 0.5f), 100);
            PuzzleImageRef.sprite = spriteToUse;
        }

        www.Dispose();
        www = null;

        StopCoroutine(LoadPuzzleImage());
    }


    IEnumerator SpawnPuzzleUnits(GetPuzzleResponseRootObject obj)
    {
        foreach (PuzzleUnit unit in FinalPuzzlePieces)
        {
            yield return null;
            if (unit != null)
                DestroyImmediate(unit.gameObject);
        }

        //complementary Cleanup
        //AllPuzzlePieces.Clear();
        FinalPuzzlePieces.Clear();


        //Grab All Puzzle Units as per the GetPuzzleAPI info
        foreach (GetPuzzleResponseMessage unit in obj.Message)
        {
            yield return null;

            foreach (string number in Globals.PuzzlePiecesToSpawnNumbers)
            {
                yield return null;
                //Required Puzzle Piece Found
                if (unit.PuzzleNumber == int.Parse(number))
                {
                    //Instantiate New Puzzle piece
                    PuzzleUnit newPuzzleUnit = GameObject.Instantiate(PuzzleUnitPrefabRef, transform);

                    //Save Number
                    newPuzzleUnit.myNumber = unit.PuzzleNumber;

                    //Save position
                    newPuzzleUnit.myPosition = Globals.PositionFromString(unit.Position);

                    //Save Index
                    newPuzzleUnit.myIndex = unit.Id;
                    
                    //Add to Final Puzzle Pieces List
                    FinalPuzzlePieces.Add(newPuzzleUnit);

                    //Initialize New Puzzle Unit
                    newPuzzleUnit.Initialize();

                    break;
                }
            }


            //All required Puzzle Pieces Found
            if (FinalPuzzlePieces.Count >= Globals.PuzzlePiecesToSpawnNumbers.Length)
            {
                StopCoroutine("SpawnPuzzleUnits");
            }
        }

        
        StopCoroutine("SpawnPuzzleUnits");
    }

    
}
