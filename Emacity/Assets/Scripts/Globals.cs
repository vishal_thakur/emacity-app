﻿using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class Globals
{

    #region DataStructures

    //All UI Panels
    public enum UIPanel{

        //landing Page Panels
        signIn = 0,
        forgotPassword,
        signUp,
        signInSuccessful,
        settings,

        //Main Menu panels
        ModeSelection,
        mapSelection,
        avatarSelection,
        market,

        //GameScene Panels
        HUD,
        Chat,


        //Generic 
        loading,
        myProfile,
        webView
    }

    public enum AdvertisementType
    {
        image,
        video,
        disabled
    }

    public enum Avatar {
        afro_female = 0,
        blonde_female,
        glass_female,
        charming_male,
        kevin_male,
        tGuy_male
    }
    
    public enum CharacterSex{
        M = 0,
        F
    }
    
    public enum MiniGame {
        puzzle,
        quiz
    }


    #region Avatar Customization Options
    public enum CustomizationOptionType
    {
        hair = 0,
        top,
        bottom,
        shoe,
        avatarType
    }

    //public enum ShopLevel
    //{
    //    small = 0,
    //    medium,
    //    big
    //}
    #endregion

   

    public enum CharacterHairType{
        type1 = 0,
        type2,
        type3
    }

    public enum CharacterClothTopType
    {
        type1 = 0,
        type2,
        type3
    }

    public enum CharacterClothBottomType
    {
        type1 = 0,
        type2,
        type3
    }


    public enum CharacterShoeType{
        type1 = 0,
        type2,
        type3
    }



    public enum SceneNames
    {
        Login = 0,
        MainMenu,
        GameScene
    }

    public enum Country{
        United_Arab_Emirates = 0,
        Saudi_Arabia,
        Israel,
        Japan,
        France,
        Germany,
        United_Kingdom,
        Russia,
        India,
        United_States
    }
    #endregion


    #region GameVariables
    public static string CurrentWebViewURL;

    public static List<string> DebugMessages = new List<string>();

    //Active Customization Option During Avatar Customization Menu
    public static Globals.CustomizationOptionType ActiveCustomizationOption = Globals.CustomizationOptionType.hair;

    public static QuizGame quizGameObj;

    //After this Delay the Slideshow will load next Advertisement
    public static int AdvertisementPanelSlideshowDelay = 2;

    //Cached Data used in Game Scene for Faster Loading of textures and Data
    public static List<ShopUnitData> vendorUnitsDataList = new List<ShopUnitData>();
    public static List<AdvertisementUnitData> advertisementUnitsDataList = new List<AdvertisementUnitData>();

    //Game Sound On|OFF
    public static bool GameSoundStatus = true;

    //Used for AddUserToVendor and RemoveUserFromVendor API
    public static string ActiveVendorID = null;

    //List of Countries for the Signup Menu
    public static List<Dropdown.OptionData> countryDropdownOptions = new List<Dropdown.OptionData>();
   

    //The Selected Avatar's Features
    public static CharacterSex CurrentCharacterSex = CharacterSex.M;
    public static string CurrentCharacterHairType = "#FFFFFF";
    public static string CurrentCharacterClothTopType = "#FFFFFF";
    public static string CurrentCharacterClothBottomType = "#FFFFFF";
    public static string CurrentCharacterShoeType = "#FFFFFF";
    public static string CurrentCharacterType = "afro_female";


    //Current User's Info
    public static string myUserID;
    public static string StartGameID;
    public static string myFirstName = "New Player";
    public static string myEmail;
    public static string myCountry = "5c27a4270237324fbceb8298";
    
    //Used while Viewing Shopping Mode in the mode selection Menu in Main menu
    public static string WebViewAccessToken;

    //Shopping Mode WEBVIEW URL
    public static string ShoppingModeURL = "https://emacityapp.com/login";
    public static string UserInfoURL = "https://emacityapp.com/customer/info";

    //is True Upon Guest Sign IN
    public static bool IsGuestLogin = false;

    //PlayerController's walk Speed in the Game Scene
    public static float PlayerWalkSpeed = 0.3f;
    //PlayerController's Running Speed in the Game Scene
    public static float PlayerRunSpeed = 0.7f;

    //User's Total Coins Balance and Level
    public static int TotalCoins = 0;


    //Each time The Level is Set Spell , Shield , player speed parameters are Updated to match the respective level
    private static int level = 0;
    public static int Level {
        get { return level; }

        //Set Player Walk and Run Speed
        //Set Spell Effect Duration
        //Set Shield Effect Duration
        set
        {
            switch (value)
            {
                case 0:
                    level = 0;
                    PlayerWalkSpeed = 0.3f;
                    PlayerRunSpeed = 0.7f;
                    ShieldEffectDuration = 0;
                    SpellEffectDuration = 0;
                    break;
                case 1:
                    level = 1;
                    PlayerWalkSpeed = 0.32f;
                    PlayerRunSpeed = 0.72f;
                    ShieldEffectDuration = 3;
                    SpellEffectDuration = 3;
                    break;
                case 2:
                    level = 2;
                    PlayerWalkSpeed = 0.34f;
                    PlayerRunSpeed = 0.74f;
                    ShieldEffectDuration = 5;
                    SpellEffectDuration = 5;
                    break;
                case 3:
                    level = 3;
                    PlayerWalkSpeed = 0.36f;
                    PlayerRunSpeed = 0.76f;
                    ShieldEffectDuration = 7;
                    SpellEffectDuration = 7;
                    break;
                case 4:
                    level = 4;
                    PlayerWalkSpeed = 0.38f;
                    PlayerRunSpeed = 0.78f;
                    ShieldEffectDuration = 9;
                    SpellEffectDuration = 9;
                    break;
            }
        }
    }

    //public static bool DebugMode = false;
    public static bool CanExitGameplay = false;

    //Puzzle Game Parameters
    public static int PuzzlePiecesRemainingToCollect = 10;
    public static int PuzzleGameDeadlineTime = 20;
    public static string PuzzleImageURL = "https://sample-videos.com/img/Sample-jpg-image-50kb.jpg";
    public static bool PuzzleGameActive = false;
    public static bool QuizGameActive = false;
    public static int MaxPuzzlePiecesToSpawn = 10;
    public static float MinDistanceBetweenPuzzlePieces = 1.2f;
    public static string[] PuzzlePiecesToSpawnNumbers = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };


    //Highlight System Params
    public static Color inactiveHighlightColor = new Color(255 , 12 , 0 , 255);
    public static Color activeHighlightColor = new Color(255, 245, 0, 255);
    
    //Firebase
    public static string FirebasePushNotificationID = "NULL";


    //Chat
    public static List<string> ChatBlockedUsersList = new List<string>();
    public static List<string> ChatFriendList = new List<string>();

    //List of Users in which we are in minimised/Active Chat mode
    public static List<string> ActiveChatBubbleFriendList = new List<string>();

    public static string activeChatableUserName;
    public static string newMessageUserName;
    public static bool InChattingMode = false;



    //Durations of Spell reload , spell effect , Shield reload , shield active
    //Duration For which The Shield Shall Stay Active, It depends on the User level
    public static float ShieldReloadDuration = 0;
    public static float ShieldEffectDuration = 0;
    public static float SpellEffectDuration = 0;

    public static float SpellReloadDuration = 3;
    public static int OnShopHitBySpellCoinsPenalty = 1;
    #endregion






    #region REST API URLs
    //Game
    public static string StartGameRestAPIURL = "https://emacityapp.com/api/Gamae/StartGame?userId=";
    public static string EndGameRestAPIURL = "https://emacityapp.com/api/Gamae/EndGame?id=";

    //Main menu Stores
    public static string GetStoresURL = "https://emacityapp.com/api/Stores/GeStores?langId=5c803523493d605aab551c5a";


    //Sign in/up
    public static string LoginRESTAPIURL = "https://emacityapp.com/api/User/Login";
    public static string SignUpRESTAPIURL = "https://emacityapp.com/api/User/Register";
    public static string SocialSignInRESTAPIURL = "https://emacityapp.com/api/User/Social";
    public static string ForgotPasswordRESTAPIURL = "https://emacityapp.com/api/User/ForgetPassword";

    //User Profile
    public static string GetUserProfileRESTAPIURL = "https://emacityapp.com/api/User/UserProfile";
    public static string UpdateUserProfileRESTAPIURL = "https://emacityapp.com/api/User/UpdateUserProfile";
    public static string AddUserCoins = "https://emacityapp.com/api/Coins/AddUserCoin";

    //Vendor 
    public static string GetVendorsInfoURL = "https://emacityapp.com/api/Vendor/GetAllVendors?storeId=5c27a4270237324fbceb822a";
    public static string GetAdvertisementsInfoURL = "https://emacityapp.com/api/Home/GetPuplicAdvertisement";
    public static string AddUserToVendor = "https://emacityapp.com/api/Vendor/AddUserToVendor";
    public static string RemoveUserFromVendor = "https://emacityapp.com/api/Vendor/RemoveUserFromVendor";


    //MISC
    public static string GetAllSignupCountriesURL = "https://emacityapp.com/api/Countryapi/CountryList";
    public static string AlphaMallContactUsURL = "https://emacityapp.com/contactus";
    public static string ReduceUserCoinsURL = "https://emacityapp.com/api/Coins/RemoveUserCoin";


    //Puzzle Game
    public static string GetPuzzleGameInfoURL = "https://emacityapp.com/api/Puzzle/GetPuzzle";
    #endregion


    

    #region FirebaseAUTHParameters
    //FireBase AUTH Parameters
    //public static string SocialAccessToken;
    public static bool loggedInSocial = false;
    public static string FirebaseSocialLoginIDToken = null;
    #endregion



    #region Utility Methods
    public static void ResetToDefaults() {

        //Active Customization Option During Avatar Customization Menu
        ActiveCustomizationOption = Globals.CustomizationOptionType.hair;

        quizGameObj = null;

        //After this Delay the Slideshow will load next Advertisement
        AdvertisementPanelSlideshowDelay = 2;

        //Cached Data used in Game Scene for Faster Loading of textures and Data
        //public static List<ShopUnitData> vendorUnitsDataList = new List<ShopUnitData>();
        //public static List<AdvertisementUnitData> advertisementUnitsDataList = new List<AdvertisementUnitData>();

        //Game Sound On|OFF
        //public static bool GameSoundStatus = true;

        //Used for AddUserToVendor and RemoveUserFromVendor API
        ActiveVendorID = null;

        //List of Countries for the Signup Menu
        //public static List<Dropdown.OptionData> countryDropdownOptions = new List<Dropdown.OptionData>();


        //The Selected Avatar's Features
        CurrentCharacterSex = CharacterSex.M;
        CurrentCharacterHairType = "#FFFFFF";
        CurrentCharacterClothTopType = "#FFFFFF";
        CurrentCharacterClothBottomType = "#FFFFFF";
        CurrentCharacterShoeType = "#FFFFFF";
        CurrentCharacterType = "afro_female";


        //Current User's Info
        myUserID = null;
        StartGameID = null;
        myFirstName = null;
        myEmail = null;
        myCountry = "5c27a4270237324fbceb8242";

        //Used while Viewing Shopping Mode in the mode selection Menu in Main menu
        WebViewAccessToken = null;
        

        //is True Upon Guest Sign IN
        IsGuestLogin = false;

        //PlayerController's walk Speed in the Game Scene
        PlayerWalkSpeed = 0.3f;
        //PlayerController's Running Speed in the Game Scene
        PlayerRunSpeed = 0.7f;

        //User's Total Coins Balance and Level
        TotalCoins = 0;
        Level = 0;

        //public static bool DebugMode = false;
        CanExitGameplay = false;

        //Puzzle Game Parameters
        PuzzlePiecesRemainingToCollect = 0;
        PuzzleGameDeadlineTime = 0;
        //Sample Image if no Image Is specified
        PuzzleImageURL = "https://sample-videos.com/img/Sample-jpg-image-50kb.jpg";
        PuzzleGameActive = false;
        QuizGameActive = false;
        MaxPuzzlePiecesToSpawn = 0;
        //MinDistanceBetweenPuzzlePieces = 1.2f;
        PuzzlePiecesToSpawnNumbers = null;


        //Highlight System Params
        //public static Color inactiveHighlightColor = new Color(255, 12, 0, 255);
        //public static Color activeHighlightColor = new Color(255, 245, 0, 255);

        //Firebase
        FirebasePushNotificationID = "none";


        //Chat
        ChatBlockedUsersList.Clear();
        ChatFriendList.Clear();

        activeChatableUserName = null;
        InChattingMode = false;



        //Durations of Spell reload , spell effect , Shield reload , shield active
        //Duration For which The Shield Shall Stay Active, It depends on the User level
        ShieldReloadDuration = 7;
        ShieldEffectDuration = 5;
        SpellEffectDuration = 5;
        SpellReloadDuration = 3;

}


    public static bool VerifyEmailAddress(string address)
    {
        string[] atCharacter;
        string[] dotCharacter;
        // this splits the input address by the @ symbol. If it finds the @ symbol, it throws it away,
        // and puts what comes before the @ symbol into atCharacter[0], and what comes after the @ symbol
        // into atCharacter[1]
        atCharacter = address.Split("@"[0]);
        //now we check that the returned array is exactly 2 members long, that means there was only 1 @ symbol
        if (atCharacter.Length == 2)
        {
            //here we split the second member returned above by the dot character, and shove the returned info
            // into the dotCharacter array.
            dotCharacter = atCharacter[1].Split("."[0]);
            // now we check the length of the dotCharacter array. If it is greater than or equal to 2, we know
            // we have at least one dot, maybe more than one.
            if (dotCharacter.Length >= 2)
            {
                // this last check makes sure that there is actual data after the last dot.
                if (dotCharacter[dotCharacter.Length - 1].Length == 0)
                {
                    // fail
                    return false;
                }
                else
                {
                    // if we get to here, you have a valid email address format
                    return true;
                }
            }
            else
            {
                // fail
                return false;
            }
        }
        else
        {
            // fail
            return false;
        }
    }

    //public static int GetRandomNumber(int min ,){
    //    return UnityEngine.Random.Range(0 ,100);
    //}

    public static Vector3 PositionFromString(string positionString)
    {
        try
        {
            string[] result = positionString.Split(',');

            return new Vector3(float.Parse(result[0]), float.Parse(result[1]), float.Parse(result[2]));
        }
        catch(System.Exception e)
        {
            return Vector3.zero;
            Debug.LogError("Something Wrong in positionFromString");
        }
    }

    public static bool AreTwoObjectsFarEnough(Vector3 obj1Position, Vector3 obj2Position , float minDistanceToCheck)
    {
        float distanceBetweenTwoObjects = Vector3.Distance(obj1Position, obj2Position);

        //Yes the two Objects are Far Enough from the Specified MinDistance to check
        if (distanceBetweenTwoObjects > minDistanceToCheck)
            return true;
        else//These are too close[As per min Distance to check Obviously]
            return false;
    }


    public static int GetRandomNumber(int min, int max)
    {
        return Random.Range(min, max);
    }


    


    #endregion

}
