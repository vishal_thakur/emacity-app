﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class RESTAPIExceptionObject
{
    public bool Success;
    public string Message;
}
