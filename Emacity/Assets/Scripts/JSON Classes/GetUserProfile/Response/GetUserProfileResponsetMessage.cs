﻿
[System.Serializable]
public class GetUserProfileRequestMessage
{
    public string FirstName;
    public string LastName;
    public string Email;
    public string Country;
    public string Mobile;
    public string Interests;
    public string CharacterType;
    public string TypeOfHairColor;
    public string TypeOfClothTopColor;
    public string TypeOfClothBottomColor;
    public string TypeOfShoeColor;
    public int TotalCoins;
    public int Level;
}