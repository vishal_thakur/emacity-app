﻿using System.Collections.Generic;

[System.Serializable]
public class GetVendorsInfoResponseRootObject
{
    public bool Success;
    public List<GetVendorsInfoResponseMessage> Message;
}