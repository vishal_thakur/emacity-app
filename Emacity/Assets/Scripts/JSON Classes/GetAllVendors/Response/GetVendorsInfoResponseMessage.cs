﻿
using System.Collections.Generic;

[System.Serializable]
public class GetVendorsInfoResponseMessage
{
    public string VendorId;
    public string VendorName;
    public string VendorDescription;
    public string VendorURL;
    public string VendorStatus;
    public string VendorFrontPicture;
    public string VendorRightPicture;
    public string VendorLeftPicture;
    public int VendorIndex;
    public int IntVer;
    public string ShopLevel;
    public string ShopPosition;
    public string ShopRotation;
    //public List<AdvertisementObject> Advertisement;
}