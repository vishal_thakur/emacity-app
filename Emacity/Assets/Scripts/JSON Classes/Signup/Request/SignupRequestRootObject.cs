﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class SignupRequestRootObject
{
    public string Email;
    public string Password;
    public string CountryId;
    public string Gender;
}
