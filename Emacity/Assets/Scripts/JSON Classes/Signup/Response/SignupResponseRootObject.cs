﻿[System.Serializable]
public class SignupResponseRootObject
{
    public bool Success;
    public SignupResponseMessage Message;
}
