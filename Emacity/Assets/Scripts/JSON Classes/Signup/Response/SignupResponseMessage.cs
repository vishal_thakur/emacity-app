﻿
[System.Serializable]
public class SignupResponseMessage
{
    public string Id;
    public string Email;
    public string CountryId;
    public string Gender;
}
