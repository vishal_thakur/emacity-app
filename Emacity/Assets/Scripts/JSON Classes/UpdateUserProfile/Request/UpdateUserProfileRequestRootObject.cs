﻿
[System.Serializable]
public class UpdateUserProfileRequestRootObject
{
    public string UserId;
    public string Gender;
    public string FirstName;
    public string LastName;
    public string Email;
    public string CountryId;
    public string Phone;
    public string Interests;
    public string CharacterType;
    public string TypeOfHairColor;
    public string TypeOfClothTopColor;
    public string TypeOfClothBottomColor;
    public string TypeOfShoeColor;
}