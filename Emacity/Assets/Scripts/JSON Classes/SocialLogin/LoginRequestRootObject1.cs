﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class LoginRequestRootObject
{
    public string Email;
    public string Password;
}
