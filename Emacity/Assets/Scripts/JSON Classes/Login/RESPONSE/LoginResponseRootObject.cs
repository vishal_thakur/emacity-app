﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class LoginResponseRootObject
{
    public bool Success;
    public LoginResponseMessage Message;
}
