﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class LoginResponseMessage
{
    public string Id;
    public string Email;
    public string CountryId;
    public string Gender;
    public string Token;
}
