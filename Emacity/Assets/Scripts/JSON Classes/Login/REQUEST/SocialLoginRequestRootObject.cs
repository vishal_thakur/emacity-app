﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SocialLoginRequestRootObject
{
    public string Token;
}
