﻿
using System.Collections.Generic;

[System.Serializable]
public class GetSignupCountriesResponseMessage
{
    public string Id;
    public string Name;
    public string PhoneCode;
}