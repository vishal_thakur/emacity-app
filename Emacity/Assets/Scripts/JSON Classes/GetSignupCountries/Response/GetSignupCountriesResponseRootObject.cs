﻿using System.Collections.Generic;

[System.Serializable]
public class GetSignupCountriesResponseRootObject
{
    public bool Success;
    public List<GetSignupCountriesResponseMessage> Message;
}