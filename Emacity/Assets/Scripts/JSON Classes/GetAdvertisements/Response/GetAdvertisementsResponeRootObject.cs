﻿using System.Collections.Generic;

[System.Serializable]

public class GetAdvertisementsResponeRootObject
{
    public bool Success;
    public List<GetAdvertisementsResponeMessage> Message;
}
