﻿using System.Collections.Generic;

[System.Serializable]
public class GetPuzzleResponseRootObject
{
    public bool Success;
    public List<GetPuzzleResponseMessage> Message;
}