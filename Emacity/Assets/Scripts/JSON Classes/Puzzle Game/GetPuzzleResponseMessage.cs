﻿using System.Collections.Generic;

[System.Serializable]
public class GetPuzzleResponseMessage
{
    public string Id;
    public int PuzzleNumber;
    public string Position;
}