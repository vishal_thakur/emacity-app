﻿
[System.Serializable]
public class GetStoresResponseMessage
{
    public string Id;
    public string Name;
    public string StoreImage;
}