﻿using System.Collections.Generic;

[System.Serializable]
public class GetStoresResponseRootObject
{
    public bool Success;
    public List<GetStoresResponseMessage> Message;
}