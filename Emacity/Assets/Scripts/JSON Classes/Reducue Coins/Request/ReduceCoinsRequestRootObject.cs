﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ReduceCoinsRequestRootObject
{
    public string UserId;
    public int Coins;
}
