﻿using UnityEngine;
//using System.Collections;
using System.Text;
//using System.Collections.Generic;
//using ParserSimpleJSON;
using BestHTTP;


public static class RestAPICallManager
{
    
    //onComplete = call a desired Method on successful call of the HTTP request
    public static void CallRESTAPI(HTTPMethods methodType, string urlToCall, string rawData)
    {
        //Donot Call any Backend API is it is a Guest Login
        if (Globals.IsGuestLogin)
            return;

        //HTTPRequest theRequest = new HTTPRequest(new System.Uri(urlToCall) , methodType , OnRequestFinished);

        HTTPRequest theRequest = new HTTPRequest(new System.Uri(urlToCall), methodType, OnRequestFinished);
        //Set Header
        theRequest.SetHeader("Content-Type", "application/json");

        //New UTF8 Encoding Obj
        UTF8Encoding UTF8EncodingObj = new UTF8Encoding();

        //Set Raw Data
        theRequest.RawData = UTF8EncodingObj.GetBytes(rawData);

        //Call the URL
        theRequest.Send();

        Debug.Log("POST Sent/t" + urlToCall);
    }




    //Type 2
    //void OnCallRESTAPIType2(HTTPMethods methodType, string urlToCall, string fieldKey, string value)
    //{
    //    CallRESTAPI(methodType, urlToCall, fieldKey , value);
    //}
    //calltype =  "get" | "post"
    //urlToCall = RestAPI url that will be called by the HTTP request
    //hashTableDataToSend = Hastable data to send with the Called URL
    //onComplete = call a desired Method on successful call of the HTTP request
    public static void CallRESTAPI(HTTPMethods methodType, string urlToCall, string fieldKey, string value)
    {

        //Donot Call any Backend API is it is a Guest Login
        if (Globals.IsGuestLogin)
            return;
        //HTTPRequest theRequest = new HTTPRequest(new System.Uri(urlToCall) , methodType , OnRequestFinished);

        HTTPRequest theRequest = new HTTPRequest(new System.Uri(urlToCall), methodType, OnRequestFinished);

        //Set Header
        //heRequest.SetHeader("Content-Type", "application/json");

        theRequest.AddField(fieldKey, value);


        //New UTF8 Encoding Obj
        UTF8Encoding UTF8EncodingObj = new UTF8Encoding();

        //Set Raw Data
        //theRequest.RawData = UTF8EncodingObj.GetBytes(rawData);

        //Call the URL
        theRequest.Send();

        Debug.Log("POST Sent/t" + urlToCall);
    }





    //Type 3
    //calltype =  "get" | "post"
    //urlToCall = RestAPI url that will be called by the HTTP request
    //hashTableDataToSend = Hastable data to send with the Called URL
    //onComplete = call a desired Method on successful call of the HTTP request
    public static void CallRESTAPI(HTTPMethods methodType, string urlToCall)
    {
        HTTPRequest theRequest = new HTTPRequest(new System.Uri(urlToCall), methodType, OnRequestFinished);
        
        //New UTF8 Encoding Obj
        UTF8Encoding UTF8EncodingObj = new UTF8Encoding();
        

        //Call the URL
        theRequest.Send();

        Debug.Log("POST Sent /t" + urlToCall);
    }




    //Called when RESTAPI response is received
    static void OnRequestFinished(HTTPRequest request, HTTPResponse response)
    {
        try
        {
            if (response == null)
            {
                //TestString = request.Exception.ToString();
                Messenger.Broadcast(Events.OnRESTAPICallback, request.Exception.ToString());
                Debug.LogError("Error---! ! " + request.Exception);
            }
            else
            {
                //TestString = response.DataAsText;
                Messenger.Broadcast(Events.OnRESTAPICallback, response.DataAsText);
                Debug.Log("Success---Request Finished! Text received: " + response.DataAsText);
                Debug.Log("Success---Request Finished! Text received: " + response.Message);
            }
        }
        catch (System.Exception e)
        {
            Messenger.Broadcast(Events.OnRESTAPICallback, e.Message);
            Debug.LogError("Exception ---Respose here!" + "Message : " + response.Message + "/nExceotion:" + request.Exception.ToString());
            //Messenger.Broadcast(Events.OnRESTAPICallback, e.Message);
        }
    }
}