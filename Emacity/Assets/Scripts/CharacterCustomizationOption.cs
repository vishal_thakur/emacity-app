﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterCustomizationOption : MonoBehaviour
{
    public string myColorValue;


    public void OnSelected() {
        // Tell avatar selection Manager that my color Value has been selected 
       Messenger.Broadcast<string>(Events.OnAvatarCustomizationOptionSelected ,myColorValue);
    }
    
}
