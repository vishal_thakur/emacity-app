﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterCustomizationOptionType : MonoBehaviour
{
    public Globals.CustomizationOptionType customizationOption;

    public void OnSelected()
    {
        // Tell avatar selection Manager that my Type of Customization option is active now (hari , Top , Bottom ,Shoe)
        Messenger.Broadcast<Globals.CustomizationOptionType>(Events.OnAvatarCustomizationOptionTypeSelected , customizationOption);
    }
}
