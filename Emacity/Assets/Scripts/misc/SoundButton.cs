﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundButton : MonoBehaviour
{
    [SerializeField] GameObject soundOnImageRef;
    [SerializeField] GameObject soundOffImageRef;


    public void OnToggle(bool flag)
    {

        soundOnImageRef.SetActive(flag);
        soundOffImageRef.SetActive(!flag);

        //Messenger.Broadcast<bool>(Events.OnSoundButtonToggled , flag);
    }
}
