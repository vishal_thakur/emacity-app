﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VFXRotation : MonoBehaviour
{
    public Camera target;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        transform.LookAt(transform.position + target.transform.rotation * Vector3.forward,
             target.transform.rotation * Vector3.up);
    }
}
