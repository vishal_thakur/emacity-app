﻿using System;
using UnityEngine;
using UnityEngine.Purchasing;


public class IAPManager : MonoBehaviour, IStoreListener
{
    public static IAPManager instance;

    private static IStoreController m_StoreController;
    private static IExtensionProvider m_StoreExtensionProvider;

    //Step 1 create your products
    string skill_Ladder = "skill_ladder";//Cost = $5

    //Coins
    string coins_100 = "coins_100";//Cost = $1
    string coins_600 = "coins_600";//Cost = $5
    string coins_950 = "coins_950";//Cost = $8
    string coins_1500 = "coins_1500";//Cost = $10

    //Spell
    string spell_5 = "spell_5";//Cost = 50 Coins
    string spell_10 = "spell_10";//Cost = 70 Coins
    string spell_20 = "spell_20";//Cost = 100 Coins
    string spell_Mega = "spell_Mega";//Cost = $5

    //shield
    string shield_5 = "shield_5";//Cost = 50 Coins
    string shield_10 = "shield_10";//Cost = 70 Coins
    string shield_20 = "shield_20";//Cost = 100 Coins
    string shield_Mega = "shield_Mega";//Cost = $5



    //************************** Adjust these methods **************************************
    public void InitializePurchasing()
    {
        if (IsInitialized()) { return; }
        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

        //Step 2 choose if your product is a consumable or non consumable

        //Add One Time Buy | Non Consumable Products
        builder.AddProduct(skill_Ladder, ProductType.NonConsumable);


        //Add Consumable Prodcuts
        //Coins
        builder.AddProduct(coins_100, ProductType.Consumable);
        builder.AddProduct(coins_600, ProductType.Consumable);
        builder.AddProduct(coins_950, ProductType.Consumable);
        builder.AddProduct(coins_1500, ProductType.Consumable);
        //Spells
        builder.AddProduct(spell_5, ProductType.Consumable);
        builder.AddProduct(spell_10, ProductType.Consumable);
        builder.AddProduct(spell_20, ProductType.Consumable);
        builder.AddProduct(spell_Mega, ProductType.Subscription);
        //Shield
        builder.AddProduct(shield_5, ProductType.Consumable);
        builder.AddProduct(shield_10, ProductType.Consumable);
        builder.AddProduct(shield_20, ProductType.Consumable);
        builder.AddProduct(shield_Mega, ProductType.Subscription);



        UnityPurchasing.Initialize(this, builder);
    }


    private bool IsInitialized()
    {
        return m_StoreController != null && m_StoreExtensionProvider != null;
    }


    //Step 3 Create methods
    public void PurchaseSkillLadder()
    {
        BuyProductID(skill_Ladder);
    }

    public void PurchaseCoins(string quantity)
    {
        switch (quantity)
        {
            case "100":
                BuyProductID(coins_100);
                break;
            case "600":
                BuyProductID(coins_600);
                break;
            case "950":
                BuyProductID(coins_950);
                break;
            case "1500":
                BuyProductID(coins_1500);
                break;
        }
    }

    public void PurchaseSpell(string duration)
    {
        switch (duration)
        {
            case "5":
                BuyProductID(spell_5);
                break;
            case "10":
                BuyProductID(spell_10);
                break;
            case "20":
                BuyProductID(spell_20);
                break;
            case "mega":
                BuyProductID(spell_Mega);
                break;
        }
    }

    public void PurchaseShield(string duration)
    {
        switch (duration)
        {
            case "5":
                BuyProductID(shield_5);
                break;
            case "10":
                BuyProductID(shield_10);
                break;
            case "20":
                BuyProductID(shield_20);
                break;
            case "mega":
                BuyProductID(shield_Mega);
                break;
        }
    }


    //Step 4 modify purchasing
    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
    {
        //Ladder
        if (String.Equals(args.purchasedProduct.definition.id, skill_Ladder, StringComparison.Ordinal))
        {
            //Messenger.Broadcast Ladder Purchased Event
            Messenger.Broadcast(Events.OnIAPSkillLadderPurchaseSuccess);
            Debug.LogError("Skill Ladder Purchased");
        }

        //Coins
        else if (String.Equals(args.purchasedProduct.definition.id, coins_100, StringComparison.Ordinal))
        {
            //Messenger.Broadcast coins_100 Purchased Event
            Messenger.Broadcast<string>(Events.OnIAPCoinsPurchaseSuccess , "100");
            Debug.LogError("coins_100 Purchased");
        }
        else if (String.Equals(args.purchasedProduct.definition.id, coins_600, StringComparison.Ordinal))
        {
            //Messenger.Broadcast coins_600 Purchased Event
            Messenger.Broadcast<string>(Events.OnIAPCoinsPurchaseSuccess, "600");
            Debug.LogError("coins_600 Purchased");
        }
        else if (String.Equals(args.purchasedProduct.definition.id, coins_950, StringComparison.Ordinal))
        {
            //Messenger.Broadcast coins_950 Purchased Event
            Messenger.Broadcast<string>(Events.OnIAPCoinsPurchaseSuccess, "950");
            Debug.LogError("coins_950 Purchased");
        }
        else if (String.Equals(args.purchasedProduct.definition.id, coins_1500, StringComparison.Ordinal))
        {
            //Messenger.Broadcast coins_1500 Purchased Event
            Messenger.Broadcast<string>(Events.OnIAPCoinsPurchaseSuccess, "1500");
            Debug.LogError("coins_1500 Purchased");
        }



        //Spells
        else if (String.Equals(args.purchasedProduct.definition.id, spell_5, StringComparison.Ordinal))
        {
            //Messenger.Broadcast spell_5 Purchased Event
            Messenger.Broadcast<string>(Events.OnIAPSpellPurchaseSuccess, "5");
            Debug.LogError("spell_5 Purchased");
        }
        else if (String.Equals(args.purchasedProduct.definition.id, spell_10, StringComparison.Ordinal))
        {
            //Messenger.Broadcast spell_10 Purchased Event
            Messenger.Broadcast<string>(Events.OnIAPSpellPurchaseSuccess, "10");
            Debug.LogError("spell_10 Purchased");
        }
        else if (String.Equals(args.purchasedProduct.definition.id, spell_20, StringComparison.Ordinal))
        {
            //Messenger.Broadcast spell_20 Purchased Event
            Messenger.Broadcast<string>(Events.OnIAPSpellPurchaseSuccess, "20");
            Debug.LogError("spell_20 Purchased");
        }
        else if (String.Equals(args.purchasedProduct.definition.id, spell_Mega, StringComparison.Ordinal))
        {
            //Messenger.Broadcast spell_Mega Purchased Event
            Messenger.Broadcast<string>(Events.OnIAPSpellPurchaseSuccess, "mega");
            Debug.LogError("spell_Mega Purchased");
        }


        //Shield
        else if (String.Equals(args.purchasedProduct.definition.id, shield_5, StringComparison.Ordinal))
        {
            //Messenger.Broadcast shield_5 Purchased Event
            Messenger.Broadcast<string>(Events.OnIAPShieldPurchaseSuccess, "5");
            Debug.LogError("shield_5 Purchased");
        }
        else if (String.Equals(args.purchasedProduct.definition.id, shield_10, StringComparison.Ordinal))
        {
            //Messenger.Broadcast shield_10 Purchased Event
            Messenger.Broadcast<string>(Events.OnIAPShieldPurchaseSuccess, "10");
            Debug.LogError("shield_10 Purchased");
        }
        else if (String.Equals(args.purchasedProduct.definition.id, shield_20, StringComparison.Ordinal))
        {
            //Messenger.Broadcast shield_20 Purchased Event
            Messenger.Broadcast<string>(Events.OnIAPShieldPurchaseSuccess, "20");
            Debug.LogError("shield_20 Purchased");
        }
        else if (String.Equals(args.purchasedProduct.definition.id, shield_Mega, StringComparison.Ordinal))
        {
            //Messenger.Broadcast shield_Mega Purchased Event
            Messenger.Broadcast<string>(Events.OnIAPShieldPurchaseSuccess, "mega");
            Debug.LogError("shield_Mega Purchased");
        }




        else
        {
            Messenger.Broadcast<string, PopupManager.Type>(Events.OnPopupRequired, args.purchasedProduct.definition.id + " Purchase failed, Try Again !" , PopupManager.Type.notification);
            Debug.LogError("Purchase Failed for" + args.purchasedProduct.definition.id);
        }


        return PurchaseProcessingResult.Complete;
    }










    //**************************** Dont worry about these methods ***********************************
    private void Awake()
    {
        TestSingleton();
    }

    void Start()
    {
        if (m_StoreController == null) { InitializePurchasing(); }
    }

    private void TestSingleton()
    {
        if (instance != null) { Destroy(gameObject); return; }
        instance = this;
        DontDestroyOnLoad(gameObject);
    }

    void BuyProductID(string productId)
    {
        if (IsInitialized())
        {
            Product product = m_StoreController.products.WithID(productId);
            if (product != null && product.availableToPurchase)
            {
                Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
                m_StoreController.InitiatePurchase(product);
            }
            else
            {
                Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
            }
        }
        else
        {
            Messenger.Broadcast<string, PopupManager.Type>(Events.OnPopupRequired, "Purchase failed, Publish to store first!", PopupManager.Type.notification);
            Debug.Log("BuyProductID FAIL. Not initialized.");
        }
    }

    public void RestorePurchases()
    {
        if (!IsInitialized())
        {
            Debug.Log("RestorePurchases FAIL. Not initialized.");
            return;
        }

        if (Application.platform == RuntimePlatform.IPhonePlayer ||
            Application.platform == RuntimePlatform.OSXPlayer)
        {
            Debug.Log("RestorePurchases started ...");

            var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
            apple.RestoreTransactions((result) => {
                Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
            });
        }
        else
        {
            Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
        }
    }

    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        Debug.Log("OnInitialized: PASS");
        m_StoreController = controller;
        m_StoreExtensionProvider = extensions;
    }


    public void OnInitializeFailed(InitializationFailureReason error)
    {
        Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
    }

    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
    }
}