﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class EmacitySceneManager
{

    public static void LoadScene(Globals.SceneNames sceneName)
    {
        Application.LoadLevel(sceneName.ToString());
    }

    public static void LoadScene(string sceneName)
    {
        Application.LoadLevel(sceneName.ToString());
    }
}
