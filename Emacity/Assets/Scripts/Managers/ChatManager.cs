﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChatManager : MonoBehaviour
{
    [Header ("Active Chat Bubble Window params")]
    [SerializeField]
    List<ActiveChatUnit> AllActiveChatUnits = new List<ActiveChatUnit>();

    [SerializeField]
    ActiveChatUnit ActiveChatUnitRef;

    [SerializeField]
    Transform ActiveChatBubbleWindowParentRef;



    [Space(20)]
    [SerializeField]
    GameObject userListPanelRef;

    [SerializeField]
    GameObject newMessageIconRef;

    [Header ("Popup Params")]
    [Space(20), SerializeField] GameObject incomingChatRequestPopupRef;
    [SerializeField] GameObject ChatRequestSentPopupRef;
    [SerializeField] GameObject incoChatRequestDeclinedPopupRef;
    [SerializeField] Text incomingChatRequestTextRef;

    string lastSenderName = null;


    [Header ("Chat Box Params")]
    [Space(20) , SerializeField]
    GameObject ChatBoxRef;

    [SerializeField]
    Text ChatBoxHeaderTextRef;

    [SerializeField]
    Transform chatListParentRef;

    [SerializeField]
    ChatUserUnit chatUserUnitPrefabRef;

    [SerializeField]
    List<ChatUserUnit> userPanelList = new List<ChatUserUnit>();


    // Start is called before the first frame update
    void Start()
    {

        Messenger.AddListener<string>(Events.OnUserSelectedToChat, OnUserSelectedToChat);
        Messenger.AddListener<string, object>(Events.OnIncomingChatRequest, OnIncomingChatRequest);
    }

    private void OnDestroy()
    {

        Messenger.RemoveListener<string>(Events.OnUserSelectedToChat, OnUserSelectedToChat);
        Messenger.RemoveListener<string, object>(Events.OnIncomingChatRequest, OnIncomingChatRequest);
    }

    //Show the Chatable User list
    public void OnChatButtonClicked()
    {
        if (userListPanelRef.activeSelf == true)
        {
            userPanelList.Clear();
        }


        userListPanelRef.SetActive(!userListPanelRef.activeSelf);
        

        //Show Chatable Users list
        //// userListPanelRef.SetActive(true);
        //If Chat Panel is intented To be hidden Then do nothing
        if (!userListPanelRef.activeSelf)
            return;
        else {
            //Clear Old list so that we have the updated List every Time
            for (int i = 0; i < chatListParentRef.childCount; i++)
                GameObject.DestroyImmediate(chatListParentRef.GetChild(i).gameObject);

            //Get All Player's list in the Current Room
            foreach (Player player in PhotonNetwork.PlayerList)
            {
                //Load Chat User List
                if (!player.IsLocal)
                {
                    ChatUserUnit newChatUserUnit = Instantiate(chatUserUnitPrefabRef, chatListParentRef, false);
                    newChatUserUnit.Initialize(player.NickName);
                    userPanelList.Add(newChatUserUnit);
                }
            }

            foreach (ChatUserUnit user in userPanelList)
            {
                if (user.OnIncomingChatRequest(Globals.newMessageUserName))
                {
                    return;
                }
            }
        }
    }



    void OnIncomingChatRequest(string newSender, object message)
    {
        //Ignore self call
        if (newSender == Globals.myFirstName)
            return;

        Globals.newMessageUserName = newSender;

        //Check if the user a Blocked User, if yes then just ignore him 
        foreach (string blockedSenderName in Globals.ChatBlockedUsersList)
        {
            if (blockedSenderName == newSender)
            {
                Debug.LogError(newSender + "\t has been blocked. Ignore him!!");
                return;
            }

        }

        //If already in Friend List
        if (Globals.ChatFriendList.Contains(newSender))
        {
            //If Active Chat Bubble Does not have the User who just sent new message , then only show the newMessageIconRef
            if (!Globals.ActiveChatBubbleFriendList.Contains(newSender))
            {
                //if chat box is open already then dont show Notif icon
                if (!Globals.InChattingMode)
                {
                    //Show new Message icon
                    newMessageIconRef.SetActive(true);
                }
            }
        }
        else
        {

            lastSenderName = newSender;

            //else Show incoming Chat Request Popup with The senders name
            incomingChatRequestPopupRef.SetActive(true);

            incomingChatRequestTextRef.text = "CHAT REQUEST FROM " + newSender;
        }
    }


    //private void OnGUI()
    //{
    //    //foreach (string user in Globals.ChatFriendList)
    //    //{
    //    //    GUILayout.TextField("user in Globals.ChatFriendList = " + user);
    //    //}

    //    GUILayout.TextField("=_==_==_==_==_==_==_==_==_==_==_==_==_=");


    //    //foreach (string user in Globals.ActiveChatBubbleFriendList)
    //    //{
    //    //    GUILayout.TextField("user in Globals.ActiveChatBubbleFriendList = " + user);
    //    //}

    //    foreach (ChatUserUnit user in userPanelList)
    //    {
    //        GUILayout.TextField("user in Globals.ActiveChatBubbleFriendList = " + user.nameTextRef.text);
    //    }

        
    //}
    void OnUserSelectedToChat(string userName)
    {
         userPanelList.Clear();

        //Hide New Meesage Icon as the message will be Read now
        if(newMessageIconRef.activeSelf)
        newMessageIconRef.SetActive(false);

        lastSenderName = userName;

        userListPanelRef.SetActive(false);


        //Set Header Text so as to intuit with whom the chat is going on
        ChatBoxHeaderTextRef.text = userName;

        //Check if the User is already in our Friend List
        if (Globals.ChatFriendList.Contains(userName))
        {
            //Show Chat Box with open chat with Selected User
            ChatBoxRef.SetActive(true);
            Globals.InChattingMode = true;
        }
        else
        {
            StartCoroutine(ShowForSeconds(3, ChatRequestSentPopupRef));
        }

        //Add the user to friend list
        //Globals.ChatFriendList.Add(userName);
    }

    public void OnChatRequestAccepted()
    {
        Globals.activeChatableUserName = lastSenderName;

        //Add user To Friend list
        Globals.ChatFriendList.Add(lastSenderName);

        //Set Header Text so as to intuit with whom the chat is going on
        ChatBoxHeaderTextRef.text = lastSenderName;

        //Show Chat Box and start chat with the new user
        ChatBoxRef.SetActive(true);

        Globals.InChattingMode = true;
    }


    public void OnChatRequestDeclined()
    {
        //Show Chat declined and user blocked Popup
        StartCoroutine(ShowForSeconds(3 , incoChatRequestDeclinedPopupRef));
    }




    //Utility
    IEnumerator ShowForSeconds(int seconds , GameObject obj)
    {
        obj.SetActive(true);

        yield return new WaitForSeconds(seconds);


        obj.SetActive(false);

        StopAllCoroutines();

    }


    public void OnChatBoxMinimize()
    {

        Globals.InChattingMode = false;

        //Avoid Override same name Chat Bubble
        foreach (ActiveChatUnit chatBubble in AllActiveChatUnits)
        {
            if (chatBubble.userName == Globals.activeChatableUserName)
                return;
        }

        //Add a new chat bubble in Active Chat Bubbles Window
        ActiveChatUnit newChatBubble = Instantiate(ActiveChatUnitRef , ActiveChatBubbleWindowParentRef , false);
        
        AllActiveChatUnits.Add(newChatBubble);

        newChatBubble.Initialize(Globals.activeChatableUserName);

        //Save the Active Chat bubble User
        Globals.ActiveChatBubbleFriendList.Add(Globals.activeChatableUserName);
    }

    public void OnChatClosed()
    {
        Globals.ActiveChatBubbleFriendList.Remove(Globals.activeChatableUserName);

        Globals.InChattingMode = false;
        //Avoid Override same name Chat Bubble
        foreach (ActiveChatUnit chatBubble in AllActiveChatUnits)
        {
            if (chatBubble.userName == Globals.activeChatableUserName)
            {
                AllActiveChatUnits.Remove(chatBubble);
                DestroyImmediate(chatBubble.gameObject);
                return;
            }
        }
    }

    
}


