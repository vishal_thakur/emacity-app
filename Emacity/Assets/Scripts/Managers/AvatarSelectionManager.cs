﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class AvatarSelectionManager : MonoBehaviour
{
    //[SerializeField]
    //RestAPICallManager RestAPICallManagerRef;

    [SerializeField] GameObject avatar3DSetupRef;
    //Multiplayer player's Custom properties 
    ExitGames.Client.Photon.Hashtable myCustomProperties = new ExitGames.Client.Photon.Hashtable();

    [SerializeField]
    AudioSource MainMenuThemeAudioSourceRef;


    [SerializeField]
    List<AvatarUnit> Avatars;

    [SerializeField]
    GameObject SaveChangesButtonRef;
    
    //When We exit the Avatar Customization Menu , then This bool helps us know wether we made any update to the Avatar's Customization Options
    bool wasAvatarCustomized = false;

    int CurrentAvatarIndex = 0;



    IEnumerator Start()
    {
        yield return null;
        Messenger.AddListener<string>(Events.OnAvatarCustomizationOptionSelected, OnAvatarCustomizationOptionSelected);
        Messenger.AddListener<Globals.CustomizationOptionType>(Events.OnAvatarCustomizationOptionTypeSelected , OnAvatarCustomizationOptionTypeSelected);
        Messenger.AddListener(Events.OnAvatarCustomizationStarted, OnAvatarCustomizationStarted);
        Messenger.AddListener(Events.OnAvatarCustomizationFinished, OnAvatarCustomizationFinished);
        Messenger.AddListener(Events.OnPopupRetrySelected, OnPopupRetrySelected);
        //Screen.orientation = ScreenOrientation.Portrait;
        //RestAPICallManagerObj = RestAPICallManager.pInstance;
        Messenger.AddListener<UIPanel, UIPanel>(Events.OnUIPanelShown , OnUIPanelShown);

        StartCoroutine(AudioController.FadeIn(MainMenuThemeAudioSourceRef, 6));

        yield return new WaitForEndOfFrame();
        //Get User Profile
        GetUserProfile();
    }



    void OnDestroy(){
        Messenger.RemoveListener<string>(Events.OnAvatarCustomizationOptionSelected, OnAvatarCustomizationOptionSelected);
        Messenger.RemoveListener<Globals.CustomizationOptionType>(Events.OnAvatarCustomizationOptionTypeSelected, OnAvatarCustomizationOptionTypeSelected);
        Messenger.RemoveListener(Events.OnAvatarCustomizationFinished, OnAvatarCustomizationFinished);
        Messenger.RemoveListener(Events.OnAvatarCustomizationStarted, OnAvatarCustomizationStarted);
        Messenger.RemoveListener(Events.OnPopupRetrySelected, OnPopupRetrySelected);
        Messenger.RemoveListener<UIPanel , UIPanel>(Events.OnUIPanelShown, OnUIPanelShown);
    }
    
    void OnUIPanelShown(UIPanel activeUIpanel , UIPanel previousUIPanel)
    {
        switch (activeUIpanel.UIPanelType)
        {
            case Globals.UIPanel.avatarSelection:
                //Show 3D Avatar Setup
                avatar3DSetupRef.SetActive(true);
                break;
            default:
                avatar3DSetupRef.SetActive(false);
                break;

        }
        
    }

    void OnPopupRetrySelected()
    {
        GetUserProfile();
    }

    ////Update Current Character Hair
    //public void OnCharacterHairSelected(int index)
    //{
    //    //Update Current Hair Color
    //    Globals.CurrentCharacterHairType = (Globals.CharacterHairType)index;
    //}

    ////Update Current Character Cloth
    //public void OnCharacterClothTopSelected(int index)
    //{
    //    //Update Current Cloth Type
    //    Globals.CurrentCharacterClothTopType = (Globals.CharacterClothTopType)index;
    //}

    ////Update Current Character Cloth
    //public void OnCharacterClothBottomSelected(int index)
    //{
    //    //Update Current Cloth Type
    //    Globals.CurrentCharacterClothBottomType = (Globals.CharacterClothBottomType)index;
    //}

    ////Update Current Character Shoe
    //public void OnCharacterShoeSelected(int index)
    //{
    //    //Update Current Cloth Type
    //    Globals.CurrentCharacterShoeType = (Globals.CharacterShoeType)index;
    //}




    //-------Avatar Selection---------------
    public void NextAvatar(){
        CurrentAvatarIndex++;
        LoadAvatar();
    }

    public void PreviousAvatar(){
        CurrentAvatarIndex--;
        LoadAvatar();
    }


    void LoadAvatar() {
        //prevent index overflow
        if (CurrentAvatarIndex < 0)
            CurrentAvatarIndex = Avatars.Count - 1;
        if (CurrentAvatarIndex >= Avatars.Count)
            CurrentAvatarIndex = 0;

        //Load Specified Avatar
        for (int i = 0; i < Avatars.Count; i++){
            if ((int)Avatars[i].myAvatar == CurrentAvatarIndex)
                Avatars[i].gameObject.SetActive(true);
            else
                Avatars[i].gameObject.SetActive(false);
        }
        

        //Globals.ActiveCustomizationOption = Globals.CustomizationOptionType.avatarType;

        OnAvatarCustomizationOptionSelected(Globals.CustomizationOptionType.avatarType , ((Globals.Avatar)CurrentAvatarIndex).ToString());
    }
    //---------------




    //Call API to Update user profile
    public void UpdateUserProfile()
    {

        //Donot Call any Backend API is it is a Guest Login
        if (Globals.IsGuestLogin)
            return;


        Messenger.Broadcast<string, PopupManager.Type>(Events.OnPopupRequired, "Saving Avatar.." , PopupManager.Type.loading);

        //Subscribe To UpdateUserProfile Response Event
        Messenger.AddListener<string>(Events.OnRESTAPICallback, OnUpdateUserProfileResponseRecieved);

        //New UpdateUserProfile Object 
        UpdateUserProfileRequestRootObject updateUserProfileRequestRootObject = new UpdateUserProfileRequestRootObject();

        //Feed in Information as Updated
        updateUserProfileRequestRootObject.UserId = Globals.myUserID;
        updateUserProfileRequestRootObject.Gender = Globals.CurrentCharacterSex.ToString();
        updateUserProfileRequestRootObject.FirstName = Globals.myFirstName;
        //updateUserProfileRequestRootObject.LastName = "Dummy";
        updateUserProfileRequestRootObject.Email = Globals.myEmail;
        updateUserProfileRequestRootObject.CountryId = Globals.myCountry;
        //updateUserProfileRequestRootObject.Phone = "dummy";
        //updateUserProfileRequestRootObject.Interests = "dummy";

        updateUserProfileRequestRootObject.CharacterType = Globals.CurrentCharacterType.ToString();
        updateUserProfileRequestRootObject.TypeOfClothTopColor = Globals.CurrentCharacterClothTopType.ToString();
        updateUserProfileRequestRootObject.TypeOfClothBottomColor = Globals.CurrentCharacterClothBottomType.ToString();
        updateUserProfileRequestRootObject.TypeOfHairColor = Globals.CurrentCharacterHairType.ToString();
        updateUserProfileRequestRootObject.TypeOfShoeColor = Globals.CurrentCharacterShoeType.ToString();


        //Prepare Raw data
        string rawData = JsonUtility.ToJson(updateUserProfileRequestRootObject);
        //Debug.LogError(rawData);
        //Call the API
        RestAPICallManager.CallRESTAPI(BestHTTP.HTTPMethods.Put, Globals.UpdateUserProfileRESTAPIURL, rawData);
        //Messenger.Broadcast<BestHTTP.HTTPMethods, string, string>(Events.OnCallRESTAPIType1 , BestHTTP.HTTPMethods.Put, Globals.UpdateUserProfileRESTAPIURL, rawData);
    }

    void AddCustomProperty(string key , string value) {
        //If Custom property Already Exists then Update the value
        if (myCustomProperties.ContainsKey(key))
        {
            myCustomProperties[key] = value;
        }
        //If not then Add that new Custom Property as a new entity
        else
            myCustomProperties.Add(key, value);
    }

    //Call API to Get user profile
    public void GetUserProfile()
    {

        if (Globals.IsGuestLogin)
        {
            Globals.myFirstName = "Guest_" + Random.Range(0 ,100);
            Globals.TotalCoins = 0;
            Globals.Level = 0;
            ReflectGuestLoginUserProfile();
            //Messenger.Broadcast(Events.OnMainMenuLoadedCompletely);
        }
        else
        {

            Messenger.Broadcast<string, PopupManager.Type>(Events.OnPopupRequired, "Getting User Profile..", PopupManager.Type.loading);

            //Subscribe To GetUserProfile Response Event
            Messenger.AddListener<string>(Events.OnRESTAPICallback, OnGetUserProfileResponseRecieved);
            //Call the API
            Globals.DebugMessages.Add("calling GetUserProfileRESTAPIURL with userID = " + Globals.myUserID);
            RestAPICallManager.CallRESTAPI(BestHTTP.HTTPMethods.Get, Globals.GetUserProfileRESTAPIURL, "userId", Globals.myUserID);
            //Messenger.Broadcast<BestHTTP.HTTPMethods, string, string, string>(Events.OnCallRESTAPIType2 , BestHTTP.HTTPMethods.Get, Globals.GetUserProfileRESTAPIURL, "userId", Globals.myUserID);
        }
    }





    //------------Callbacks------------------------------
    //Update User Profile callback
    void OnUpdateUserProfileResponseRecieved(string response)
    {
        //Debug.LogError(response);

        //UpdateUserProfileResponse obj
        UpdateUserProfileResponseRootObject updateUserProfileResponseRootObject = new UpdateUserProfileResponseRootObject();

        //Deserialize The JSON Object
        try
        {
            //----Success----
            updateUserProfileResponseRootObject = JsonUtility.FromJson<UpdateUserProfileResponseRootObject>(response);

        }
        catch (System.Exception e)
        {
            //----Fail----
            updateUserProfileResponseRootObject.Success = false;
            Debug.LogError("Something Wrong here");
            return;
        }

        //remove Loading Popuup
        Messenger.Broadcast<PopupManager.Type>(Events.OnPopupEnd, PopupManager.Type.loading);

        //UnSubscribe To Login Response Event
        Messenger.RemoveListener<string>(Events.OnRESTAPICallback, OnUpdateUserProfileResponseRecieved);
    }
    //Update User Profile callback
    void OnGetUserProfileResponseRecieved(string response)
    {
        Globals.DebugMessages.Add("OnGetUserProfileResponseRecieved");

        //Response Equivalent Root Object
        try
        {
            GetUserProfileRequestRootObject getUserProfileRequestRootObject = new GetUserProfileRequestRootObject();

            //Deserialize The JSON Object
            getUserProfileRequestRootObject = JsonUtility.FromJson<GetUserProfileRequestRootObject>(response);

            if (getUserProfileRequestRootObject.Success)
            {
                Globals.DebugMessages.Add("userFetched Successfully");
                ReflectUserProfileInGameAsPerBackendAPI(getUserProfileRequestRootObject);
            }
            else
            {
                Globals.DebugMessages.Add("Unable To get user!!" + getUserProfileRequestRootObject.Message);
                Messenger.Broadcast<string, PopupManager.Type>(Events.OnPopupRequired, "Unable To get user!!", PopupManager.Type.retry);
            }

        }
        catch(System.Exception e)
        {
            Globals.DebugMessages.Add("Unable to get user");
            Messenger.Broadcast<string, PopupManager.Type>(Events.OnPopupRequired, "Unable To get user!!", PopupManager.Type.retry);
        }


        //Disable Save Avatar Button Initially
        SaveChangesButtonRef.SetActive(false);

        //UnSubscribe To Login Response Event
        Messenger.RemoveListener<string>(Events.OnRESTAPICallback, OnGetUserProfileResponseRecieved);

    }



    void ReflectGuestLoginUserProfile()
    {
        //Parse The String to Enum
        //Globals.Avatar avatarType = (Globals.Avatar)System.Enum.Parse(typeof(Globals.Avatar), getUserProfileRequestRootObject.Message.CharacterType, true);
        //Save Global param
        //Globals.CurrentCharacterType = avatarType;
        //Globals.FirstName = getUserProfileRequestRootObject.Message.FirstName;

        Globals.Avatar parsed_enum = (Globals.Avatar)System.Enum.Parse(typeof(Globals.Avatar), Globals.CurrentCharacterType);

        //Save Avatar Type Index
        CurrentAvatarIndex = (int)parsed_enum;


        LoadAvatar();

   
        //------------Update The 3D avatar Params------------------
        //Enable the Choosen 3D Avatar and Update it's [hair , Top , Bottom and Shoe ]
        foreach (AvatarUnit unit in Avatars)
        {
            // Debug.LogError( unit.myAvatar.ToString() + "====" + getUserProfileRequestRootObject.Message.CharacterType);

            if (unit.myAvatar.ToString() == Globals.CurrentCharacterType.ToString())
            {
                //Enable Avatar Gameobject
                unit.gameObject.SetActive(true);

                //Apply 3D Customization Paramaters
                Globals.ActiveCustomizationOption = Globals.CustomizationOptionType.hair;
                unit.OnAvatarCustomizationOptionSelected(Globals.CurrentCharacterHairType);


                Globals.ActiveCustomizationOption = Globals.CustomizationOptionType.top;
                unit.OnAvatarCustomizationOptionSelected(Globals.CurrentCharacterClothTopType);

                Globals.ActiveCustomizationOption = Globals.CustomizationOptionType.bottom;
                unit.OnAvatarCustomizationOptionSelected(Globals.CurrentCharacterClothBottomType);


                Globals.ActiveCustomizationOption = Globals.CustomizationOptionType.shoe;
                unit.OnAvatarCustomizationOptionSelected(Globals.CurrentCharacterShoeType);
                break;
            }
            else
                //Enable Avatar Gameobject
                unit.gameObject.SetActive(false);
        }

        SaveChangesButtonRef.SetActive(false);
        Messenger.Broadcast<GetUserProfileRequestRootObject>(Events.OnGetUserProfile, null);
    }



    void ReflectUserProfileInGameAsPerBackendAPI (GetUserProfileRequestRootObject getUserProfileRequestRootObject) {
        //Parse The String to Enum
        Globals.Avatar avatarType = (Globals.Avatar)System.Enum.Parse(typeof(Globals.Avatar), getUserProfileRequestRootObject.Message.CharacterType , true);

        //Save Global param
        Globals.CurrentCharacterType = avatarType.ToString();
        Globals.myFirstName = getUserProfileRequestRootObject.Message.FirstName;
        Globals.myEmail = getUserProfileRequestRootObject.Message.Email;
        Globals.TotalCoins = getUserProfileRequestRootObject.Message.TotalCoins;
        Globals.Level = getUserProfileRequestRootObject.Message.Level;

        //Save Avatar Type Index
        CurrentAvatarIndex = (int)avatarType;

        LoadAvatar();

        //Save Global param for Hair , Top , bottom , shoe
        Globals.CurrentCharacterHairType = getUserProfileRequestRootObject.Message.TypeOfHairColor;
        Globals.CurrentCharacterClothTopType = getUserProfileRequestRootObject.Message.TypeOfClothTopColor;
        Globals.CurrentCharacterClothBottomType = getUserProfileRequestRootObject.Message.TypeOfClothBottomColor;
        Globals.CurrentCharacterShoeType = getUserProfileRequestRootObject.Message.TypeOfShoeColor;
        

        //------------Update The 3D avatar Params------------------
        //Enable the Choosen 3D Avatar and Update it's [hair , Top , Bottom and Shoe ]
        foreach (AvatarUnit unit in Avatars){
           // Debug.LogError( unit.myAvatar.ToString() + "====" + getUserProfileRequestRootObject.Message.CharacterType);

            if (unit.myAvatar.ToString() == getUserProfileRequestRootObject.Message.CharacterType) {
                //Enable Avatar Gameobject
                unit.gameObject.SetActive(true);

                //Apply 3D Customization Paramaters
                Globals.ActiveCustomizationOption = Globals.CustomizationOptionType.hair;
                unit.OnAvatarCustomizationOptionSelected(Globals.CurrentCharacterHairType);

                Globals.ActiveCustomizationOption = Globals.CustomizationOptionType.top;
                unit.OnAvatarCustomizationOptionSelected(Globals.CurrentCharacterClothTopType);

                Globals.ActiveCustomizationOption = Globals.CustomizationOptionType.bottom;
                unit.OnAvatarCustomizationOptionSelected(Globals.CurrentCharacterClothBottomType);

                Globals.ActiveCustomizationOption = Globals.CustomizationOptionType.shoe;
                unit.OnAvatarCustomizationOptionSelected(Globals.CurrentCharacterShoeType);
                break;
            }
            else
                //Enable Avatar Gameobject
                unit.gameObject.SetActive(false);

        }

        SaveChangesButtonRef.SetActive(false);
        //Fill up the Retrieved Information and Enable Interactable UI || or me
        Messenger.Broadcast<GetUserProfileRequestRootObject>(Events.OnGetUserProfile, getUserProfileRequestRootObject);
    }


    //private void OnGUI()
    //{

    //    GUILayout.TextField("Current Avatar =  " + Globals.CurrentCharacterType);
    //    GUILayout.TextField("Current hair =  " + Globals.CurrentCharacterHairType);
    //    GUILayout.TextField("Current top =  " + Globals.CurrentCharacterClothTopType);
    //    GUILayout.TextField("Current bottom =  " + Globals.CurrentCharacterClothBottomType);
    //    GUILayout.TextField("Current shoe =  " + Globals.CurrentCharacterShoeType);
    //}




    //private void Update()
    //{
    //    Debug.LogError(Globals.ActiveCustomizationOption.ToString());
    //}




    //Update And Save The Updated Parameter
    void OnAvatarCustomizationOptionSelected(string customizationStringValue)
    {

        //Update The Global's "Current" parameters for all Avatar Customization options
        switch (Globals.ActiveCustomizationOption)
        {
            case Globals.CustomizationOptionType.hair:
                Globals.CurrentCharacterHairType = customizationStringValue;
                break;
            case Globals.CustomizationOptionType.top:
                Globals.CurrentCharacterClothTopType = customizationStringValue;
                break;
            case Globals.CustomizationOptionType.bottom:
                Globals.CurrentCharacterClothBottomType = customizationStringValue;
                break;
            case Globals.CustomizationOptionType.shoe:
                Globals.CurrentCharacterShoeType = customizationStringValue;
                break;
            case Globals.CustomizationOptionType.avatarType:
                Globals.CurrentCharacterType = customizationStringValue;
                break;
        }

        //We made a Customization hence it should be true to diffrentiate during The Disabling of the AvatarCustomizationMenu
        wasAvatarCustomized = true;
        SaveChangesButtonRef.SetActive(true);
    }


    //Update And Save The Updated Parameter
    void OnAvatarCustomizationOptionSelected(Globals.CustomizationOptionType optionType, string customizationStringValue)
    {

        //Update The Global's "Current" parameters for all Avatar Customization options
        switch (optionType)
        {
            case Globals.CustomizationOptionType.hair:
                Globals.CurrentCharacterHairType = customizationStringValue;
                break;
            case Globals.CustomizationOptionType.top:
                Globals.CurrentCharacterClothTopType = customizationStringValue;
                break;
            case Globals.CustomizationOptionType.bottom:
                Globals.CurrentCharacterClothBottomType = customizationStringValue;
                break;
            case Globals.CustomizationOptionType.shoe:
                Globals.CurrentCharacterShoeType = customizationStringValue;
                break;
            case Globals.CustomizationOptionType.avatarType:
                Globals.CurrentCharacterType = customizationStringValue;
                break;
        }

        //We made a Customization hence it should be true to diffrentiate during The Disabling of the AvatarCustomizationMenu
        wasAvatarCustomized = true;
        SaveChangesButtonRef.SetActive(true);
    }


    void OnAvatarCustomizationOptionTypeSelected(Globals.CustomizationOptionType option)
    {
        //update the Active Customization Option
        Globals.ActiveCustomizationOption = option;
    }



    void OnAvatarCustomizationStarted()
    {
        wasAvatarCustomized = false;
    }


    public void OnAvatarCustomizationFinished()
    {
        //If we did not modify any of the available Customization options then Just Dont do anything
        if (!wasAvatarCustomized)
            return;



        //Else Update the Updated Customization Parameters to the backend[Call the UpdateUserProfileAPI]
         UpdateUserProfile();

        //Reset Was Avatart Cutomized to False
        wasAvatarCustomized = false;

        //Disable The save Changes button
        SaveChangesButtonRef.SetActive(false);
    }

    
}
