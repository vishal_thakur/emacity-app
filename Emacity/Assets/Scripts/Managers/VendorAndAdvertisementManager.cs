﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VendorAndAdvertisementManager : MonoBehaviour
{

    //The Types of shops as per shopLevel [small , medium , Big]
    //[SerializeField]
    //List<ShopUnit> smallShopUnits;
    [SerializeField]
    List<ShopUnit> mediumShopUnits;
    //[SerializeField]
    //List<ShopUnit> bigShopUnits;

    [SerializeField]
    List<AdvertisementUnit> advertisementUnits;


    [SerializeField, Space(10)]
    float totalUnitsLoadingProgress = 0;

    [SerializeField]
    float totalUnitsToLoad = 0;


    //void OnGUI()
    //{
    //    GUIStyle style = new GUIStyle();
    //    style.wordWrap = true;
    //    style.normal.textColor = Color.red;
    //    style.fontSize = 30;
    //    style.fontStyle = FontStyle.Bold;


    //    GUILayout.TextField("Progress = " + totalUnitsLoadingProgress, style);
    //    GUILayout.TextField("Total = " + totalUnitsToLoad, style);
    //}



    // Start is called before the first frame update
    IEnumerator Start()
    {
        yield return new WaitForEndOfFrame();

        //Broadcast that All Vendor and Adverisement Units were Loaded Succesfully
        //Messenger.Broadcast(Events.OnAllVendorsAndAdvertisementUnitsLoaded);
         GetVendorsAndAdvertisementsInfo();
    }



    //Call API to Get Vendor and Advertisement units Info
    public void GetVendorsAndAdvertisementsInfo()
    {
        //Calculate total Units to Load which includes all vendor and Advertisement Units
        //totalUnitsToLoad = smallShopUnits.Count + mediumShopUnits.Count + bigShopUnits.Count + advertisementUnits.Count;
        totalUnitsToLoad = mediumShopUnits.Count + advertisementUnits.Count;

        //Subscribe To Vendor unit loaded Successfully Event
        Messenger.AddListener<GetVendorsInfoResponseMessage, Texture, Texture, Texture>(Events.OnVendorUnitLoadedSuccessfully, OnVendorUnitLoadedSuccessfully);

        //Subscribe To Advertisement unit loaded Successfully Event
        Messenger.AddListener<List<GetAdvertisementsResponeMessage>, List<Texture>>(Events.OnAdvertisementUnitLoadedSuccessfully, OnAdvertisementUnitLoadedSuccessfully);


        //Load Vendors
        //If Cached Vendor Data is available then Load from Cache
        if (Globals.vendorUnitsDataList.Count > 0)
        {
            StartCoroutine(InitializeVendorUnits());
        }
        else//Otherwise then Load Vendor Units Info from API 
        {
            //Subscribe To getVendorsInfo Response Event
            Messenger.AddListener<string>(Events.OnRESTAPICallback, OnGetVendorsInfoResponseRecieved);

            //Call the API
            RestAPICallManager.CallRESTAPI(BestHTTP.HTTPMethods.Get, Globals.GetVendorsInfoURL);
        }
    }





    #region Event Callbacks

    void OnPopupRetrySelected()
    {
        GetVendorsAndAdvertisementsInfo();

        Messenger.RemoveListener(Events.OnPopupRetrySelected, OnPopupRetrySelected);
    }

    //Update Vendor Info Profile callback
    void OnGetAdvertisementsInfoResponseRecieved(string response)
    {

        //Response Equivalent Root Object
        GetAdvertisementsResponeRootObject getAdvertisementsResponeRootObject = new GetAdvertisementsResponeRootObject();

        try
        {
            getAdvertisementsResponeRootObject = JsonUtility.FromJson<GetAdvertisementsResponeRootObject>(response);

            if (getAdvertisementsResponeRootObject.Success)
            {
                //If Zero Advertisement Units in the Callback Give error Popup
                if (getAdvertisementsResponeRootObject.Message.Count == 0)
                {
                    Messenger.AddListener(Events.OnPopupRetrySelected, OnPopupRetrySelected);
                }
                else//Load Advertisement Units in game Scene
                    StartCoroutine(InitializeAdvertisementUnits(getAdvertisementsResponeRootObject));
            }
            {
                Messenger.AddListener(Events.OnPopupRetrySelected, OnPopupRetrySelected);
            }

        }
        catch (System.Exception e)
        {
            Messenger.AddListener(Events.OnPopupRetrySelected, OnPopupRetrySelected);
        }

        //UnSubscribe To Response Event
        Messenger.RemoveListener<string>(Events.OnRESTAPICallback, OnGetAdvertisementsInfoResponseRecieved);
    }



    //Update Vendor Info Profile callback
    void OnGetVendorsInfoResponseRecieved(string response)
    {

        //Response Equivalent Root Object
        GetVendorsInfoResponseRootObject getVendorsInfoResponseRootObject = new GetVendorsInfoResponseRootObject();

        try
        {
            getVendorsInfoResponseRootObject = JsonUtility.FromJson<GetVendorsInfoResponseRootObject>(response);

            //Successful
            if (getVendorsInfoResponseRootObject.Success)
            {
                //Successful but No Vendors in The List
                if (getVendorsInfoResponseRootObject.Message.Count == 0)
                {
                    Messenger.AddListener(Events.OnPopupRetrySelected, OnPopupRetrySelected);
                }
                else//Successful and Have a list of Vendors
                    StartCoroutine(InitializeVendorUnits(getVendorsInfoResponseRootObject));
            }
            else//UnSuccessful
            {
                Messenger.AddListener(Events.OnPopupRetrySelected, OnPopupRetrySelected);
            }
        }
        catch (System.Exception e)
        {
            Messenger.AddListener(Events.OnPopupRetrySelected, OnPopupRetrySelected);
        }

        //UnSubscribe To Response Event
        Messenger.RemoveListener<string>(Events.OnRESTAPICallback, OnGetVendorsInfoResponseRecieved);


        //Load Advertisements
        //If Cached Advertisement Data is available then Load from Cache
        if (Globals.advertisementUnitsDataList.Count > 0)
        {
            StartCoroutine(InitializeAdvertisementUnits());
        }
        else//Otherwise then Load Advertisement Units Info from API 
        {
            //Subscribe To GetAdvertisementsInfo Response Event
            Messenger.AddListener<string>(Events.OnRESTAPICallback, OnGetAdvertisementsInfoResponseRecieved);
            //Call the API
            RestAPICallManager.CallRESTAPI(BestHTTP.HTTPMethods.Get, Globals.GetAdvertisementsInfoURL);
        }
    }



    void OnVendorUnitLoadedSuccessfully(GetVendorsInfoResponseMessage myInfo, Texture frontAdTexture, Texture leftAdTexture, Texture rightAdTexture)
    {
        totalUnitsLoadingProgress++;
        

        Messenger.Broadcast<string, PopupManager.Type>(Events.OnPopupRequired, "Loading Vendors " + (int)((totalUnitsLoadingProgress / totalUnitsToLoad) * 100) + "%", PopupManager.Type.loading);


        //If Fetched From API then save it to Cache
        if (myInfo != null)
        {
            ShopUnitData newShopUnitData = new ShopUnitData(myInfo, frontAdTexture, leftAdTexture, rightAdTexture);
            Globals.vendorUnitsDataList.Add(newShopUnitData);
        }


        if (totalUnitsLoadingProgress == totalUnitsToLoad)
        {

            //Broadcast that All Vendor and Adverisement Units were Loaded Succesfully
            Messenger.Broadcast(Events.OnAllVendorsAndAdvertisementUnitsLoaded);

            //Unsubscribe from Vendor unit loaded Successfully as now all vendor Units have been loaded 
            Messenger.RemoveListener<GetVendorsInfoResponseMessage, Texture, Texture, Texture>(Events.OnVendorUnitLoadedSuccessfully, OnVendorUnitLoadedSuccessfully);
        }

    }

    void OnAdvertisementUnitLoadedSuccessfully(List<GetAdvertisementsResponeMessage> myInfo, List<Texture> adTextures)
    {
        totalUnitsLoadingProgress++;
        

        Messenger.Broadcast<string, PopupManager.Type>(Events.OnPopupRequired, "Loading Advertisements " + (int)((totalUnitsLoadingProgress / totalUnitsToLoad) * 100) + "%", PopupManager.Type.loading);


        //If Fetched From API then save it to Cache
        if (myInfo != null)
        {
            AdvertisementUnitData newAdvertisementUnitData = new AdvertisementUnitData(myInfo, adTextures);
            Globals.advertisementUnitsDataList.Add(newAdvertisementUnitData);
        }


        if (totalUnitsLoadingProgress == totalUnitsToLoad)
        {
            //Debug.LogError("All Advertisement Units Loaded Broadast");

            //Broadcast that All Vendor and Adverisement Units were Loaded Succesfully
            Messenger.Broadcast(Events.OnAllVendorsAndAdvertisementUnitsLoaded);

            //Unsubscribe from Advertisement unit loaded Successfully as now all advertisement Units have been loaded 
            Messenger.RemoveListener<List<GetAdvertisementsResponeMessage>, List<Texture>>(Events.OnAdvertisementUnitLoadedSuccessfully, OnAdvertisementUnitLoadedSuccessfully);
        }

    }
    #endregion



    #region API CALL LOADING
    IEnumerator InitializeVendorUnits(GetVendorsInfoResponseRootObject vendorUnitsInfoObj)
    {


        string shopLevel = "small";


        //int smallShopIndex = 0;
        int mediumShopIndex = 0;
        //int bigShopIndex = 0;



        //Spawn 3D models at specified Positions and Rotations as per API
        foreach (GetVendorsInfoResponseMessage vendor in vendorUnitsInfoObj.Message)
        {
            yield return null;

            //Switch Between Small , medium and Big Shops by shopLevel parameter
            shopLevel = vendor.ShopLevel.ToLower();

            if (mediumShopIndex < mediumShopUnits.Count)
            {
                mediumShopUnits[mediumShopIndex].Initialize(vendor);
                mediumShopIndex++;
            }

            /*
            if (shopLevel == Globals.ShopLevel.small.ToString().ToLower() && smallShopIndex < smallShopUnits.Count)
            {
                smallShopUnits[smallShopIndex].Initialize(vendor);
                smallShopIndex++;
            }
            else if (shopLevel == Globals.ShopLevel.medium.ToString() && mediumShopIndex < mediumShopUnits.Count)
            {
                mediumShopUnits[mediumShopIndex].Initialize(vendor);
                mediumShopIndex++;
            }
            else if (shopLevel == Globals.ShopLevel.big.ToString() && bigShopIndex < bigShopUnits.Count)
            {
                bigShopUnits[bigShopIndex].Initialize(vendor);
                bigShopIndex++;
            }
            */
        }

        StopCoroutine(InitializeVendorUnits(vendorUnitsInfoObj));
    }

    IEnumerator InitializeAdvertisementUnits(GetAdvertisementsResponeRootObject getAdvertisementsResponeRootObject)
    {
        //int adPanelIndex = 0;

        //Iterate Advertisements As per the API info
        foreach (GetAdvertisementsResponeMessage newAd in getAdvertisementsResponeRootObject.Message)
        {
            yield return null;
            //Iterate Through all Advertisement Units
            foreach (AdvertisementUnit advertisementUnit in advertisementUnits)
            {
                yield return null;
                
                if (advertisementUnit.allAdvertisements.Count > 0)
                {
                    if (advertisementUnit.allAdvertisements[0].Name == newAd.Name)
                    {
                        //Debug.LogError("Image = " + newAd.Image + " and " + "Video = " + newAd.DownloadURL);
                        //Decide Whether Advertisement Is of type Image or Video
                        newAd.type = string.IsNullOrEmpty(newAd.Image) ? Globals.AdvertisementType.video : Globals.AdvertisementType.image;

                        //if Neither Image nor Video URL is specified This means the adverisement is of type Disabled
                        if (string.IsNullOrEmpty(newAd.DownloadURL) && string.IsNullOrEmpty(newAd.Image))
                        {
                            newAd.type = Globals.AdvertisementType.disabled;
                        }
                        else
                        {
                            //Decide Whether Advertisement Is of type Image or Video
                            newAd.type = string.IsNullOrEmpty(newAd.Image) ? Globals.AdvertisementType.video : Globals.AdvertisementType.image;
                        }

                        //Debug.LogError("Type = " + newAd.type);

                        advertisementUnit.allAdvertisements.Add(newAd);
                        break;
                    }
                }
                else
                {
                    //Decide Whether Advertisement Is of type Image or Video
                    newAd.type = string.IsNullOrEmpty(newAd.Image) ? Globals.AdvertisementType.video : Globals.AdvertisementType.image;
                    
                    //if Neither Image nor Video URL is specified This means the adverisement is of type Disabled
                    if (string.IsNullOrEmpty(newAd.DownloadURL) && string.IsNullOrEmpty(newAd.Image))
                    {
                        newAd.type = Globals.AdvertisementType.disabled;
                    }
                    else
                    {
                        //Decide Whether Advertisement Is of type Image or Video
                        newAd.type = string.IsNullOrEmpty(newAd.Image) ? Globals.AdvertisementType.video : Globals.AdvertisementType.image;
                    }

                    advertisementUnit.allAdvertisements.Add(newAd);
                    break;
                }
            }
        }


        //Iterate Through all Advertisement Units
        foreach (AdvertisementUnit advertisementUnit in advertisementUnits)
        {
            yield return null;

           advertisementUnit.Initialize();
        }


        StopCoroutine(InitializeAdvertisementUnits(getAdvertisementsResponeRootObject));
    }

    #endregion


    #region Cached Loading
    //For Loading Vendors For The Cached VendorInfo as per the Globals Class
    IEnumerator InitializeVendorUnits()
    {
        //Calculate total Units to Load
        string shopLevel = "small";

        //int smallShopIndex = 0;
        int mediumShopIndex = 0;
        //int bigShopIndex = 0;

        foreach (ShopUnitData shopUnitData in Globals.vendorUnitsDataList)
        {
            yield return null;

            //Switch Between Small , medium and Big Shops by shopLevel parameter
            shopLevel = shopUnitData.MyInfo.ShopLevel.ToLower();


            if (mediumShopIndex < mediumShopUnits.Count)
            {
                mediumShopUnits[mediumShopIndex].Initialize(shopUnitData);
                mediumShopIndex++;
            }

            /*
            if (shopLevel == Globals.ShopLevel.small.ToString().ToLower() && smallShopIndex < smallShopUnits.Count)
            {
                smallShopUnits[smallShopIndex].Initialize(shopUnitData);
                smallShopIndex++;
            }
            else if (shopLevel == Globals.ShopLevel.medium.ToString() && mediumShopIndex < mediumShopUnits.Count)
            {
                mediumShopUnits[mediumShopIndex].Initialize(shopUnitData);
                mediumShopIndex++;
            }
            else if (shopLevel == Globals.ShopLevel.big.ToString() && bigShopIndex < bigShopUnits.Count)
            {
                bigShopUnits[bigShopIndex].Initialize(shopUnitData);
                bigShopIndex++;
            }
            */
        }


        //Load Advertisements
        //If Cached Advertisement Data is available then Load from Cache
        if (Globals.advertisementUnitsDataList.Count > 0)
        {
            StartCoroutine(InitializeAdvertisementUnits());
        }

        StopCoroutine(InitializeVendorUnits());
    }


    IEnumerator InitializeAdvertisementUnits()
    {
        int adIndex = 0;
        //Iterate Advertisements As per the API info
        foreach (AdvertisementUnitData newAd in Globals.advertisementUnitsDataList)
        {
            yield return null;

            //Save All Advertisements
            foreach (GetAdvertisementsResponeMessage ad in newAd.MyInfo)
            {
                yield return null;
                advertisementUnits[adIndex].allAdvertisements.Add(ad);
            }

            //Save all Textures
            foreach(Texture texture in newAd.AdTextures){
                yield return null;
                advertisementUnits[adIndex].adTextures.Add(texture);
            }


            adIndex++;
                
        }
        Debug.LogError("Index = " +  adIndex.ToString());

        //Iterate Through all Advertisement Units
        foreach (AdvertisementUnit advertisementUnit in advertisementUnits)
        {
            yield return null;

            advertisementUnit.Initialize();
        }

        StopCoroutine(InitializeAdvertisementUnits());
    }
    #endregion
    

}
