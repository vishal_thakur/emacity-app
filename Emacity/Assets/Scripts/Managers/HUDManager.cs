﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;

public class HUDManager : MonoBehaviour
{
    [SerializeField]
    Button SpellButtonRef;

    [SerializeField]
    Button ShieldButtonRef;

    [SerializeField]
    Button LadderButtonRef;

    [SerializeField]
    Toggle SoundToggleRef;

    void Start()
    {
        Messenger.AddListener<PhotonView,float>(Events.OnHitBySpell, OnHitBySpell);
        SoundToggleRef.SetIsOnWithoutNotify(Globals.GameSoundStatus);
        SoundToggleRef.GetComponent<SoundButton>().OnToggle(Globals.GameSoundStatus);

        //Disable Spell if Spell Duration is Zero or the User Level is zero
        if (Globals.Level == 0)
            SpellButtonRef.interactable = false;

        //Disable Shield if Shield Duration is Zero or the User Level is zero
        if (Globals.Level == 0)
            ShieldButtonRef.interactable = false;
    }

    private void OnDestroy()
    {
        Messenger.RemoveListener<PhotonView,float>(Events.OnHitBySpell , OnHitBySpell);
    }

    public void OnRotationSliderUpdated(float value)
    {
        //Debug.LogError(value);
        Messenger.Broadcast<float>(Events.OnCameraRotationSliderUpdated , value);
    }
    
    public void OnGameSoundToggled(bool flag)
    {
        Messenger.Broadcast<bool>(Events.OnGameSoundToggled , flag);
    }

    public void CastSpell()
    {
        Messenger.Broadcast(Events.OnSpellCasted);
        StartCoroutine(DisableButtonForAWhile(SpellButtonRef , Globals.SpellReloadDuration));
    }

    public void UseLadder()
    {
        Messenger.Broadcast(Events.OnLadderUsed);
    }

    public void UseShield()
    {
        Messenger.Broadcast(Events.OnShieldUsed);
        StartCoroutine(DisableButtonForAWhile(ShieldButtonRef, Globals.ShieldReloadDuration));
    }



    void OnHitBySpell(PhotonView PV , float duration)
    {
        if (PV.IsMine)
        {
            StartCoroutine(OnHitBySpellBehaviour(duration));
        }
    }


    IEnumerator DisableButtonForAWhile(Button button, float duration)
    {
        yield return new WaitForEndOfFrame();
        button.interactable = false;
        yield return new WaitForSeconds(duration);
        button.interactable = true;
        StopCoroutine(DisableButtonForAWhile(button , duration));
    }


    IEnumerator OnHitBySpellBehaviour(float duration)
    {
        //Disable Ladder , Spell and Shield Buttons for the duration that User Has been hit by spell
        SpellButtonRef.gameObject.SetActive(false);
        ShieldButtonRef.gameObject.SetActive(false);
        LadderButtonRef.gameObject.SetActive(false);

        yield return new WaitForSeconds(duration);

        //Enable Ladder , Spell and Shield Buttons back again
        SpellButtonRef.gameObject.SetActive(true);
        ShieldButtonRef.gameObject.SetActive(true);
        LadderButtonRef.gameObject.SetActive(true);

        StopCoroutine(OnHitBySpellBehaviour(duration));
    }
}
