﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;


//This Handles the Game Logic, Is the Decisive Part
public class AppManager : MonoBehaviourPunCallbacks , IInRoomCallbacks
{
    [SerializeField]
    public List<UIPanel> UIPanels;
    

    [SerializeField]
    List<UIPanel> UIPanelsStack;

    [SerializeField, Space(10)]
    PopupManager popupManagerRef;

    [SerializeField]
    SampleWebView SampleWebViewRef;
    



    void Start()
    {

        //DontDestroyOnLoad(this.gameObject);
        switch (SceneManager.GetActiveScene().name.ToLower())
        {
            case "login":
                Messenger.AddListener<LoginResponseRootObject>(Events.OnLoginSuccessful, OnLoginSuccessful);
                Messenger.AddListener(Events.OnSignupSuccessful, OnSignupSuccessful);
                Messenger.AddListener(Events.OnLoginTried, OnLoginTried);
                Messenger.AddListener<string>(Events.OnLoginFailed, OnLoginFailed);
                ShowUIPanel(Globals.UIPanel.signIn);
                break;

            case "mainmenu":
                Messenger.AddListener<GetUserProfileRequestRootObject>(Events.OnGetUserProfile, OnGetUserProfile);
                Messenger.AddListener(Events.OnStoreSelected, OnStoreSelected);
                Messenger.AddListener<bool>(Events.OnGameSoundToggled, OnGameSoundToggled);
                Messenger.AddListener<int>(Events.OnQuizGameRewardRecieved, OnQuizGameRewardReceived);

                //IAP
                Messenger.AddListener(Events.OnIAPSkillLadderPurchaseSuccess, OnIAPSkillLadderPurchaseSuccess);
                Messenger.AddListener<string>(Events.OnIAPCoinsPurchaseSuccess, OnIAPCoinsPurchaseSuccess);
                Messenger.AddListener<string>(Events.OnIAPSpellPurchaseSuccess, OnIAPSpellPurchaseSuccess);
                Messenger.AddListener<string>(Events.OnIAPShieldPurchaseSuccess, OnIAPShieldPurchaseSuccess);

                //Firebase Cloud Messaging
                Firebase.Messaging.FirebaseMessaging.TokenReceived += OnTokenReceived;
                Firebase.Messaging.FirebaseMessaging.MessageReceived += OnMessageReceived;
                break;

            case "gamescene":
                Messenger.AddListener(Events.OnAllVendorsAndAdvertisementUnitsLoaded, OnAllVendorsAndAdvertisementUnitsLoaded);
                Messenger.AddListener<string, string>(Events.OnShopInteracted, OnShopInteracted);
                Messenger.AddListener(Events.OnAdvertisementInteracted, OnAdvertisementInteracted);
                Messenger.AddListener(Events.OnReadyToExitGame, OnReadyToExitGame);
                Messenger.AddListener<PuzzleUnit>(Events.OnPuzzlePieceCollected, OnPuzzlePieceCollected);
                Messenger.AddListener<bool>(Events.OnGameSoundToggled, OnGameSoundToggled);
                Messenger.AddListener<int>(Events.OnQuizGameRewardRecieved, OnQuizGameRewardReceived);
                Firebase.Messaging.FirebaseMessaging.TokenReceived += OnTokenReceived;
                Firebase.Messaging.FirebaseMessaging.MessageReceived += OnMessageReceived;
                break;
        }


        //Mandatory Broadcast Listeners
        Messenger.AddListener<string, PopupManager.Type>(Events.OnPopupRequired, OnPopupRequired);
        //Mandatory Broadcast Listeners
        Messenger.AddListener<PopupManager.Type>(Events.OnPopupEnd, OnPopupEnd);

        //if (SceneManager.GetActiveScene().name == Globals.SceneNames.Login.ToString())
        //    yield return new WaitForSeconds(1);
        //else
        //yield return null;



    }


    void OnIAPSkillLadderPurchaseSuccess()
    {
        //Enable Ladder Button Permanently for this User
        Messenger.Broadcast<string, PopupManager.Type>(Events.OnPopupRequired, "Ladder Purchase Success, Publish to store first!", PopupManager.Type.notification);
    }

    void OnIAPCoinsPurchaseSuccess(string quantity)
    {
        //Add X coins to User's total Coins 
        //Update the same to Backend aswell
        Messenger.Broadcast<string, PopupManager.Type>(Events.OnPopupRequired, quantity  + " Coins Purchase Success, Publish to store first!", PopupManager.Type.notification);
    }
    
    void OnIAPSpellPurchaseSuccess(string duration)
    {
        //Set the new duration of the Spell , When Casted the opponent shall remain frozen for the Spell Duration
        //Update the same to Backend aswell

        Messenger.Broadcast<string, PopupManager.Type>(Events.OnPopupRequired, duration + " seconds of Spell duration Purchase Success, Publish to store first!", PopupManager.Type.notification);
    }

    void OnIAPShieldPurchaseSuccess(string duration)
    {
        //Set the new duration of the Shield, When used the shield shall remain active for the specified Shield Duration
        //Update the same to Backend aswell
        Messenger.Broadcast<string, PopupManager.Type>(Events.OnPopupRequired, duration + " seconds of Shield duration Purchase Success, Publish to store first!", PopupManager.Type.notification);
    }



    void OnDestroy()
    {
        //DontDestroyOnLoad(this.gameObject);
        switch (SceneManager.GetActiveScene().name.ToLower())
        {
            case "login":
                Messenger.RemoveListener<LoginResponseRootObject>(Events.OnLoginSuccessful, OnLoginSuccessful);
                Messenger.RemoveListener(Events.OnSignupSuccessful, OnSignupSuccessful);
                Messenger.RemoveListener(Events.OnLoginTried, OnLoginTried);
                Messenger.RemoveListener<string>(Events.OnLoginFailed, OnLoginFailed);
                break;

            case "mainmenu":
                Messenger.RemoveListener(Events.OnStoreSelected, OnStoreSelected);
                Messenger.RemoveListener<bool>(Events.OnGameSoundToggled, OnGameSoundToggled);
                Messenger.RemoveListener<int>(Events.OnQuizGameRewardRecieved, OnQuizGameRewardReceived);
                Firebase.Messaging.FirebaseMessaging.TokenReceived -= OnTokenReceived;
                Firebase.Messaging.FirebaseMessaging.MessageReceived -= OnMessageReceived;
                break;

            case "gamescene":
                Messenger.RemoveListener<string,string>(Events.OnShopInteracted, OnShopInteracted);
                Messenger.RemoveListener(Events.OnReadyToExitGame, OnReadyToExitGame);
                Messenger.RemoveListener(Events.OnAdvertisementInteracted, OnAdvertisementInteracted);
                Messenger.RemoveListener<PuzzleUnit>(Events.OnPuzzlePieceCollected, OnPuzzlePieceCollected);
                Messenger.RemoveListener<bool>(Events.OnGameSoundToggled, OnGameSoundToggled);
                Messenger.RemoveListener<int>(Events.OnQuizGameRewardRecieved, OnQuizGameRewardReceived);
                Firebase.Messaging.FirebaseMessaging.TokenReceived -= OnTokenReceived;
                Firebase.Messaging.FirebaseMessaging.MessageReceived -= OnMessageReceived;
                break;
        }

        //Mandatory Broadcast Listeners
        Messenger.RemoveListener<string, PopupManager.Type>(Events.OnPopupRequired, OnPopupRequired);

        //Mandatory Broadcast Listeners
        Messenger.RemoveListener<PopupManager.Type>(Events.OnPopupEnd, OnPopupEnd);
    }

    void OnGUI()
    {
        GUIStyle style = new GUIStyle();
        style.wordWrap = true;
        style.normal.textColor = Color.red;
        style.fontSize = 30;
        style.fontStyle = FontStyle.Normal;

        //try
        //{
        //    GUILayout.TextField("- " + Globals.Level, style);
        //}
        //catch (UnityException e)
        //{
        //    GUILayout.TextField(e.ToString(), style);
        //    Debug.LogError(e.ToString());
        //}
        //GUILayout.TextField(Messenger.PrintEventTable(), style);

        //foreach (string message in Globals.DebugMessages)
        //{
        //    GUILayout.TextField("- " + message, style);
        //}

        GUILayout.TextField("UseriD" + Globals.myUserID , style);
    }


    public void ShowPreviousUIPanel() {

        //Show the previous UIPanel form the Stack
        ShowUIPanel(UIPanelsStack[0]);
    }

    public void ShowUIPanel(Globals.UIPanel requiredUIpanel)
    {
        UIPanel uiPanel = GetPanel(requiredUIpanel);
        //Add new panel to the Stack [Avoid Overwrite]
        if (UIPanelsStack.Count < 2)
            UIPanelsStack.Add(uiPanel);
        //Only Keep Two UIPanels in the Stack so that we know whats the CURRENT and PREVIOUS uipanel
        else if (UIPanelsStack.Count >= 2)
        {
            if (UIPanelsStack[UIPanelsStack.Count - 1] != uiPanel)
            {
                UIPanelsStack.RemoveAt(0);
                UIPanelsStack.Add(uiPanel);
            }
            else
                return;
        }

        //Set The specified Panel As True
        LoadPanel((UIPanelsStack[UIPanelsStack.Count - 1].UIPanelType));


        if (SceneManager.GetActiveScene().name == Globals.SceneNames.MainMenu.ToString())
        {
            //Broadcast Which panel is Shown
            Messenger.Broadcast<UIPanel, UIPanel>(Events.OnUIPanelShown, UIPanelsStack[UIPanelsStack.Count - 1], UIPanelsStack[0]);
        }
    }


    public void ShowUIPanel(UIPanel uipanel)
    {
        //Add new panel to the Stack [Avoid Overwrite]
        if(UIPanelsStack.Count < 2)
            UIPanelsStack.Add(uipanel);
        //Only Keep Two UIPanels in the Stack so that we know whats the CURRENT and PREVIOUS uipanel
        else if(UIPanelsStack.Count >= 2)
        {
            if (UIPanelsStack[UIPanelsStack.Count - 1] != uipanel)
            {
                UIPanelsStack.RemoveAt(0);
                UIPanelsStack.Add(uipanel);
            }
            else
                return;
        }
        
        //Set The specified Panel As True
        LoadPanel((UIPanelsStack[UIPanelsStack.Count - 1].UIPanelType));


        if (SceneManager.GetActiveScene().name == Globals.SceneNames.MainMenu.ToString())
        {
            //Broadcast Which panel is Shown
            Messenger.Broadcast<UIPanel, UIPanel>(Events.OnUIPanelShown, UIPanelsStack[UIPanelsStack.Count - 1], UIPanelsStack[0]);
        }
    }


    //Loads Specified panel and HIDES all other
    void LoadPanel(Globals.UIPanel requiredPanel){
        for(int i=0;i< UIPanels.Count;i++){
            if (UIPanels[i].UIPanelType == requiredPanel)
            {
                UIPanels[i].gameObject.SetActive(true);
            }
            else
            {
                UIPanels[i].gameObject.SetActive(false);
            }

        }
        //Debug.LogError(requiredPanel + "Panel Not Found!!");
    }



    UIPanel GetPanel(Globals.UIPanel requiredPanel)
    {
        foreach (UIPanel panel in UIPanels)
        {
            if (panel.UIPanelType == requiredPanel)
                return panel;

        }
        Debug.LogError(requiredPanel + " Not Found!!!");
        return null;
    }

    void SetPanel(Globals.UIPanel requiredPanel , bool flag)
    {
        foreach (UIPanel panel in UIPanels)
        {
            if (panel.UIPanelType == requiredPanel)
            {
                panel.gameObject.SetActive(flag);
                return;
            }

        }

        Debug.LogError(requiredPanel + " Not Found!!!");
    }


    public void ShowExperienceModeWebview()
    {
        //Apply Webview URL Token
        SampleWebViewRef.Url = Globals.ShoppingModeURL + "?token=" + Globals.WebViewAccessToken;

        //Show Webview UI Panel
        //ShowUIPanel(Globals.UIPanel.webView);

        //Show Webview
        SampleWebViewRef.ShowWebView();

    }


    public void ShowEditUserInfoWebview() {

        //Apply Webview URL Token
        SampleWebViewRef.Url = Globals.UserInfoURL + "?token=" + Globals.WebViewAccessToken;

        //Show Webview UI Panel
        //ShowUIPanel(Globals.UIPanel.webView);

        //Show Webview
        SampleWebViewRef.ShowWebView();
    }



    #region Callbacks
    public void OnTokenReceived(object sender, Firebase.Messaging.TokenReceivedEventArgs token)
    {
        //Application.Quit();
        //Debug.LogError("Received Registration Token: " + token.Token);
    }

    public void OnMessageReceived(object sender, Firebase.Messaging.MessageReceivedEventArgs e){


        //Try To get the GameTyoe between QUIZ | PUZZLE
        string gameType = "Nothing!!";
        if (e.Message.Data.TryGetValue("gameType", out gameType))
        {

            //If it is the Puzzle Game
            if (gameType.ToLower() == Globals.MiniGame.puzzle.ToString())
            {

                //Grab Puzzle Image
                e.Message.Data.TryGetValue("gameType", out Globals.PuzzleImageURL);


                //Grab Puzzle Game Duration which is in minutes
                string duration;
                e.Message.Data.TryGetValue("duration", out duration);
                Globals.PuzzleGameDeadlineTime = (int)Time.time + int.Parse(duration);


                //Grab the Puzzle Numbers that are to be spawned
                string numbers;
                e.Message.Data.TryGetValue("numbers", out numbers);

                Globals.PuzzlePiecesToSpawnNumbers = numbers.Split(',');

                Globals.PuzzlePiecesRemainingToCollect = Globals.PuzzlePiecesToSpawnNumbers.Length;


                //Set Puzzle game active as True
                Globals.PuzzleGameActive = true;


                //Call GetPuzzle API only if we are in the game scene or In the Main menu
                if (SceneManager.GetActiveScene().name == Globals.SceneNames.GameScene.ToString() || SceneManager.GetActiveScene().name == Globals.SceneNames.MainMenu.ToString())
                {
                    Debug.LogError("OnPuzzleGameInitialized Broadcasted!!");
                    Messenger.Broadcast(Events.OnPuzzleGameInitialized);
                }


            }//If it is the Quiz Game
            else if (gameType.ToLower() == Globals.MiniGame.quiz.ToString())
            {
                Globals.quizGameObj = null;
                Globals.quizGameObj = new QuizGame();
                
                //Grab Quiz Question
                e.Message.Data.TryGetValue("question", out Globals.quizGameObj.Question);

                //Grab Quiz options
                string options;
                e.Message.Data.TryGetValue("options", out options);
                Globals.quizGameObj.Options = options.Split(',');

                //Grab Quiz Answer
                e.Message.Data.TryGetValue("answer", out Globals.quizGameObj.Answer);

                //Grab Quiz Coins
                e.Message.Data.TryGetValue("coins", out Globals.quizGameObj.Coins);


                Globals.QuizGameActive = true;

                if (SceneManager.GetActiveScene().name == Globals.SceneNames.GameScene.ToString() || SceneManager.GetActiveScene().name == Globals.SceneNames.MainMenu.ToString())
                {
                    Debug.LogError("OnPuzzleGameInitialized Broadcasted!!");
                    Messenger.Broadcast(Events.OnQuizGameInitialized);
                }
            }
        }
    }


    //Landing Page
    void OnLoginTried() {
        ShowUIPanel(Globals.UIPanel.loading);
        //SetPanel(Globals.UIPanel.loading, true);
        //TogglePanel(Globals.UIPanel.Loading, true);
    }
    void OnSignupSuccessful()
    {
        // StartCoroutine(WaitForNumberOfSeconds(3));
        //Hide SignUp Panel
        //SetPanel(Globals.UIPanel.signUp, false);
        //ShowUIPanel(Globals.UIPanel.signIn);
        EmacitySceneManager.LoadScene(Globals.SceneNames.MainMenu);

        //Show Login Panel
        //SetPanel(Globals.UIPanel.signIn, true);
    }

    void OnLoginFailed(string message){
        ShowUIPanel(Globals.UIPanel.signIn);
        //SetPanel(Globals.UIPanel.loading, false);
        //TogglePanel(Globals.UIPanel.Loading, false);
    }

    void OnLoginSuccessful(LoginResponseRootObject responseObj)
    {
        //SetPanel(Globals.UIPanel.loading, false);
        //TogglePanel(Globals.UIPanel.Loading, false);

        Globals.DebugMessages.Add("Appmanager OnLoginSuccessful  responseObj.success" + responseObj.Success);
        Globals.DebugMessages.Add("Appmanager OnLoginSuccessful responseObj.Message.Id" + responseObj.Message.Id);
        Globals.DebugMessages.Add("Appmanager OnLoginSuccessful responseObj.Message.Token" + responseObj.Message.Token);
        //Update Current UserID
        Globals.myUserID = responseObj.Message.Id;
        Globals.WebViewAccessToken = responseObj.Message.Token;

        EmacitySceneManager.LoadScene(Globals.SceneNames.MainMenu);
    }

    

    //On Successfully User Profile Fetched
    void OnGetUserProfile(GetUserProfileRequestRootObject obj)
    {

        Messenger.Broadcast<PopupManager.Type>(Events.OnPopupEnd, PopupManager.Type.loading);

        ShowUIPanel(Globals.UIPanel.ModeSelection);

        //Apply Webview URL Token
        SampleWebViewRef.Url =   Globals.ShoppingModeURL + "login?token=" + Globals.WebViewAccessToken;
        
        Messenger.RemoveListener<GetUserProfileRequestRootObject>(Events.OnGetUserProfile, OnGetUserProfile);

        Messenger.Broadcast(Events.OnMainMenuLoadedCompletely);
    }


    public void OnGameSoundToggled(bool flag)
    {
        Globals.GameSoundStatus = flag;

        AudioListener.volume = Globals.GameSoundStatus ? 1 : 0;
        //if (flag)
        //    AudioListener.volume = 1;
        //else
        //    AudioListener.volume = 0;
    }

    void OnAllVendorsAndAdvertisementUnitsLoaded() {

        //No need to Call Start Game API for Guest users
        if (!Globals.IsGuestLogin)
        {
            if (!string.IsNullOrEmpty(Globals.myUserID)){
                //Subscribe To StartGameAPI Response Event
                Messenger.AddListener<string>(Events.OnRESTAPICallback, OnStartGameResponseRecieved);

                //Call Start Game API
                RestAPICallManager.CallRESTAPI(BestHTTP.HTTPMethods.Get, Globals.StartGameRestAPIURL + Globals.myUserID);
            }
        }
        else
        {
            OnStartGameResponseRecieved(null);
        }

        //Now we have all vendors we dont need to listen to On All Vendors Loaded
        Messenger.RemoveListener(Events.OnAllVendorsAndAdvertisementUnitsLoaded, OnAllVendorsAndAdvertisementUnitsLoaded);

    }

    void OnStartGameResponseRecieved(string response)
    {
        if (!string.IsNullOrEmpty(response))
        {
            try
            {
                RESTAPIExceptionObject responseObj = JsonUtility.FromJson<RESTAPIExceptionObject>(response);

                //Analyze Response
                if (responseObj.Success)
                {
                    //Save the Start Game ID which will be used while calling End Game API
                    Globals.StartGameID = responseObj.Message;
                    //Debug.LogError("Start Game API success for user" + responseObj.Message);
                }
                else
                {
                    //Debug.LogError("Start Game API fail for user" + responseObj.Message);
                    Messenger.Broadcast<string, PopupManager.Type>(Events.OnPopupRequired, "Start Game API issue = " + responseObj.Message, PopupManager.Type.notification);
                }
            }
            catch (System.Exception e)
            {
                //Debug.LogError("Start Game API fail for " + e.ToString());
                Messenger.Broadcast<string, PopupManager.Type>(Events.OnPopupRequired, "Start Game API issue = " + e.ToString(), PopupManager.Type.notification);
            }


            //UnSubscribe From StartGameAPI Response Event
            Messenger.RemoveListener<string>(Events.OnRESTAPICallback, OnStartGameResponseRecieved);
        }

        //Loading Complete The Game play can be started
        Messenger.Broadcast<PopupManager.Type>(Events.OnPopupEnd, PopupManager.Type.loading);

        //Broadcast Game Scene Loaded Completely
        Messenger.Broadcast(Events.OnGameSceneLoadedCompletely);

        
        //Show HUD and Let the user Control The Gameplay
        ShowUIPanel(Globals.UIPanel.HUD);
    }

    void OnEndGameResponseRecieved(string response)
    {

        try
        {
            RESTAPIExceptionObject responseObj = JsonUtility.FromJson<RESTAPIExceptionObject>(response);

            //Analyze Response
            if (responseObj.Success)
            {
                //Debug.LogError("End Game API success for user" + responseObj.Message);
                StartCoroutine(ExitGame());
            }
            else
            {
                //Debug.LogError("End  Game API fail for user" + responseObj.Message);
                Messenger.Broadcast<string, PopupManager.Type>(Events.OnPopupRequired, "Something Wrong. With End Game API!", PopupManager.Type.notification);
            }
        }
        catch (System.Exception e)
        {
            //Debug.LogError("End Game API fail for user" + e.ToString());
            Messenger.Broadcast<string, PopupManager.Type>(Events.OnPopupRequired, "Something Wrong. With End Game API!", PopupManager.Type.notification);
        }
        //UnSubscribe From EndGameAPI Response Event
        Messenger.RemoveListener<string>(Events.OnRESTAPICallback, OnEndGameResponseRecieved);
    }





    IEnumerator ExitGame()
    {
        while (!Globals.CanExitGameplay)
            yield return null;


        //All Sorted Now Go safely To Main Menu
        SceneManager.LoadScene(MultiplayerSettings.multiplayerSettings.menuScene);

        StopCoroutine(ExitGame());
    }



    public void OnLogOut()
    {
        //Reset All Params To Defaults
        Globals.ResetToDefaults();

        //Destroy Photon Room
        Destroy(PhotonRoom.room.gameObject);
        //Go To Login Scene
        EmacitySceneManager.LoadScene((int)Globals.SceneNames.Login);
    }


    void OnReadyToExitGame()
    {
        if(!Globals.IsGuestLogin)
            Globals.CanExitGameplay = true;
        else
        {
            //All Sorted Now Go safely To Main Menu
            SceneManager.LoadScene(MultiplayerSettings.multiplayerSettings.menuScene);
        }
    }

    public void OnEndGameSelected()
    {
        if (!Globals.IsGuestLogin)
        {
            //Subscribe To EndGameAPI Response Event
            Messenger.AddListener<string>(Events.OnRESTAPICallback, OnEndGameResponseRecieved);

            //Call Start Game API
            RestAPICallManager.CallRESTAPI(BestHTTP.HTTPMethods.Get, Globals.EndGameRestAPIURL + Globals.StartGameID);
        }
    }

    public void OnShare()
    {

        new NativeShare().SetText("https://emacityapp.com/").Share();
        //Add Native Share Code
        //StartCoroutine(TakeSSAndShare());
    }

    //private IEnumerator TakeSSAndShare()
    //{
    //    yield return new WaitForEndOfFrame();

    //    Texture2D ss = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
    //    ss.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
    //    ss.Apply();

    //    string filePath = Path.Combine(Application.temporaryCachePath, "shared img.png");
    //    File.WriteAllBytes(filePath, ss.EncodeToPNG());

    //    // To avoid memory leaks
    //    Destroy(ss);
        
    //    new NativeShare().AddFile(filePath).SetSubject("Subject goes here").SetText("Hello world!").Share();

    //    // Share on WhatsApp only, if installed (Android only)
    //    //if( NativeShare.TargetExists( "com.whatsapp" ) )
    //    //	new NativeShare().AddFile( filePath ).SetText( "Hello world!" ).SetTarget( "com.whatsapp" ).Share();
    //}
    #endregion



    #region Messenger Callbacks



    void OnQuizGameRewardReceived(int rewardCoins)
    {
        //Update Globals Parameter
        Globals.TotalCoins += rewardCoins;
        
    }

    public void OnGuestAccountLogin() {
        Globals.IsGuestLogin = true;
        EmacitySceneManager.LoadScene(Globals.SceneNames.MainMenu);
    }

    //Popup
    void OnPopupYesSelected()
    {
        Application.Quit();
        Messenger.RemoveListener(Events.OnPopupYesSelected , OnPopupYesSelected);
    }

    void OnPopupRequired(string message, PopupManager.Type type)
    {
        popupManagerRef.gameObject.SetActive(true);
        popupManagerRef.Initialize(message, type);
    }

    void OnPopupEnd(PopupManager.Type type)
    {
        popupManagerRef.End(type);
        popupManagerRef.gameObject.SetActive(true);
    }


    //Main Menu
    void OnStoreSelected()
    {
        ShowUIPanel(Globals.UIPanel.avatarSelection);
    }

    public void OpenPreviousWebViewURL()
    {

#if UNITY_EDITOR
        Application.OpenURL(Globals.CurrentWebViewURL);
#else
        //Show Webview UI
        ShowUIPanel(Globals.UIPanel.webView);

        //Show Web view with The Specified URL
        SampleWebViewRef.ShowWebView(Globals.CurrentWebViewURL);
#endif
    }



    //Shop
    void OnShopInteracted(string url , string vendorID)
    {
        //Save WebView URL 
        Globals.CurrentWebViewURL = url;

        //Show Webview UI Panel
        ShowUIPanel(Globals.UIPanel.webView);

        Debug.LogError("-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_OnShopInteracted");

        if (!Globals.IsGuestLogin && !string.IsNullOrEmpty(vendorID))
        {
            //New AddUserToVendorRequestRootObject Obj
            AddUserToVendorRequestRootObject addUserToVendorObj = new AddUserToVendorRequestRootObject();

            //Fill in Data
            addUserToVendorObj.VendorId = vendorID;
            addUserToVendorObj.UserId = Globals.myUserID;

            string rawData = JsonUtility.ToJson(addUserToVendorObj);

            //Subscribe To OnAddUserToVendor Response Event
            Messenger.AddListener<string>(Events.OnRESTAPICallback, OnAddUserToVendorCallback);

            //Call the API , we also attach the UserID to the Vendor URL as Implemented by the Fullstack
            //string finalURL = Globals.AddUserToVendor + "?UserId=" + Globals.myUserID;
            //Debug.LogError("-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_Adding user to vendor URL = " + finalURL);
            RestAPICallManager.CallRESTAPI(BestHTTP.HTTPMethods.Post, Globals.AddUserToVendor, rawData);

            //Show Loading Vendor Webpage POPUP
            Messenger.Broadcast<string, PopupManager.Type>(Events.OnPopupRequired, "Loading Vendor, Please Wait!", PopupManager.Type.loading);
        }
        else
        {
            Debug.LogError("-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_Vendor ID is null or Empty | Guest account");
        }
    }

    void OnAdvertisementInteracted()
    {

#if UNITY_EDITOR
        Application.OpenURL(Globals.AlphaMallContactUsURL);
#else
        //Show Webview UI
        ShowUIPanel(Globals.UIPanel.webView);

        //Show Web view with The Specified URL
        SampleWebViewRef.ShowWebView(Globals.AlphaMallContactUsURL);
#endif

    }

    //AddUserToVendor
    void OnAddUserToVendorCallback(string response)
    {
        try
        {
            RESTAPIExceptionObject RESTAPIExceptionObjectRef = new RESTAPIExceptionObject();

            RESTAPIExceptionObjectRef = JsonUtility.FromJson<RESTAPIExceptionObject>(response);

            if (RESTAPIExceptionObjectRef.Success)
            {
                //User Added To vendor Successfully
                Debug.LogError("User Added To vendor Successfully");

                //this ID will be used While calling Remove User From Vendor API
                Globals.ActiveVendorID = RESTAPIExceptionObjectRef.Message;

#if UNITY_EDITOR
                //Emulate Webview by opening Browser in Editor
                Application.OpenURL(Globals.CurrentWebViewURL + "?userid=" + Globals.myUserID);
#else

                //Show Web view with The Specified URL
                SampleWebViewRef.ShowWebView(Globals.CurrentWebViewURL + "?userid=" + Globals.myUserID);
#endif


                Messenger.Broadcast<PopupManager.Type>(Events.OnPopupEnd, PopupManager.Type.loading);
            }
            else
            {
                //User Not Added To vendor 
                Debug.LogError("User Not Added To vendor" + RESTAPIExceptionObjectRef.Message);
            }

        }
        catch (System.Exception e)
        {
            Debug.LogError("Error with OnAddUserToVendorCallback" + e.ToString());
        }

        //Unsubscribe From RESTapi callback since we have the needful now
        Messenger.RemoveListener<string>(Events.OnRESTAPICallback, OnAddUserToVendorCallback);
    }



    public void RemoveUserFromVendor()
    {
        //New RemoveUserFromVendorRequestRootObject Obj
        RemoveUserFromVendorRequestRootObject removeUserFromVendorObj = new RemoveUserFromVendorRequestRootObject();

        //Fill in ActiveVendorID which we got from AddUserToVendor API
        removeUserFromVendorObj.ID = Globals.ActiveVendorID;
        
        string rawData = JsonUtility.ToJson(removeUserFromVendorObj);

        //Subscribe To OnAddUserToVendor Response Event
        Messenger.AddListener<string>(Events.OnRESTAPICallback, OnRemoveUserFromVendorCallback);

        //Call the API
        RestAPICallManager.CallRESTAPI(BestHTTP.HTTPMethods.Put, Globals.RemoveUserFromVendor, rawData);
    }

    void OnRemoveUserFromVendorCallback(string response)
    {
        try
        {
            RESTAPIExceptionObject RESTAPIExceptionObjectRef = new RESTAPIExceptionObject();

            RESTAPIExceptionObjectRef = JsonUtility.FromJson<RESTAPIExceptionObject>(response);

            if (RESTAPIExceptionObjectRef.Success)
            {
                //User Removed from vendor Successfully
                Debug.Log("User Removed from vendor Successfully");
            }
            else
            {
                //User Not removed From vendor 
                Debug.LogError("User Not Removed from vendor" + RESTAPIExceptionObjectRef.Message);
            }
        }
        catch (System.Exception e)
        {
            Debug.LogError(e.ToString());
        }

        //Unsubscribe From RESTapi callback since we have the needful now
        Messenger.RemoveListener<string>(Events.OnRESTAPICallback, OnRemoveUserFromVendorCallback);

    }

    //Puzzle
    void OnPuzzlePieceCollected(PuzzleUnit puzzleUnit)
    {
        //Reduce the Remaning Puzzle pices To be collected
        Globals.PuzzlePiecesRemainingToCollect--;

        //If all Pizzle Pieces Collected
        if(Globals.PuzzlePiecesRemainingToCollect == 0){
            Messenger.Broadcast(Events.OnAllPuzzlePiecesCollected);
            
            //Call get Puzzle Game Prize product
            //Add this product to the cart at zero price
        }
    }


#endregion



#region Utility
    public void DestroyWebView()
    {

        //if (SampleWebViewRef.webViewObject != null)
        //{
        //Debug.LogError("Going to destroy = " + SampleWebViewRef.webViewObject.gameObject.name);
        try
        {
            //SampleWebViewRef.webViewObject.gameObject.SetActive(false);
            DestroyImmediate(SampleWebViewRef.webViewObject.gameObject);
        }
        catch (System.Exception e) {
            Debug.LogError("Wops" + e.ToString());
        }
        //}
    }
#endregion
    private void Update()
    {
        //Debug.LogError("Start Time" + (int)Time.time);
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            Messenger.AddListener(Events.OnPopupYesSelected , OnPopupYesSelected);
            OnPopupRequired("Do you want to quit ?", PopupManager.Type.yesNo);
        }
    }
}
