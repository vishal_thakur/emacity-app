﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SignUpManager : MonoBehaviour
{
    //[SerializeField]
    //RestAPICallManager RestAPICallManagerRef;

    //UserName and Password
    [SerializeField] InputField userNameTextRef;
    [SerializeField] GameObject userNameErrorGraphicRef;


    [Space(10) , SerializeField] InputField passwordTextRef;
    [SerializeField] GameObject passwordErrorGraphicRef;

    ////Gender And Country
    //[SerializeField] TMPro.TMP_Dropdown GenderDropDownRef;
    //[SerializeField] TMPro.TMP_Dropdown CountryDropDownRef;


    //Gender And Country
    [SerializeField] Dropdown GenderDropDownRef;
    [SerializeField] Dropdown CountryDropDownRef;

    [SerializeField] Text ResponseTextRef;

    //[SerializeField] Toggle acceptTermsAndConditionsToggleRef;

    [SerializeField]
    SignupRequestRootObject SignupRequestRootObjectInstance;

    [SerializeField]
    GetSignupCountriesResponseRootObject GetSignupCountriesResponseRootObjectInstance;

    [SerializeField]
    SignupResponseRootObject SignupResponseRootObjectInstance;

    [SerializeField]
    RESTAPIExceptionObject RESTAPIExceptionObjectIntance;


    IEnumerator Start()
    {


        yield return new WaitForEndOfFrame();


        //Show Loading Untill The Stores Info is downloaded
        Messenger.Broadcast<string, PopupManager.Type>(Events.OnPopupRequired, "Loading Countries...", PopupManager.Type.loading);

        if (Globals.countryDropdownOptions.Count == 0)
        {
            //Fetch All Available Signup Countries
            Messenger.AddListener<string>(Events.OnRESTAPICallback, OnSignupCountriesAPICallback);

            RestAPICallManager.CallRESTAPI(BestHTTP.HTTPMethods.Get, Globals.GetAllSignupCountriesURL);
        }
        else
        {
            StartCoroutine(LoadCountriesFromSavedData());
        }

    }
    

    void OnSignupCountriesAPICallback(string responseText)
    {
        try
        {
            GetSignupCountriesResponseRootObjectInstance = JsonUtility.FromJson<GetSignupCountriesResponseRootObject>(responseText);

            //Success
            if (GetSignupCountriesResponseRootObjectInstance.Success)
            {
                StartCoroutine(LoadCountriesInUI());

                //Messenger.Broadcast<PopupManager.Type>(Events.OnPopupEnd, PopupManager.Type.loading);

            }
            else//Failed
            {
                //Messenger.Broadcast<PopupManager.Type>(Events.OnPopupEnd, PopupManager.Type.loading);

                Debug.LogError("Unable To fetch Countries!");
                Messenger.Broadcast<string, PopupManager.Type>(Events.OnPopupRequired, "Unable to fetch Countries!", PopupManager.Type.notification);
            }
        }
        catch (System.Exception e)
        {
            //Messenger.Broadcast<PopupManager.Type>(Events.OnPopupEnd, PopupManager.Type.loading);
            Debug.LogError(e);
            Messenger.Broadcast<string, PopupManager.Type>(Events.OnPopupRequired, "Fetching Countries Failed", PopupManager.Type.notification);
        }

        //Unsubscribe From OnSignupCountriesAPICallback Event 
        Messenger.RemoveListener<string>(Events.OnRESTAPICallback, OnSignupCountriesAPICallback);
    }



    IEnumerator LoadCountriesInUI()
    {
        
        foreach (GetSignupCountriesResponseMessage countryObj in GetSignupCountriesResponseRootObjectInstance.Message)
        {
            yield return new WaitForEndOfFrame();
            Dropdown.OptionData newOptionData = new Dropdown.OptionData();
            newOptionData.text = countryObj.Name;
            CountryDropDownRef.options.Add(newOptionData);

            //Save Country Dropdown option To globals params
            Globals.countryDropdownOptions.Add(newOptionData);
        }


        Messenger.Broadcast<PopupManager.Type>(Events.OnPopupEnd, PopupManager.Type.loading);

        StopCoroutine(LoadCountriesInUI());
    }

    IEnumerator LoadCountriesFromSavedData()
    {
        foreach (Dropdown.OptionData  countryOption in Globals.countryDropdownOptions)
        {
            yield return new WaitForEndOfFrame();
            CountryDropDownRef.options.Add(countryOption);
        }
        
        Messenger.Broadcast<PopupManager.Type>(Events.OnPopupEnd, PopupManager.Type.loading);

        StopCoroutine(LoadCountriesFromSavedData());
    }


    void OnSignupResponseRecieved(string response) {

        try
        {
            SignupResponseRootObjectInstance = JsonUtility.FromJson<SignupResponseRootObject>(response);
        }
        catch (System.Exception e){
            SignupResponseRootObjectInstance.Success = false;
        }

        //Hide Loading panel
       // Messenger.Broadcast<bool>(Events.OnLoadingPanelToggle, false);

        //If Signup Successful
        if (SignupResponseRootObjectInstance.Success){

            //Save userID to be used for API CAlls to the backend
            Globals.myUserID = SignupResponseRootObjectInstance.Message.Id;
            Globals.myEmail = SignupResponseRootObjectInstance.Message.Email;
            Globals.myCountry = SignupResponseRootObjectInstance.Message.CountryId;
            //Intuit User
            ResponseTextRef.color = Color.green;
            ResponseTextRef.text = "SignUp Successfull";
            
            Messenger.Broadcast(Events.OnSignupSuccessful);
        }
        //If Signup Not Successful
        else
        {

            //Show Erro Borders On Email and Password as Sign UP Failed
            userNameTextRef.transform.GetChild(4).gameObject.SetActive(true);
            passwordTextRef.transform.GetChild(4).gameObject.SetActive(true);

            RESTAPIExceptionObjectIntance = JsonUtility.FromJson<RESTAPIExceptionObject>(response);

            Messenger.Broadcast<string, PopupManager.Type>(Events.OnPopupRequired , RESTAPIExceptionObjectIntance.Message , PopupManager.Type.notification);
            
            Messenger.Broadcast<string>(Events.OnSignupFailed , RESTAPIExceptionObjectIntance.Message);
        }

        //UnSubscribe To Login Response Event
        Messenger.RemoveListener<string>(Events.OnRESTAPICallback, OnSignupResponseRecieved);
    }



    public void Signup()
    {

        bool allGood = true;

        //If information not filled up correctly then donot Signup and show respective Popup
        if (!Globals.VerifyEmailAddress(userNameTextRef.text))
        {
            userNameErrorGraphicRef.SetActive(true);
            //userNameTextRef.transform.GetChild(4).gameObject.SetActive(true);
            allGood = false;
        }
        else
            userNameErrorGraphicRef.SetActive(false);
        //userNameTextRef.transform.GetChild(4).gameObject.SetActive(false);

        //If information not filled up correctly then donot Signup and show respective Popup
        if (string.IsNullOrEmpty(passwordTextRef.text))
        {
            passwordErrorGraphicRef.SetActive(true);
            //passwordTextRef.transform.GetChild(4).gameObject.SetActive(true);
            allGood = false;
        }
        else
            passwordErrorGraphicRef.SetActive(false);
        //passwordTextRef.transform.GetChild(4).gameObject.SetActive(false);

        //Something was wrong , Either Mail or password
        if (!allGood)
            return;

        //Subscribe To Signup Response Event
        Messenger.AddListener<string>(Events.OnRESTAPICallback, OnSignupResponseRecieved);
        

        //New LoginData Obj
        SignupRequestRootObject signUpData = new SignupRequestRootObject();

        //Fill in Data
        signUpData.Email = userNameTextRef.text;
        signUpData.Password = passwordTextRef.text;

        //Gender 
        if (GenderDropDownRef.value == (int)Globals.CharacterSex.M)
            signUpData.Gender = "M";
        else if(GenderDropDownRef.value == (int)Globals.CharacterSex.M)
            signUpData.Gender = "F";
        else
            signUpData.Gender = "M";

        //Country
        //signUpData.CountryId = ((Globals.Country) CountryDropDownRef.value).ToString();
        signUpData.CountryId = Globals.myCountry;

        //string rawData = JsonUtility.ToJson(loginData, true);
        //string rawData = RawDataEditor;
        string rawData = JsonUtility.ToJson(signUpData);


        //Debug.LogError("Raw data:" + rawData);

        //RestAPICallManagerRef.CallRESTAPI(BestHTTP.HTTPMethods.Post, Globals.LoginRESTAPIURL, rawData);
        RestAPICallManager.CallRESTAPI(BestHTTP.HTTPMethods.Post, Globals.SignUpRESTAPIURL, rawData);
        //Messenger.Broadcast<BestHTTP.HTTPMethods, string, string>(Events.OnCallRESTAPIType1 , BestHTTP.HTTPMethods.Post, Globals.SignUpRESTAPIURL, rawData);
    }


    


}
