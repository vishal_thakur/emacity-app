﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoginManager : MonoBehaviour
{
    //[SerializeField]
    //RestAPICallManager RestAPICallManagerRef;

    //Username  params
    [SerializeField] InputField userNameTextRef;
    [SerializeField] GameObject userNameErrorGraphicRef;


    [Space(10) , SerializeField] InputField passwordTextRef;
    [SerializeField] GameObject passwordErrorGraphicRef;

    [SerializeField] GameObject forgotPasswordErrorGraphicRef;

    [SerializeField] Toggle rememberMeToggleRef;

    [SerializeField] Text ResponseTextRef;


    [SerializeField]
    LoginRequestRootObject LoginRequestRootObject;

    //[SerializeField]
    //LoginResponseRootObject responseObject;

   // [SerializeField]
    //RESTAPIExceptionObject RESTAPIExceptionObjectInstance;


    private void Start()
    {

        Messenger.AddListener(Events.OnSocialLoginSuccessful, OnSocialLoginSuccessful);


        //Get Saved/last Email
        if (PlayerPrefs.HasKey("email"))
        {
            userNameTextRef.text = PlayerPrefs.GetString("email");
            rememberMeToggleRef.SetIsOnWithoutNotify(true);
        }

        //Get Saved/last Password
        if (PlayerPrefs.HasKey("password"))
        {
            passwordTextRef.text = PlayerPrefs.GetString("password");
            rememberMeToggleRef.SetIsOnWithoutNotify(true);
        }

    }


    private void OnDestroy()
    {
        Messenger.RemoveListener(Events.OnSocialLoginSuccessful, OnSocialLoginSuccessful);
    }

    void OnSocialLoginSuccessful()
    {
        //Call Rest API with the respective social login mode's Access Token

        Debug.LogError("Social Access Token = " + Globals.FirebaseSocialLoginIDToken);
        

        //Create a new SocialLogin Root Obj
        SocialLoginRequestRootObject socialLoginRequestRootObject = new SocialLoginRequestRootObject();

        //Set the TOKEN
        socialLoginRequestRootObject.Token = Globals.FirebaseSocialLoginIDToken;
        
        string rawData = JsonUtility.ToJson(socialLoginRequestRootObject);

        Messenger.Broadcast<string, PopupManager.Type>(Events.OnPopupRequired, "Authenticating Social Login..", PopupManager.Type.loading);

        //Subscribe To Social Login Token Response Event
        Messenger.AddListener<string>(Events.OnRESTAPICallback, OnSocialLoginTokenResponseRecieved);

        //Call the API
        RestAPICallManager.CallRESTAPI(BestHTTP.HTTPMethods.Post, Globals.SocialSignInRESTAPIURL, rawData);

        Globals.DebugMessages.Clear();


        Globals.DebugMessages.Add("Called Social API");
        Globals.DebugMessages.Add("rawdata = " + rawData);
    }

    void OnSocialLoginTokenResponseRecieved(string responseText)
    {
        Globals.DebugMessages.Clear();
        Debug.LogError("Social API callback response =   " + responseText);
        Globals.DebugMessages.Add("Social API callback response =   " + responseText);

        LoginResponseRootObject responseObject = new LoginResponseRootObject();


        try
        {

            responseObject = JsonUtility.FromJson<LoginResponseRootObject>(responseText);


            Globals.DebugMessages.Add("Success = " + responseObject.Success);

            Globals.DebugMessages.Add("id = " + responseObject.Message.Id);
            Globals.DebugMessages.Add("email = " + responseObject.Message.Email);
            Globals.DebugMessages.Add("cntryID = " + responseObject.Message.CountryId);
            Globals.DebugMessages.Add("gender = " + responseObject.Message.Gender);
            Globals.DebugMessages.Add("Token = " + responseObject.Message.Token);

            if (!responseObject.Success)
            {
                Globals.DebugMessages.Add("Social API Failed");
                Globals.DebugMessages.Add("Fail Message = " + responseObject.Message);
                //tell user Social Login Token API Failed
                Messenger.Broadcast<string, PopupManager.Type>(Events.OnPopupRequired, "Social Login failed, Reason = " + responseObject.Message.ToString() , PopupManager.Type.notification);
            }
            else
            {

                Globals.DebugMessages.Add("Social API Success");
                Globals.DebugMessages.Add("Success Message = " + responseObject.Message);

                //Call These after succssful SOCIAL TOKEN API CALLBACK
                Globals.loggedInSocial = true;

                //Intuit App Manager About Login Success
                Messenger.Broadcast<LoginResponseRootObject>(Events.OnLoginSuccessful, responseObject);
            }
        }
        catch (System.Exception e)
        {

            Globals.DebugMessages.Add("Social API Exception");
            Globals.DebugMessages.Add("Exception Message = " + e);
            Messenger.Broadcast<string>(Events.OnLoginFailed, e.Message);
            //Messenger.Broadcast<string, PopupManager.Type>(Events.OnPopupRequired, "Social Login failed. Try again !!", PopupManager.Type.notification);
        }

        //remove Loading Popuup
        Messenger.Broadcast<PopupManager.Type>(Events.OnPopupEnd, PopupManager.Type.loading);

        Messenger.RemoveListener<string>(Events.OnRESTAPICallback, OnSocialLoginTokenResponseRecieved);
    }



    void OnForgotPasswordResponseRecieved(string responseText)
    {
        try
        {
            RESTAPIExceptionObject RESTAPIExceptionObjectInstance = JsonUtility.FromJson<RESTAPIExceptionObject>(responseText);

            if (RESTAPIExceptionObjectInstance.Success)
            {
                forgotPasswordErrorGraphicRef.SetActive(false);
            }
            else
            {
                forgotPasswordErrorGraphicRef.SetActive(true);
            }

            Messenger.Broadcast<string, PopupManager.Type>(Events.OnPopupRequired, RESTAPIExceptionObjectInstance.Message , PopupManager.Type.notification);
        }
        catch (System.Exception e){
            Messenger.Broadcast<string, PopupManager.Type>(Events.OnPopupRequired, "Something Wrong. Please try again!" , PopupManager.Type.notification);
        }
    }

    void OnLoginResponseRecieved(string responseText) {
        LoginResponseRootObject responseObject = new LoginResponseRootObject();

        try
        {
            responseObject = JsonUtility.FromJson<LoginResponseRootObject>(responseText);
        }
        catch (System.Exception e){
            responseObject.Success = false;
            Messenger.Broadcast<string>(Events.OnLoginFailed, responseText);
        }

        //Hide Loading panel
       // Messenger.Broadcast<bool>(Events.OnLoadingPanelToggle, false);

        //-------------If Login Successful----------------
        if (responseObject.Success){
            //Intuit User
            ResponseTextRef.color = Color.green;
            ResponseTextRef.text = "Login Successfull";
            
            //Intuit App Manager About Login Success
            Messenger.Broadcast<LoginResponseRootObject>(Events.OnLoginSuccessful , responseObject);
        }
        //----------------If Login Not Successful----------------
        else{
            try
            {
                RESTAPIExceptionObject RESTAPIExceptionObjectInstance = JsonUtility.FromJson<RESTAPIExceptionObject>(responseText);

                userNameTextRef.transform.GetChild(4).gameObject.SetActive(true);
                passwordTextRef.transform.GetChild(4).gameObject.SetActive(true);


                Messenger.Broadcast<string, PopupManager.Type>(Events.OnPopupRequired, RESTAPIExceptionObjectInstance.Message , PopupManager.Type.notification);

                //Intuit App Manager About Login Failed
                Messenger.Broadcast<string>(Events.OnLoginFailed, responseText);
            }
            catch(System.Exception e)
            {
                Messenger.Broadcast<string, PopupManager.Type>(Events.OnPopupRequired, "Login failed. Try again !!" , PopupManager.Type.notification);

                //Intuit App Manager About Login Failed
                Messenger.Broadcast<string>(Events.OnLoginFailed, responseText);
            }
        }

        //UnSubscribe To Login Response Event
        Messenger.RemoveListener<string>(Events.OnRESTAPICallback, OnLoginResponseRecieved);
    }



    public void Login() {

        bool allGood = true;

        //If information not filled up correctly then donot Signup and show respective Popup
        if (!Globals.VerifyEmailAddress(userNameTextRef.text))
        {
            userNameErrorGraphicRef.SetActive(true);
            //userNameTextRef.transform.GetChild(4).gameObject.SetActive(true);
            allGood = false;
        }
        else
            userNameErrorGraphicRef.SetActive(false);
        //userNameTextRef.transform.GetChild(4).gameObject.SetActive(false);

        //If information not filled up correctly then donot Signup and show respective Popup
        if (string.IsNullOrEmpty(passwordTextRef.text))
        {
            passwordErrorGraphicRef.SetActive(true);
            //passwordTextRef.transform.GetChild(4).gameObject.SetActive(true);
            allGood = false;
        }
        else
            passwordErrorGraphicRef.SetActive(false);
        //passwordTextRef.transform.GetChild(4).gameObject.SetActive(false);

        //Something was wrong , Either Mail or password
        if (!allGood)
            return;


        //Subscribe To Login Response Event
        Messenger.AddListener<string>(Events.OnRESTAPICallback, OnLoginResponseRecieved);

        //New LoginData Obj
        LoginRequestRootObject loginData = new LoginRequestRootObject();

        //Fill in Data
        loginData.Email = userNameTextRef.text;
        loginData.Password = passwordTextRef.text;

        //Save credentials if User selected Remember me 
        if (rememberMeToggleRef.isOn)
        {
            PlayerPrefs.SetString("email", loginData.Email);
            PlayerPrefs.SetString("password", loginData.Password);
        }

        //string rawData = JsonUtility.ToJson(loginData, true);
        //string rawData = RawDataEditor;
        string rawData = JsonUtility.ToJson(loginData);


        //Debug.LogError("Raw data:" + rawData);

        //RestAPICallManagerRef.CallRESTAPI(BestHTTP.HTTPMethods.Post, Globals.LoginRESTAPIURL, rawData);
        RestAPICallManager.CallRESTAPI(BestHTTP.HTTPMethods.Post, Globals.LoginRESTAPIURL, rawData);
        //Messenger.Broadcast<BestHTTP.HTTPMethods, string, string>(Events.OnCallRESTAPIType1 , BestHTTP.HTTPMethods.Post, Globals.LoginRESTAPIURL, rawData);

        Messenger.Broadcast(Events.OnLoginTried);
    }


    public void ResetPassword(InputField emailTextRef)
    {
        //If information not filled up correctly then donot Signup and show respective Popup
        if (!Globals.VerifyEmailAddress(emailTextRef.text)){
            forgotPasswordErrorGraphicRef.SetActive(true);
            return;
        }
        else
            forgotPasswordErrorGraphicRef.SetActive(false);


        //Subscribe To Login Response Event
        Messenger.AddListener<string>(Events.OnRESTAPICallback, OnForgotPasswordResponseRecieved);

        //Call Api
        string forgotPasswordFinalURL = Globals.ForgotPasswordRESTAPIURL + "?email=" + emailTextRef.text + "&langId=5c803523493d605aab551c5a";
        RestAPICallManager.CallRESTAPI(BestHTTP.HTTPMethods.Post, forgotPasswordFinalURL);
    }
}
