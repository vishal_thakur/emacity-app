﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupManager : MonoBehaviour
{
    public enum Type
    {
        notification = 0,
        retry,
        yesNo,
        loading
    }



    [SerializeField]
    Text popupMessageTextRef;
    

    [SerializeField]
    GameObject OkButton;
    [SerializeField]
    GameObject YesButton;
    [SerializeField]
    GameObject RetryButton;
    [SerializeField]
    GameObject CancelButton;

    [Space(10)]
    [SerializeField]
    GameObject LoadingScreenRef;
    [SerializeField]
    GameObject PopupRef;

    //When user Clicks on the Retry button in the popup window
    public void OnRetrySelected()
    {
        Messenger.Broadcast(Events.OnPopupRetrySelected);
    }

    //When user Clicks on the Retry button in the popup window
    public void OnYesSelected()
    {
        Messenger.Broadcast(Events.OnPopupYesSelected);
    }

    //Event Callback Methods
    public void Initialize(string message, Type type)
    {
        //Set the Popup Message
        popupMessageTextRef.gameObject.SetActive(true);
        popupMessageTextRef.text = message;
        PopupRef.SetActive(true);

        switch (type)
        {
            case Type.notification:
                YesButton.SetActive(false);
                OkButton.SetActive(true);
                RetryButton.SetActive(false);
                CancelButton.SetActive(false);
                break;
            case Type.retry:
                YesButton.SetActive(false);
                OkButton.SetActive(false);
                RetryButton.SetActive(true);
                CancelButton.SetActive(false);
                break;
            case Type.yesNo:
                YesButton.SetActive(true);
                OkButton.SetActive(false);
                RetryButton.SetActive(false);
                CancelButton.SetActive(true);
                break;
            case Type.loading:
                PopupRef.SetActive(false);
                LoadingScreenRef.SetActive(true);
                YesButton.SetActive(false);
                OkButton.SetActive(false);
                RetryButton.SetActive(false);
                CancelButton.SetActive(false);
                break;
        }
    }



    public void End(Type type)
    {
        switch (type)
        {
            case Type.loading:
                LoadingScreenRef.SetActive(false);
                popupMessageTextRef.text = null;
                popupMessageTextRef.gameObject.SetActive(false);
                break;
        }
    }


    



}
