﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ArabicSupport;

public class MyProfileManager : MonoBehaviour
{

    [SerializeField] InputField myNameInputFieldRef;
    [SerializeField] Text myEmailTextRef;

    [Space(10)]
    [SerializeField] Text myCoinsTextRef;

    [Space(10)]
    [SerializeField] Text myLevelTextRef;


    [SerializeField]
    bool wasNameEditied = false;

    [SerializeField]
    Toggle SoundToggleRef;

    //[SerializeField]
    //RestAPICallManager RestAPICallManagerRef;


    void Start()
    {
        Messenger.AddListener<GetUserProfileRequestRootObject>(Events.OnGetUserProfile, OnGetUserProfile);
        Messenger.AddListener<int>(Events.OnQuizGameRewardRecieved, OnQuizGameRewardReceived);
        SoundToggleRef.SetIsOnWithoutNotify(Globals.GameSoundStatus);
        SoundToggleRef.GetComponent<SoundButton>().OnToggle(Globals.GameSoundStatus);
    }

    private void OnDestroy()
    {
        Messenger.RemoveListener<int>(Events.OnQuizGameRewardRecieved, OnQuizGameRewardReceived);
    }


    void Initialize(GetUserProfileRequestRootObject obj)
    {
        if (obj == null)
        {
            myNameInputFieldRef.text = Globals.myFirstName;
            myEmailTextRef.text = Globals.myEmail;

            myCoinsTextRef.text = Globals.TotalCoins.ToString();
        }
        else
        {

            myNameInputFieldRef.text = obj.Message.FirstName;
            myEmailTextRef.text = obj.Message.Email;

            myCoinsTextRef.text = obj.Message.TotalCoins.ToString();
            myLevelTextRef.text = "L " + obj.Message.Level.ToString();
        }


        wasNameEditied = false;
    }
    public void OnUserNameEdited()
    {
        wasNameEditied = true;
    }


    void OnQuizGameRewardReceived(int reward)
    {
        StartCoroutine(UpdateUserCoinsUI());
    }


    public void OnExit()
    {
        if (wasNameEditied && !Globals.IsGuestLogin)
        {
            wasNameEditied = false;

            //update User Profile

            //Subscribe To UpdateUserProfile Response Event
            Messenger.AddListener<string>(Events.OnRESTAPICallback, OnUpdateUserProfileResponseRecieved);

            //New UpdateUserProfile Object 
            UpdateUserProfileRequestRootObject updateUserProfileRequestRootObject = new UpdateUserProfileRequestRootObject();

            //Feed in Information as Updated
            updateUserProfileRequestRootObject.UserId = Globals.myUserID;
            updateUserProfileRequestRootObject.Gender = Globals.CurrentCharacterSex.ToString();
            updateUserProfileRequestRootObject.FirstName = Globals.myFirstName = myNameInputFieldRef.text;
            //updateUserProfileRequestRootObject.LastName = "Dummy";
            updateUserProfileRequestRootObject.Email = Globals.myEmail;
            updateUserProfileRequestRootObject.CountryId = Globals.myCountry;
            //updateUserProfileRequestRootObject.Phone = "dummy";
            //updateUserProfileRequestRootObject.Interests = "dummy";

            updateUserProfileRequestRootObject.CharacterType = Globals.CurrentCharacterType.ToString();
            updateUserProfileRequestRootObject.TypeOfClothTopColor = Globals.CurrentCharacterClothTopType.ToString();
            updateUserProfileRequestRootObject.TypeOfClothBottomColor = Globals.CurrentCharacterClothBottomType.ToString();
            updateUserProfileRequestRootObject.TypeOfHairColor = Globals.CurrentCharacterHairType.ToString();
            updateUserProfileRequestRootObject.TypeOfShoeColor = Globals.CurrentCharacterShoeType.ToString();

            //Prepare Raw data
            string rawData = JsonUtility.ToJson(updateUserProfileRequestRootObject);

            //Call the API
            RestAPICallManager.CallRESTAPI(BestHTTP.HTTPMethods.Put, Globals.UpdateUserProfileRESTAPIURL, rawData);


            Messenger.Broadcast<string, PopupManager.Type>(Events.OnPopupRequired, "Saving Changes..", PopupManager.Type.loading);
        }
    }


    //------------Callbacks------------------------------


    //Update User Profile callback
    void OnUpdateUserProfileResponseRecieved(string response)
    {
        //Debug.LogError(response);

        //UpdateUserProfileResponse obj
        UpdateUserProfileResponseRootObject updateUserProfileResponseRootObject = new UpdateUserProfileResponseRootObject();

        //Deserialize The JSON Object
        try
        {
            //----Success----
            updateUserProfileResponseRootObject = JsonUtility.FromJson<UpdateUserProfileResponseRootObject>(response);

            //Success
            if (updateUserProfileResponseRootObject.Success)
            {
                Messenger.Broadcast<PopupManager.Type>(Events.OnPopupEnd, PopupManager.Type.loading);
            }
            else//Fail
            {
                Messenger.Broadcast<PopupManager.Type>(Events.OnPopupEnd, PopupManager.Type.loading);
                Messenger.Broadcast<string, PopupManager.Type>(Events.OnPopupRequired, "User Name Update Failed!", PopupManager.Type.notification);
            }


        }
        catch (System.Exception e)
        {
            //------Fail-----
            updateUserProfileResponseRootObject.Success = false;


            Messenger.Broadcast<PopupManager.Type>(Events.OnPopupEnd, PopupManager.Type.loading);
            Messenger.Broadcast<string, PopupManager.Type>(Events.OnPopupRequired, "User Name Update Failed!", PopupManager.Type.notification);
            return;
        }
        

        //UnSubscribe To Login Response Event
        Messenger.RemoveListener<string>(Events.OnRESTAPICallback, OnUpdateUserProfileResponseRecieved);
    }


    

    void OnGetUserProfile(GetUserProfileRequestRootObject obj)
    {
        Initialize(obj);

        Messenger.RemoveListener<GetUserProfileRequestRootObject>(Events.OnGetUserProfile, OnGetUserProfile);
    }

    //-----------------------Delayed Update--------------------------
    IEnumerator UpdateUserCoinsUI()
    {
        yield return new WaitForEndOfFrame();
        myCoinsTextRef.text = Globals.TotalCoins.ToString();
        StopCoroutine(UpdateUserCoinsUI());
    }
}
