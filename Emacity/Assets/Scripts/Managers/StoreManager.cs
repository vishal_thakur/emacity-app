﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoreManager : MonoBehaviour
{

    [SerializeField]
    Transform StoreListParentRef;

    [SerializeField]
    StoreUnit storeUnitPrefabRef;

    //[SerializeField]
    //RestAPICallManager RestAPICallManagerRef;

    [SerializeField]
    GetStoresResponseRootObject getStoresResponseRootObject;

    void Start()
    {
        //Call Get Stores Api    
        Messenger.AddListener<string>(Events.OnRESTAPICallback, OnGetStoresResponseRecieved);

        //Grab The Puzzle Game info from the GetPuzzle API
        RestAPICallManager.CallRESTAPI(BestHTTP.HTTPMethods.Get, Globals.GetStoresURL);

        //Show Loading Untill The Stores Info is downloaded
        Messenger.Broadcast<string, PopupManager.Type>(Events.OnPopupRequired , "Loading Stores...",  PopupManager.Type.loading);
    }

    

    void OnGetStoresResponseRecieved(string response)
    {
        try
        {
            getStoresResponseRootObject = JsonUtility.FromJson<GetStoresResponseRootObject>(response);

            if (getStoresResponseRootObject.Success)
            {
                //Instantiate New Store Unit
                StoreUnit newStoreUnit = GameObject.Instantiate(storeUnitPrefabRef, StoreListParentRef, false);

                //Initialize new Store Unit
                newStoreUnit.Initialize(getStoresResponseRootObject);
            }
            else
            {
                //Show Loading Untill The Stores Info is downloaded
                Messenger.Broadcast<PopupManager.Type>(Events.OnPopupEnd, PopupManager.Type.loading);
                Messenger.Broadcast<string, PopupManager.Type>(Events.OnPopupRequired, "Unable to fetch Stores! ", PopupManager.Type.notification);

            }
        }
        catch (System.Exception e)
        {
            //Show Loading Untill The Stores Info is downloaded
            Messenger.Broadcast<PopupManager.Type>(Events.OnPopupEnd, PopupManager.Type.loading);
            Debug.LogError("Exception BRO = " + e.ToString());
            Messenger.Broadcast<string, PopupManager.Type>(Events.OnPopupRequired, "Unable to fetch Stores! ", PopupManager.Type.notification);
        }


        Messenger.RemoveListener<string>(Events.OnRESTAPICallback, OnGetStoresResponseRecieved);
    }

}
