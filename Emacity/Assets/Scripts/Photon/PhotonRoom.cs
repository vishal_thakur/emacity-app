﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PhotonRoom : MonoBehaviourPunCallbacks , IInRoomCallbacks
{
    //Room info
    public static PhotonRoom room;
    PhotonView PV;

    public bool isGameLoaded;
    public int currentScene;

    //Player info
    public Player[] photonPlayers;
    public int playersInRoom;
    public int myNumberInRoom;
    public int playerInGame;

    //Deplayed Start
    bool readyToCount;
    bool readyToStart;
    public float startingTime;
    float lessThanMaxPlayers;
    float atMaxPlayer;
    float timeToStart;


    private void Awake()
    {
        //Set up Singelton
        if(PhotonRoom.room == null)
        {
            PhotonRoom.room = this;
        }
        else
        {
            if(PhotonRoom.room != this)
            {
                Destroy(PhotonRoom.room.gameObject);
                PhotonRoom.room = this;
            }
        }
        DontDestroyOnLoad(this.gameObject);
    }

    public override void OnEnable()
    {
        //subscribe to Funtions
        base.OnEnable();
        PhotonNetwork.AddCallbackTarget(this);
        SceneManager.sceneLoaded += OnSceneFinishedLoading;
    }

    public override void OnDisable()
    {
        base.OnDisable();
        PhotonNetwork.RemoveCallbackTarget(this);
        SceneManager.sceneLoaded -= OnSceneFinishedLoading;
    }


    // Start is called before the first frame update
    void Start()
    {
        PV = GetComponent<PhotonView>();
        readyToCount = false;
        readyToStart = false;
        lessThanMaxPlayers = startingTime;
        atMaxPlayer = 6;
        timeToStart = startingTime;

    }

    void Update() {
        //For delay start only, count Down to Start
        if (MultiplayerSettings.multiplayerSettings.delayStart) {
            if (playersInRoom == 1) {
                RestartTimer();
            }
            if (!isGameLoaded) {
                if (readyToStart)
                {
                    atMaxPlayer -= Time.deltaTime;
                    lessThanMaxPlayers = atMaxPlayer;
                    timeToStart = atMaxPlayer;
                }
                else if (readyToCount) {
                    lessThanMaxPlayers -= Time.deltaTime;
                    timeToStart = lessThanMaxPlayers;
                }
                Debug.LogError("Display time to start to the Players" + timeToStart);
                if (timeToStart <= 0) {
                    StartGame();
                }
            }
        }
    }





    public override void OnJoinedRoom()
    {
        Debug.LogError("Room Joined");
        //Sets player data when we join the room
        base.OnJoinedRoom();
        photonPlayers = PhotonNetwork.PlayerList;
        playersInRoom = photonPlayers.Length;
        myNumberInRoom = playersInRoom;
        ///PhotonNetwork.NickName = myNumberInRoom.ToString();
        PhotonNetwork.NickName = Globals.myFirstName;
        //For delay start only
        if (MultiplayerSettings.multiplayerSettings.delayStart)
        {
            Debug.LogError("Displayed players in room out of max players possible ( " + playersInRoom + ":" + MultiplayerSettings.multiplayerSettings.maxPlayers + ")");
            if (playersInRoom > 1)
            {
                readyToCount = true;
            }
            if (playersInRoom == MultiplayerSettings.multiplayerSettings.maxPlayers)
            {
                readyToStart = true;
                if (!PhotonNetwork.IsMasterClient)
                    return;
                PhotonNetwork.CurrentRoom.IsOpen = false;
            }
        }
        //For non Delay Start
        else {
            StartGame();
        }

    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        base.OnPlayerEnteredRoom(newPlayer);
       // Globals.newPlayer = newPlayer;
        Debug.LogError("A new player has joined the room");
        photonPlayers = PhotonNetwork.PlayerList;
        playersInRoom++;
       // Messenger.Broadcast<Player>(Events.OnPlayerJoinedRoom , newPlayer);
        if (MultiplayerSettings.multiplayerSettings.delayStart)
        {
            Debug.LogError("Displayed players in room out of max players possible ( " + playersInRoom + ":" + MultiplayerSettings.multiplayerSettings.maxPlayers + ")");
            if(playersInRoom > 1)
            {
                readyToCount = true;
            }
            if(playersInRoom == MultiplayerSettings.multiplayerSettings.maxPlayers)
            {
                readyToStart = true;
                if (!PhotonNetwork.IsMasterClient)
                    return;
                PhotonNetwork.CurrentRoom.IsOpen = false;
            }
        }
    }


    void StartGame() {
        isGameLoaded = true;
        if (!PhotonNetwork.IsMasterClient)
            return;

        if (MultiplayerSettings.multiplayerSettings.delayStart)
        {
            PhotonNetwork.CurrentRoom.IsOpen = false;
        }

        PhotonNetwork.LoadLevel(MultiplayerSettings.multiplayerSettings.multiplayerScene);
    }


    void RestartTimer() {
        lessThanMaxPlayers = startingTime;
        timeToStart = startingTime;
        atMaxPlayer = 6;
        readyToCount = false;
        readyToStart = false;
    }


    void OnSceneFinishedLoading(Scene scene, LoadSceneMode mode) {
        //Called when multiplayer scene is loaded
        currentScene = scene.buildIndex;

        if (currentScene == MultiplayerSettings.multiplayerSettings.multiplayerScene) {
            isGameLoaded = true;

            //for Delay start game
            if (MultiplayerSettings.multiplayerSettings.delayStart) {
                PV.RPC("RPC_LoadedGameScene", RpcTarget.MasterClient);
            }
            // For non delay Start Game
            else {
                RPC_CreatePlayer();
            }
        }
    }


    [PunRPC]
    private void RPC_LoadedGameScene()
    {
        playerInGame++;

        if (playerInGame == PhotonNetwork.PlayerList.Length)
        {
            PV.RPC("RPC_CreatePlayer", RpcTarget.All);
        }
    }

    [PunRPC]
    private void RPC_CreatePlayer()
    {
       PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs" , "PhotonNetworkPlayer") ,transform.position , Quaternion.identity , 0);
        
    }


    public override void OnDisconnected(DisconnectCause cause)
    {
        base.OnDisconnected(cause);

        Debug.LogError(Globals.myFirstName +   "Got Disconnected due to " + cause.ToString()) ;

        EmacitySceneManager.LoadScene(Globals.SceneNames.MainMenu);
    }
    

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        base.OnPlayerLeftRoom(otherPlayer);
        Debug.LogError(otherPlayer.NickName + " has left the room.");
        playersInRoom--;
    }
}
