﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class PhotonPlayer : MonoBehaviour
{

    PhotonView PV;

    public GameObject myAvatar;
    // Start is called before the first frame update
    void Start()
    {
        PV = GetComponent<PhotonView>();
        int spawnPicker = Random.Range(0, GameSetup.GS.spawnPoints.Length);

        if (PV.IsMine)
        {
           myAvatar = PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs" , "PlayerAvatar") ,
                GameSetup.GS.spawnPoints[spawnPicker].localPosition , GameSetup.GS.spawnPoints[spawnPicker].rotation , 0);
            
        }
    }
    
}
