﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PhotonLobby : MonoBehaviourPunCallbacks
{

    public static PhotonLobby lobby;

    public Button BeginMultiplayerButton;
    public GameObject CancelRoomButton;

    void Awake()
    {
        if (PhotonNetwork.IsConnected)
        {
            BeginMultiplayerButton.interactable = true;
            BeginMultiplayerButton.GetComponentInChildren<Text>().text = "Start";
        }
        else
        {
            BeginMultiplayerButton.interactable = false;
            BeginMultiplayerButton.GetComponentInChildren<Text>().text = "Loading";
        }
        //BeginMultiplayerButton.SetActive(false);
        //CancelRoomButton.SetActive(false);
        lobby = this;
    }

    // called second
    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
    }
    
    //void ConnectToServer()
    //{
    //    PhotonNetwork.ConnectUsingSettings();//Connects TO master Photon Network
    //}

    void Start()
    {
        PhotonNetwork.ConnectUsingSettings();//Connects TO master Photon Network
       // ConnectToServer();
    }


    public void OnBeginMultiplayerButtonClicked()
    {
        Messenger.Broadcast<string, PopupManager.Type>(Events.OnPopupRequired, "Loading Emacity World..", PopupManager.Type.loading);

        BeginMultiplayerButton.interactable = false;
        BeginMultiplayerButton.GetComponentInChildren<Text>().text = "WAIT..";
        //BeginMultiplayerButton.SetActive(false);
        //CancelRoomButton.SetActive(true);
        PhotonNetwork.JoinRandomRoom();
    }



    //Creates New Room
    void CreateNewRoom() {
        //Create a new Room
        int randomRoomName = Random.Range(0, 10000);
        RoomOptions roomOps = new RoomOptions() { IsVisible = true, IsOpen = true, MaxPlayers = (byte)MultiplayerSettings.multiplayerSettings.maxPlayers};
        PhotonNetwork.CreateRoom("Room" + randomRoomName, roomOps , null);
    }


    public void OnCancelRoomButtonClicked() {
        //CancelRoomButton.SetActive(false);
        PhotonNetwork.LeaveRoom();
    }

    //-------------------------Event Callbacks-------------------------

    //On Successful Connection to Master 
    public override void OnConnectedToMaster()
    {
        //BeginMultiplayerButton.gameObject.SetActive(true);
        BeginMultiplayerButton.interactable = true;
        BeginMultiplayerButton.GetComponentInChildren<Text>().text = "START";
        //BeginMultiplayerButton.GetComponentInChildren<Text>().text = "Done";
        //BeginMultiplayerButton.name = "Done";
        //BeginMultiplayerButton.SetActive(true);
        //CancelRoomButton.SetActive(true);
        PhotonNetwork.AutomaticallySyncScene = true;
    }

    //On Random Room Creation Failed
    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        Debug.LogError("Not Able To Join Random Room, Return Code /t +" + returnCode + "Erorr Message /t" + message);
        //base.OnJoinRandomFailed(returnCode, message);

        CreateNewRoom();
    }

    public override void OnJoinedRoom()
    {
        BeginMultiplayerButton.interactable = false;
        BeginMultiplayerButton.GetComponentInChildren<Text>().text = "OK";
        Debug.LogError("Room joined");
        base.OnJoinedRoom();
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        Debug.LogError("Not Able To Create Room, Return Code /t +" + returnCode + "Erorr Message /t" + message);
        CreateNewRoom();
    }


    //private void OnGUI()
    //{
    //    GUILayout.TextField("Room Count \t" + PhotonNetwork.CountOfRooms);
    //}
}
