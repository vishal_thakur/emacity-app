﻿using UnityEngine;

public class CharacterControl : MonoBehaviour
{
    CharacterController cc;
    float wSpeed = 0.5f;
    float speed = 1.0f;
    float rotSpeed = 25.0f;
    float rot = 0f;
    float gravity = 20.0f;
    Animator anim;
    private Vector3 moveDirection = Vector3.zero;

    void Start()
    {
        cc = GetComponent<CharacterController>();
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        if (cc.isGrounded)
        {
            moveDirection = new Vector3(0.0f, 0.0f, Input.GetAxis("Vertical"));
            moveDirection = transform.TransformDirection(moveDirection);
            moveDirection *= wSpeed;
            //Walk case
            if(Input.GetKey(KeyCode.W))
            {
                anim.SetBool("CharInAction", true);
                anim.SetInteger("aCode", 1);
            }
            if (Input.GetKeyUp(KeyCode.W))
                resetCharacter();
            //Run Case
            if (Input.GetKey(KeyCode.Q))
            {
                anim.SetBool("CharInAction", true);
                anim.SetInteger("aCode", 2);
                moveDirection = new Vector3(0.0f, 0.0f, 1f);
                moveDirection = transform.TransformDirection(moveDirection);
                moveDirection *= speed;
            }
            if (Input.GetKeyUp(KeyCode.Q))
                resetCharacter();

            //Attack Case
            if (Input.GetKey(KeyCode.E))
            {
                anim.SetBool("CharInAction", true);
                anim.SetBool("Attack", true);
                anim.SetInteger("aCode", 3);
            }
            if (Input.GetKeyUp(KeyCode.E))
                resetCharacter();
        }
        cc.Move(moveDirection * Time.deltaTime);
        transform.Rotate(0, Input.GetAxis("Horizontal"), 0);
    }

    void resetCharacter()
    {
        anim.SetBool("CharInAction", false);
        anim.SetInteger("aCode", 0);
        anim.SetBool("Attack", false);
        moveDirection = Vector3.zero;
        //transform.rotation = Quaternion.Euler(Vector3.zero);
    }
}
