﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshHandler : MonoBehaviour
{
    [SerializeField]
    bool update = false;
    [SerializeField]
    bool meshRenderedEnabled = false;


    [SerializeField]
    MeshRenderer[] meshArray;


    private void OnDrawGizmos()
    {
        if (update)
        {
            meshArray = GetComponentsInChildren<MeshRenderer>();
            foreach (MeshRenderer mesh in meshArray)
            {
                mesh.enabled = meshRenderedEnabled;
            }
            Debug.LogError("set to " + meshRenderedEnabled);

            update = false;
            //meshRenderedEnabled = !meshRenderedEnabled;
        }

        
    }
}
