﻿using System;

public class Events{

    #region Misc
    public static string OnRESTAPICallback = "OnRESTAPICallback";
    #endregion



    #region Login Scene
    public static string OnFirebaseDependenciesChecked = "OnFirebaseDependenciesChecked";

    public static string OnLoginTried = "OnLoginTried";

    public static string OnLoginSuccessful = "OnLoginSuccessful";
    public static string OnSocialLoginSuccessful = "OnSocialLoginSuccessful";
    public static string OnLoginFailed = "OnLoginFailed";

    public static string OnSignupSuccessful = "OnSignupSuccessful";
    public static string OnSignupFailed = "OnSignupFailed";

    public static string OnMainMenuLoadedCompletely = "OnMainMenuLoadedCompletely";
    public static string OnGameSceneLoadedCompletely = "OnGameSceneLoadedCompletely";
    #endregion




    #region Main Menu Scene
    public static string OnGetUserProfile = "OnGetUserProfile";
    public static string OnCustomizationOptionUpdate = "OnCustomizationOptionUpdate";
    public static string OnAvatarCustomizationOptionSelected = "OnAvatarCustomizationOptionSelected";
    public static string OnAvatarCustomizationOptionTypeSelected = "OnAvatarCustomizationOptionTypeSelected";


    public static string OnAvatarCustomizationStarted = "OnAvatarCustomizationStarted";
    public static string OnAvatarCustomizationFinished = "OnAvatarCustomizationFinished";

    #endregion


    #region Gameplay Scene
    public static string OnNetworkHosted = "OnNetworkHosted";
    public static string OnReadyToExitGame = "OnReadyToExitGame";
    public static string OnGameSoundToggled = "OnGameSoundToggled";
    public static string OnHitBySpell = "OnHitBySpell";
    public static string OnCameraRotationSliderUpdated = "OnCameraRotationSliderUpdated";
    public static string OnSoundButtonToggled = "OnSoundButtonToggled";
    public static string OnSpellCasted = "OnSpellCasted";
    #endregion



    #region Photon
    //public static string OnPlayerJoinedRoom = "OnPlayerJoinedRoom";
    //public static string OnPlayerLeftRoom = "OnPlayerLeftRoom";
    #endregion







    #region Chat 
    public static string OnUserSelectedToChat = "OnUserSelectedToChat";

    public static string OnIncomingChatRequest = "OnIncomingChatRequest";
    #endregion




    #region Puzzle game
    public static string OnPuzzleGameInitialized = "OnPuzzleGameInitialized";
    public static string OnPuzzlePieceCollected = "OnPuzzlePieceCollected";
    public static string OnAllPuzzlePiecesCollected = "OnAllPuzzlePiecesCollected";
    public static string OnLadderUsed = "OnLadderUsed";
    public static string OnShieldUsed = "OnShieldUsed";
    #endregion

    #region Quiz Game
    public static string OnQuizGameInitialized = "OnQuizGameInitialized";
    public static string OnQuizGameRewardRecieved = "OnQuizGameRewardRecieved";
    #endregion


    #region UI
    public static string OnUIPanelShown = "OnUIPanelShown";
    public static string OnPopupRequired = "OnPopupRequired";
    public static string OnPopupEnd = "OnPopupEnd";
    public static string OnPopupRetrySelected = "OnPopupRetrySelected";
    public static string OnPopupYesSelected = "OnPopupYesSelected";
    #endregion



    #region Vendor and Advertisement Units
    public static string OnShopInteracted = "OnShopInteracted";
    public static string OnAdvertisementInteracted = "OnAdvertisementInteracted";
    public static string OnVendorUnitLoadedSuccessfully = "OnVendorUnitLoadedSuccessfully";
    public static string OnAdvertisementUnitLoadedSuccessfully = "OnAdvertisementUnitLoadedSuccessfully";
    public static string OnStoreSelected = "OnStoreSelected";
    public static string OnAllVendorsAndAdvertisementUnitsLoaded = "OnAllVendorsAndAdvertisementUnitsLoaded";
    public static string OnAddUserToVendor = "OnAddUserToVendor";
    public static string OnRemoveUserFromVendor = "OnRemoveUserFromVendor";
    #endregion




    #region IAP
    //Ladder 
    public static string OnIAPSkillLadderPurchaseSuccess = "OnIAPSkillLadderPurchaseSuccess";

    //Coins
    public static string OnIAPCoinsPurchaseSuccess = "OnIAPCoinsPurchaseSuccess";

    //Spells
    public static string OnIAPSpellPurchaseSuccess = "OnIAPSpellPurchaseSuccess";

    //Shield
    public static string OnIAPShieldPurchaseSuccess = "OnIAPShieldPurchaseSuccess";
    #endregion


}