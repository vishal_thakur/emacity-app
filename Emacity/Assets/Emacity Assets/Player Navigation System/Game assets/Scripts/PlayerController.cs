﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    List<AvatarUnit> allAvatars = new List<AvatarUnit>();

    [SerializeField]
    AvatarUnit currentAvatar;

    [SerializeField]
    Canvas AvatarNameCanvasRef;

   public Joystick joystick;
   public CharacterController controller;
    public Animator anim;
    public Image bulletIndicator;
   public Transform targetIndicator;
   public Transform target;
   public ParticleSystem movementEffect;
   public ParticleSystem shootingEffect;
   public GameObject bloodEffect;
   
   public Transform gunFront;
   public GameObject bullet;
   
   public float speed;
   public float gravity;
   public float bulletCount;
   public float reloadSpeed;


   string movementAnimationName = "walking";

   Vector3 moveDirection;
   float bullets = 1f;


    Vector3 raycastInitialPoint;
    bool reloading;

    [SerializeField]
    bool CanClimbShop = false;
   
   List<GameObject> bulletStorage = new List<GameObject>();
   
   [HideInInspector]
   public bool safe;
   
   GameManager manager;

    [SerializeField]
    GameObject ShieldObjRef;

    [SerializeField]
    GameObject FreezedEffectObjRef;

    // Photon 
    PhotonView PV;

    [SerializeField]
    bool isFreezed = false;

    [Header("Audios"), Space(10)]
    [SerializeField] AudioSource puzzlePieceCollectedAudioRef;
    [SerializeField] AudioSource spellCastedAudioRef;
    [SerializeField] AudioSource shieldActivatedAudioRef;
    [SerializeField] AudioSource freezeAudioRef;



    private void OnTriggerEnter(Collider other)
    {
        if (!PV.IsMine)
            return;

        //Collided with an Advertisement
        if (other.gameObject.layer == 17)
        {
            //Interact with the Advertisement
            other.GetComponent<AdvertisementUnit>().OnCustomerApproachedAdvertisement();
        }
        //Collided with A shop 
        else if (other.gameObject.layer == 16)
        {
            //Interact with the shop
            other.transform.parent.GetComponent<ShopUnit>().OnCustomerApproachedEntrance();
        }
        //Collided with A Puzzle Piece 
        else if (other.gameObject.layer == 15)
        {
            //Interact with the puzzle piece
            PuzzleUnit unit = other.transform.parent.GetComponent<PuzzleUnit>();
            unit.OnCollected();
            Messenger.Broadcast<PuzzleUnit>(Events.OnPuzzlePieceCollected , unit);

            //Play puzzle Piece Collected Audio
            puzzlePieceCollectedAudioRef.Play();

            Debug.LogError("Puzzle Piece Collected, name is "  + other.gameObject.name );
        }

    }


    void OnCollisionEnter(Collision collision)
    {
        //If is a Projectile
        if (collision.gameObject.layer == 13)
        {
            //If shield is Inactive Then Take Spell Damage
            if (!ShieldObjRef.activeSelf)
                OnHitBySpell();
        }
    }
    



    private void OnTriggerExit(Collider other)
    {
        if (!PV.IsMine)
            return;

        //Collided with A shop 
        if (other.gameObject.layer == 16)
        {
            other.transform.parent.GetComponent<ShopUnit>().OnCustomerLeftEntrance();
        }
        //Collided with an Advertisement 
        if (other.gameObject.layer == 17)
        {
            other.GetComponent<AdvertisementUnit>().OnCustomerLeftAdvertisement();
        }
    }



    //-------------RPC Calls---------------------

    [PunRPC]
    void LoadAvatar(string[] data) {
        //[0]string avatarName, [1]string hairType, [2]string clothTopType, [3]string clothBottomType, [4]string shoeType
        //Debug.LogError((string)PhotonNetwork.LocalPlayer.CustomProperties["CharacterType"]);
        currentAvatar = GetAvatar(data[0]);

        

        //Enable the Gameobject
        currentAvatar.gameObject.SetActive(true);

        //Assign Animator
        anim = currentAvatar.GetComponent<Animator>();

        //anim.runtimeAnimatorController = currentAvatar.RuntimeAnimatorControllerRef;
        //anim.avatar = currentAvatar.AvatarRef;

        //Set 3D avatar Customizations As per saved by Avatar Selection Menu Earlier

        Globals.ActiveCustomizationOption = Globals.CustomizationOptionType.hair;
        currentAvatar.OnAvatarCustomizationOptionSelected(data[1]);

        Globals.ActiveCustomizationOption = Globals.CustomizationOptionType.top;
        currentAvatar.OnAvatarCustomizationOptionSelected(data[2]);

        Globals.ActiveCustomizationOption = Globals.CustomizationOptionType.bottom;
        currentAvatar.OnAvatarCustomizationOptionSelected(data[3]);

        Globals.ActiveCustomizationOption = Globals.CustomizationOptionType.shoe;
        currentAvatar.OnAvatarCustomizationOptionSelected(data[4]);
        

    }

    [PunRPC]
    void LoadCanvasName(string name)
    {
        ///AvatarNameCanvasRef.gameObject.SetActive(true);

        AvatarNameCanvasRef.GetComponentInChildren<Text>().text = name;
    }

    //-------------RPC Calls---------------------
    void Start(){

        PV = GetComponent<PhotonView>();
        manager = GameObject.FindObjectOfType<GameManager>();
        

        if (PV.IsMine)
        {
            PV.RPC("LoadAvatar" , RpcTarget.AllBuffered ,new string[] {Globals.CurrentCharacterType.ToString() , Globals.CurrentCharacterHairType.ToString(),
                Globals.CurrentCharacterClothTopType.ToString(), Globals.CurrentCharacterClothBottomType.ToString(), Globals.CurrentCharacterShoeType.ToString() } );

            joystick = manager.joystick;
            //manager.moveCamera.GetComponentInChildren<CameraFollow>().camTarget = this.transform;
            manager.moveCamera.GetComponent<CameraFollow>().camTarget = this.transform;

            //Dont Show My Name Canvas
            PV.RPC("LoadCanvasName", RpcTarget.AllBuffered, Globals.myFirstName);
            

            //Dont Show My Name Canvas
            AvatarNameCanvasRef.gameObject.SetActive(false);

            //Liten To Spell casted Event Only if We are the Main player not other Multiplayers
            Messenger.AddListener(Events.OnSpellCasted, OnSpellCasted);
            Messenger.AddListener(Events.OnLadderUsed, OnLadderUsed);
            Messenger.AddListener(Events.OnShieldUsed, OnShieldUsed);
        }
        else
        {
            targetIndicator.gameObject.SetActive(false); 
            //Dont Show My Name Canvas
            // PV.RPC("LoadCanvasName", RpcTarget.AllBuffered, PhotonNetwork.LocalPlayer.NickName + " | " + Time.time);

            //Show Name Canvas if this is another Player
            ///  PV.RPC("LoadCanvasName", RpcTarget.AllBuffered, true);
        }
    }


    private void OnDestroy()
    {
        if (PV.IsMine)
        {
            //Liten To Spell casted Event Only if We are the Main player not other Multiplayers
            Messenger.RemoveListener(Events.OnSpellCasted, OnSpellCasted);
            Messenger.RemoveListener(Events.OnLadderUsed, OnLadderUsed);
            Messenger.RemoveListener(Events.OnShieldUsed, OnShieldUsed);
        }

    }



    AvatarUnit GetAvatar(string avatar) {

        //Disable all Avatar GO's in case if any one is left enabled in the Unity Editor
        foreach (AvatarUnit unit in allAvatars){
            unit.gameObject.SetActive(false);
        }
        
        //Enable The selected avatar
        foreach (AvatarUnit unit in allAvatars){
            if (unit.myAvatar.ToString() == avatar)
                return unit;
        }

        Debug.LogError("Avatar Not found");
        return null;
    }

    void Update()
    {

       // Debug.LogError(transform.position.ToString());

        if (!manager.gameStarted)
            return;



        if (!PV.IsMine)
            return;

        if (isFreezed)
            return;

        // This would cast rays only against colliders in layer 8.
        // But instead we want to collide against everything except layer 8. The ~ operator does this, it inverts a bitmask.
        int layerMask = 1 << 11;
        //layerMask = ~layerMask;

        RaycastHit hit;
        raycastInitialPoint = new Vector3(transform.position.x, transform.position.y + 0.2f, transform.position.z);
        // Does the ray intersect any objects excluding the player layer

        if (Physics.Raycast(raycastInitialPoint, transform.TransformDirection(Vector3.forward), out hit, .2f, layerMask))
        {
            CanClimbShop = true;
            Debug.DrawRay(raycastInitialPoint, transform.TransformDirection(Vector3.forward) * hit.distance, Color.green);
            ///Debug.LogError("Did Hit" + hit.collider.gameObject.name);
        }
        else
        {
            CanClimbShop = false;
            Debug.DrawRay(raycastInitialPoint, transform.TransformDirection(Vector3.forward) * .2f, Color.red);
            //Debug.LogError("Did not Hit");
        }



        if (EventSystem.current.currentSelectedGameObject != null || Input.touchCount != 1)
        {
            anim.SetBool("walking", false);
            anim.SetBool("running", false);
            return;
        }

//#if UNITY_ANDROID && !UNITY_EDITOR
//        if (input.touchcount != 1)
//        {
//            anim.setbool("walking", false);
//            anim.setbool("running", false);
//            return;
//        }
//#endif


        //Get Joystick Direction
        Vector2 direction = joystick.direction;
        //Get Joystick Magnitude
        float magnitude = joystick.magnitude;

        //Rotate Direction with Camera View
        //direction = RotateWithCameraView();

        if (controller.isGrounded){
            moveDirection = new Vector3(direction.x, 0, direction.y);
            moveDirection = RotateWithCameraView(moveDirection);
            Quaternion targetRotation = moveDirection != Vector3.zero ? Quaternion.LookRotation(moveDirection) : transform.rotation;
			transform.rotation = targetRotation;
	
            moveDirection = moveDirection * speed;
        }

        moveDirection.y = moveDirection.y - (gravity * Time.deltaTime);
        controller.Move(moveDirection * Time.deltaTime);
		
		if(!reloading && !safe){
			if(bullets > 0){
				bullets -= Time.deltaTime * 1f/bulletCount;
			}
			else{
				reloading = true;
			}
		}
		else{
			if(bullets < 1f){
				bullets += Time.deltaTime * reloadSpeed;
			}
			else{
				reloading = false;
			}
		}


        //Walking Animation
        if (magnitude == 0)
        {

            anim.SetBool("walking", false);
            anim.SetBool("running", false);
        }
        else if (magnitude < 400 && magnitude > 0)
        {
            //Set Walking Speed
            speed = Globals.PlayerWalkSpeed;
            //movementAnimationName = "walking";
            anim.SetBool("walking", true);
            anim.SetBool("running", false);

        }
        else if(magnitude >= 400){
            //Set Running Speed
            speed = Globals.PlayerRunSpeed;
            //movementAnimationName = "running";
            anim.SetBool("walking", false);
            anim.SetBool("running", true);
        }
        
        


        ////Start Walking
        //if (anim.GetBool(movementAnimationName) != (direction != Vector2.zero))
        //{
        //    anim.SetBool(movementAnimationName, direction != Vector2.zero);

        //}

        //if(anim.GetBool("Reloading") != (reloading || safe))
        //	anim.SetBool("Reloading", (reloading || safe));

        //if(!safe){
        //	Vector3 targetPosition = new Vector3(target.position.x, transform.position.y, target.position.z);
        //	Quaternion targetIndicatorRotation = Quaternion.LookRotation((targetPosition - transform.position).normalized);
        //	targetIndicator.rotation = Quaternion.Slerp(targetIndicator.rotation, targetIndicatorRotation, Time.deltaTime * 50);
        //}	
    }

    Vector3 RotateWithCameraView(Vector3 direction) {
        Vector3 dir = manager.moveCamera.transform.TransformDirection(direction);
        dir.Set(dir.x, 0, dir.z);
        return dir.normalized * direction.magnitude;
    }


   public void Fire(){
	   GameObject newBullet = bulletStorage.Count > 0 ? recycleBullet() : Instantiate(bullet);
	   
	   newBullet.transform.rotation = transform.rotation;
	   newBullet.transform.position = gunFront.position;
	   
	   Bullet bulletController = newBullet.GetComponent<Bullet>();
	   bulletController.player = this;
	   shootingEffect.Play();
   }
   
   public void SwitchSafeState(bool safe){
	   this.safe = safe;
	   
	   targetIndicator.gameObject.SetActive(!safe);
   }
   
   public void Die(){
	   Instantiate(bloodEffect, transform.position + Vector3.up * 1.5f, transform.rotation);
	   Destroy(gameObject);
   }
   
   GameObject recycleBullet(){
	   GameObject newBullet = bulletStorage[0];
	   bulletStorage.Remove(newBullet);
	   newBullet.SetActive(true);
	   
	   return newBullet;
   }
   
   public void DisableBullet(GameObject targetBullet){
	   targetBullet.SetActive(false);
	   bulletStorage.Add(targetBullet);
   }


    //Event Callbacks
    void OnSpellCasted()
    {
        PV = GetComponent<PhotonView>();

        if (PV.IsMine)
        {
            //Load Shield
            PV.RPC("CastSpell", RpcTarget.AllBuffered);
        }
    }

    [PunRPC]
    void CastSpell()
    {
        //Play Spell Casted Audio
        spellCastedAudioRef.Play();


        //this.StopAllCoroutines();
        StopCoroutine(ProcessSpellCasting());
        StartCoroutine(ProcessSpellCasting());
    }


    void OnLadderUsed()
    {
        if (CanClimbShop)
        {
            //transform.position = new Vector3(transform.position.x, 0.61f, transform.position.z);
            transform.position = transform.position + transform.forward * 0.3f;//new Vector3(transform.position.x, 0.61f, transform.position.z);
            //here y represents the Height in which the player is elevated
            transform.position = new Vector3(transform.position.x , transform.position.y + 0.714f, transform.position.z);

            anim.SetTrigger("jump");
        }
    }

    void OnShieldUsed()
    {
        PV = GetComponent<PhotonView>();

        if (PV.IsMine)
        {
            //Load Shield
            PV.RPC("UseShield", RpcTarget.AllBuffered);
        }
    }

    void OnHitBySpell()
    {

        PV = GetComponent<PhotonView>();

        if (PV.IsMine)
        {
            //Process Hit By Spell
            PV.RPC("HitBySpell", RpcTarget.All);
        }
    }


    [PunRPC]
    void HitBySpell()
    {
        StopCoroutine(OnHitBySpellBehaviour());
        StartCoroutine(OnHitBySpellBehaviour());
    }


    IEnumerator OnHitBySpellBehaviour()
    {
        //Disable Player movement
        isFreezed = true;
        //Disable Animator 
        anim.enabled = false;
        //Play Freeze Audio
        freezeAudioRef.Play();
        //Broadcast that Player Has been hit by a spell and the effect shall be for the given duration
        
        Messenger.Broadcast<PhotonView,float>(Events.OnHitBySpell, GetComponent<PhotonView>() , 5);
        //Enable Freeze Effect Object
        FreezedEffectObjRef.SetActive(true);

        //wait for The Respective Spell Effect Duration 
        yield return new WaitForSeconds(5);

        //Disable Freeze Effect Object
        FreezedEffectObjRef.SetActive(false);
        //Enable Animator
        anim.enabled = true;
        //Enable Player Movement
        isFreezed = false;

        StopCoroutine(OnHitBySpellBehaviour());
    }

    [PunRPC]
    void UseShield()
    {
        //Play Spell Casted Audio
        shieldActivatedAudioRef.Play();


        StopCoroutine(ProcessShield());
        StartCoroutine(ProcessShield());
    }

    IEnumerator ProcessShield()
    {
        ShieldObjRef.SetActive(true);
        ShieldObjRef.GetComponent<Animator>().SetTrigger("grow");
        yield return new WaitForSeconds(5);
        ShieldObjRef.GetComponent<Animator>().SetTrigger("shrink");

        yield return new WaitForSeconds(2);
        ShieldObjRef.SetActive(false);
        StopCoroutine(ProcessShield());
    }

    IEnumerator ProcessSpellCasting()
    {
        anim.SetTrigger("castSpell");

        yield return new WaitForSeconds(currentAvatar.spellAnimationTimeWait);

        //Throw Projectile which is A Spell!
        GetComponent<SpawnProjectilesScript>().OnSpellCasted();

        StopCoroutine(ProcessSpellCasting());
    }
    
}
