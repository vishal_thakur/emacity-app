﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class CameraFollow : MonoBehaviour {

	//Variables visible in the inspector
    public float distance;
    public float height;
	public float smoothness;

    //[SerializeField] GameObject testCenterObj;
    //public LayerMask GridLayers;

    ////Zoom Params
    //[Space(10), Header("Zoom Params"), SerializeField]
    //float minOrthoCamSize = 10;

    //[SerializeField] float maxOrthoCamSize = 75;

    //[SerializeField]
    //float zoomCoeff = 1f;

    //[SerializeField]
    //float zoomLerpCoeff = 1;

    //[SerializeField]
    //bool isZooming;


    //[Space(10), Header("Rotate Params")]
    //[SerializeField]
    //float maxRotation = 90;

    //[SerializeField]
    //float minRotation = -90;

    Vector3 velocity;

    [SerializeField]
    float targetFOV = 23.2f;

    [SerializeField]
    float targetRotation = 0f;
    
    //Center Point around which the Camera Rotates
    [SerializeField]Vector3 CameraRotateAroundPoint;
    

    //Current Ortho Cam Size
    float currentFov;

    
    //Camera Rotation Speed
	public float CameraRotationSpeed = 1.4f;



    
    //Target That The Camera Will Follow
    public Transform camTarget;

    //Main Camera Reference
    [SerializeField] Camera mainCamera;


    void Start()
    {

        //listen to Camera Rotation Slider Value Update Broadcast
        Messenger.AddListener<float>(Events.OnCameraRotationSliderUpdated, OnCameraRotationSliderUpdated);
    }

    private void OnDestroy()
    {
        //listen to Camera Rotation Slider Value Update Broadcast
        Messenger.RemoveListener<float>(Events.OnCameraRotationSliderUpdated, OnCameraRotationSliderUpdated);
    }

    void OnEnable(){


        ////Subscribe To Pinch/Streth Gesturs For Zooming in/out
        //var Pinchrecognizer = new TKPinchRecognizer();
        //Pinchrecognizer.gestureRecognizedEvent += (r) =>
        //{
        //    mainCamera.orthographicSize -= Pinchrecognizer.deltaScale * zoomCoeff;
        //    mainCamera.orthographicSize = Mathf.Clamp(mainCamera.orthographicSize , minOrthoCamSize, maxOrthoCamSize);
            
        //};
        //TouchKit.addGestureRecognizer(Pinchrecognizer);



        //Subscribe To Rotation Gesturs For Rotate left/right
        var Rotationrecognizer = new TKRotationRecognizer();
        Rotationrecognizer.gestureRecognizedEvent += (r) =>
        {
            transform.RotateAround(camTarget.position, Vector3.up, -Rotationrecognizer.deltaRotation * CameraRotationSpeed);
        };
        TouchKit.addGestureRecognizer(Rotationrecognizer);
    }

    void OnDisable(){
        TouchKit.removeAllGestureRecognizers();

    }
    

    void OnCameraRotationSliderUpdated(float value)
    {
        mainCamera.orthographicSize = value;
        //mainCamera.orthographicSize = Mathf.Clamp(mainCamera.orthographicSize, minOrthoCamSize, maxOrthoCamSize);
    }


    void FixedUpdate()
    {
        ////Calculate The Center Point Position Around Which the Camera Shall Rotate
        //if (Input.touchCount > 1)
        //{
        //    if (PlayerInput.Phase == TouchPhase.Moved)
        //    {
        //        RaycastHit hit;

        //        if (Physics.Raycast(transform.position, transform.forward, out hit))
        //        {
        //            CameraRotateAroundPoint = hit.point;
        //            //Debug.DrawRay(transform.position, transform.forward * 1000f, Color.red);
        //            //testCenterObj.transform.position = CameraRotateAroundPoint;

        //        }

        //    }
        //}
    }





    void LateUpdate()
    {
        //Check if the camera has a target to follow
        if (!camTarget)
            return;
		
		Vector3 pos = Vector3.zero;
		pos.x = camTarget.position.x;
		pos.y = camTarget.position.y + height;
		pos.z = camTarget.position.z - distance;
		
		transform.position = Vector3.SmoothDamp(transform.position, pos, ref velocity, smoothness);
		//transform.LookAt(camTarget.position);
    }
}
