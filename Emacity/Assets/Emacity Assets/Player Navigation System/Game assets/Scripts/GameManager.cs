﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Photon.Pun;
using Photon.Realtime;

public class GameManager : MonoBehaviour
{

    public static GameManager Instance = null;

    public Joystick joystick;
    public Animator startPanel;
    //public Animator gamePanel;
    public Animator gameOverPanel;
    public Animator startOverlay;
    public Animator tutorial;
   // public Animator moveCamera;
    public Camera moveCamera;
    public Animator transition;

    public Text bestText;
    public Text scoreText;

    [HideInInspector]
    public bool gameStarted;

    Spawner spawner;

    [SerializeField]
    public PlayerController player;

    [Space(10)]
    [SerializeField]
    List<AudioSource> BGMusicList;

    [SerializeField]
    AudioSource BGMusicLinkerAudioRef;

    [SerializeField]
    int randomBGMusicIndex = 0;

    public void Awake()
    {
        Instance = this;
        
    }

    void Start() {
        spawner = GameObject.FindObjectOfType<Spawner>();
        player = GameObject.FindObjectOfType<PlayerController>();
        
        //Play Game Sounds
        PlayBGAudio();


        OnStartGameClicked();
    }


    void PlayBGAudio()
    {
        StopCoroutine(GamePlayBGMusicLogic());
        StartCoroutine(GamePlayBGMusicLogic());
    }


    IEnumerator GamePlayBGMusicLogic()
    {
        //Get a random Index
        int rand = Globals.GetRandomNumber(0, 2);
        //Debug.LogError("rand = " + rand);

        //Get unique Random every Time
        if (randomBGMusicIndex == rand)
        {
            while (randomBGMusicIndex == rand)
            {
                rand = Globals.GetRandomNumber(0, 2);
                yield return null;
            }
        }

        randomBGMusicIndex = rand;

        //Debug.LogError("randomBGMusicIndex = " + randomBGMusicIndex);

        StartCoroutine(AudioController.FadeIn(BGMusicList[randomBGMusicIndex] ,10));
        //BGMusicList[randomBGMusicIndex].Play();

        //Debug.LogError("playing" + BGMusicList[randomBGMusicIndex].gameObject.name);
        //wait till this BG track Completes
        yield return new WaitForSeconds(BGMusicList[randomBGMusicIndex].clip.length);

        StartCoroutine(AudioController.FadeIn(BGMusicLinkerAudioRef , 10));
        //play the BG linker Audio
        //BGMusicLinkerAudioRef.Play();

        //Debug.LogError("playing linker sound");

        yield return new WaitForSeconds(BGMusicLinkerAudioRef.clip.length);

        PlayBGAudio();
    }
    


    public void OnStartGameClicked()
    {
        if (!gameStarted)
        {
            StartGame();
        }
    }


    public void OnExitGameClicked()
    {

        Destroy(PhotonRoom.room.gameObject);
        StartCoroutine(DisconnectAndLoad());
    }


    IEnumerator DisconnectAndLoad()
    {
        //Diconnect from current Room
        PhotonNetwork.LeaveRoom();

        //Wait untill disconnected
        while (PhotonNetwork.InRoom)
            yield return null;

        Messenger.Broadcast(Events.OnReadyToExitGame);


    }

    
	
	void StartGame(){

        //PhotonNetwork.Instantiate("Player character", player.transform.position , player.transform.rotation, 0);

        gameStarted = true;
	}
    
	public void GameOver(){
		//if(!gamePanel.gameObject.activeSelf)
		//	return;
		
		Target target = GameObject.FindObjectOfType<Target>();
		int score = target.GetScore();
		
		if(score > PlayerPrefs.GetInt("Best"))
			PlayerPrefs.SetInt("Best", score);
		
		bestText.text = PlayerPrefs.GetInt("Best") + "";
		scoreText.text = score + "";
		
		//gamePanel.gameObject.SetActive(false);
		gameOverPanel.SetTrigger("Game over");
		
		player.Die();
	}
	
	IEnumerator RestartGame(){
		transition.SetTrigger("Transition");
		
		yield return new WaitForSeconds(0.5f);
		
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}
}
